#!/bin/sh

# Replace variables $ENV{<environment varname>}
ReplaceEnvironmentVariable() {
    grep -rl "\$ENV{\"$1\"}" "$3"|xargs -r \
        sed -i "s|\\\$ENV{\"$1\"}|$2|g"
}

# Replace all variables
for _curVar in $(env | awk -F = '{print $1}');do
    ReplaceEnvironmentVariable "${_curVar}" "$(eval "echo \$${_curVar}")" /etc/nginx/conf.d/default.conf
done

# Run nginx
exec /usr/sbin/nginx