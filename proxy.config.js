const PROXY_CONFIG = [
  {
    context: [
      "/back-agora",
    ],
    target: "http://localhost:8085/agora/",
    logLevel: "debug",
    changeOrigin: true,
    secure: false,
    pathRewrite: {
      "^/back-agora/": "/"
    }
  },
  {
    context: [
      "/oauth"
    ],
    target: "http://localhost:8080/agora-admin/",
    logLevel: "debug",
    changeOrigin: true,
    secure: false,
    pathRewrite: {
      "^/oauth": "/oauth"
    }
  },
  {
    context: [
      "/back"
    ],
    target: "http://localhost:8080/agora-admin/",
    logLevel: "debug",
    changeOrigin: true,
    secure: false,
    pathRewrite: {
      "^/back": "/"
    }
  }
];
module.exports = PROXY_CONFIG;
