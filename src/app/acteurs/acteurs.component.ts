import {AfterViewInit, Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Utilisateur} from '../models/utilisateur';
import {FilieresService} from '../services/filieres.service';
import {UtilisateursService} from '../services/utilisateurs.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {ToastrService} from 'ngx-toastr';
import {Role} from '../models/role';
import {GroupeRole} from '../models/goupeRole';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthGuard} from '../services/auth.service';
import {DataService} from "../services/data.service";

@Component({
  selector: 'app-acteurs',
  templateUrl: './acteurs.component.html',
  styleUrls: ['./acteurs.component.css']
})
export class ActeursComponent implements OnInit, AfterViewInit {
  @ViewChild('Acteur') template: TemplateRef<any>;
  acteurs = new Array<Utilisateur>();
  acteur: Utilisateur;
  index: number;
  filieres = new Array<any>();
  famillesRole = new Array<any>();
  roles = new Array<any>();
  rolesFilter = new Array<any>();
  required = false;
  disabled = false;
  emailErrone = false;
  modalRef: BsModalRef;
  ngbModalOptions = {
    backdrop: true,
    ignoreBackdropClick: true
  };
  modalTitle = 'Créer un acteur';
  selectConfig = {
    labelField: 'nom',
    valueField: 'idGroupeRole',
    searchField: ['nom'],
    dropdownDirection: 'down',
    sortField: [{ field: 'nom', direction: 'asc' }],
  };

  roleSelectConfig = {
    labelField: 'nom',
    valueField: 'idRole',
    searchField: ['nom'],
    dropdownDirection: 'down',
    sortField: [{ field: 'nom', direction: 'asc' }],
  };
  isAdmin = false;
  isReponsable = false;
  options = [{label: 'Non', value: false}, {label: 'Oui', value: true}];
  role = new Role();
  familleRole = new GroupeRole();
  constructor(private filiereService: FilieresService, private utilisateurService: UtilisateursService,
              private modalService: BsModalService, private toastr: ToastrService, private router: ActivatedRoute) {
  }

  ngOnInit(): void {
    AuthGuard.getConnectedUser().subscribe(currentUser => {
           this.isAdmin = currentUser.isAdmin;
           this.isReponsable = currentUser?.roles?.filter(role => role.nom === 'Responsable de filière'
             || role.nom === 'Responsable de filière délégué'
             || role.nom === 'Coordinateur Filière')?.length > 0;
    });
    this.filiereService.getV1Filieres(undefined, undefined, undefined, 'nom').subscribe(result => {
      result.forEach(item => {
        const filiere = { label: item.nom, value: item.nom };
        this.filieres.push(filiere);
      });
    });
    this.utilisateurService.getV1Utilisateurs(undefined, undefined, undefined, 'prenom').subscribe(result => {
      this.acteurs = result;
      this.acteurs.sort((a, b) =>
            (a.nomComplet.toLowerCase() > b.nomComplet.toLowerCase()) ? 1 : ((b.nomComplet.toLowerCase() > a.nomComplet.toLowerCase()) ? -1 : 0));
      this.acteurs.forEach(acteur => {
        acteur.role = '';
        acteur.familleRole = '';
        acteur.role += acteur.roles.map(item => item.nom);
        acteur.familleRole += acteur.familleRoles.map(item => item.nom);
      });
    });
    this.utilisateurService.getV1FamillesRoles(undefined, undefined, 'nom').subscribe(result => {
      this.famillesRole = result;
      this.famillesRole.forEach(item => {
        item.label = item.nom;
        item.value = item.nom;
      });
    });

    this.utilisateurService.getV1Roles(undefined, undefined, 'nom', undefined, undefined).subscribe(result => {
      this.rolesFilter = result;
      this.rolesFilter.forEach(item => {
        item.label = item.nom;
        item.value = item.nom;
      });
    });
  }

  openModal(acteur, disabled, template: TemplateRef<any>): any {
    this.disabled = disabled;
    this.roles = new Array();

    if (acteur?.idUtilisateur) {
      this.index = this.acteurs.indexOf(acteur);
      this.acteur = JSON.parse(JSON.stringify(acteur));
     if (!this.acteur.familleRoles) {
     this.acteur.familleRoles = new Array<GroupeRole>();
      }
      if (!this.acteur.roles) {
        this.acteur.roles = new Array<Role>();
      }
      /** if (this.acteur.familleRole?.idGroupeRole !== undefined) {
        this.changeFamilleRoles(this.acteur.familleRole.idGroupeRole);
      }*/
      this.modalTitle = 'Gérer l\'acteur ' + acteur.nomComplet;
    } else {
      this.acteur = new Utilisateur();
      this.acteur.roles = new Array<Role>();
      this.acteur.familleRoles = new Array<GroupeRole>();
      this.modalTitle = 'Créer un acteur';
    }
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'modal-lg' }, this.ngbModalOptions)
    );
  }

  openModalArchive(acteur, template: TemplateRef<any>): any {
    this.acteur = JSON.parse(JSON.stringify(acteur));
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'modal-dialog-centered' }, this.ngbModalOptions)
    );

  }

  submit(): any {
    if (this.checkFields()) {
      if (this.acteur.id == null) {
        this.utilisateurService.postV1utilisateurs(this.acteur).subscribe(result => {
          this.acteurs.push(result);
          this.acteurs.sort((a, b) => (a.nomComplet.toLowerCase() > b.nomComplet.toLowerCase()) ? 1 : ((b.nomComplet.toLowerCase() > a.nomComplet.toLowerCase()) ? -1 : 0))
          this.showSuccess();
          this.modalRef.hide();
        }, (err) => {
          err.status === 409 ? this.showError('Code cp déjà utilisé') : this.showFailure();
        });
      } else {
        this.utilisateurService.putV1utilisateurs(this.acteur).subscribe(result => {
          this.acteurs[this.index] = result;
          this.showSuccess();
          this.modalRef.hide();
        }, (err) => {
          err.status === 409 ? this.showError('Code cp déjà utilisé') : this.showFailure();
        });
      }
    } else if (!this.checkFields() && !this.emailErrone) {
      this.showError('Merci de remplir tous les champs obligatoires');
    }
  }

  archiverActeur(): any {
    this.utilisateurService.deleteV1utilisateursId(this.acteur.id).subscribe(() => {
      this.acteurs = this.acteurs.filter(item => item.id !== this.acteur.id);
      this.showSuccess();
      this.modalRef.hide();
    }, () => {
      this.showFailure();
    });
  }

  changeFamilleRoles(idGroupeFamille): any {
    this.familleRole.nom = this.famillesRole?.filter(famille => famille.idGroupeRole === +idGroupeFamille)[0].nom;
    this.utilisateurService.getV1Roles(undefined, undefined, 'nom', undefined, undefined, +idGroupeFamille).subscribe(result => {
     // this.roles = new Array();
      this.roles = result;
      this.roles.forEach(item => {
        item.label = item.nom;
        item.value = item.nom;
      });
    });
  }

  showSuccess(): any {
    this.toastr.clear();
    this.toastr.success('Opération effectuée avec succès !');
  }

  showFailure(): any {
    this.toastr.clear();
    this.toastr.error('L\'opération a échoué. Veuillez réessayer à nouveau.');
  }

  showError(msg): any {
    this.toastr.clear();
    this.toastr.error(msg);
  }

  hideModal(): any {
    this.required = false;
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }

  checkFields(): boolean {
    if (this.acteur.nom === undefined || this.acteur.nom.toString() === ''
      || this.acteur.prenom === undefined || this.acteur.prenom.toString() === ''
        || this.acteur.cp === undefined || this.acteur.cp === null || this.acteur.cp.toString() === ''
          || this.emailErrone) {
      this.required = true;
      return false;
    } else {
      this.required = false;
      return true;
    }
  }

  isInvalid(value): boolean {
    return this.required && (value === undefined || value === null || value === '');
  }

  changeMail(emailErr: boolean): any {
    this.emailErrone = emailErr;
  }

  ngAfterViewInit(): void {
    this.router.queryParams.subscribe(params => {
      if (params.search) {
        DataService.getActeurItem().subscribe(result => {
          this.acteur = result;
        });
        this.openModal(this.acteur, true, this.template);
      }
      if (params.ajout) {
        this.openModal(null, false, this.template);
      }
    });
  }

  changeValue(nom: string): string {
    const nomR = nom?.toLowerCase().split(' ');

    return nomR?.map((str) => {
      const index = str.indexOf('-');
      if (index > -1) {
        return       (str.charAt(0).toUpperCase() + str.slice(1, index + 1 )
          + str.charAt(index + 1).toUpperCase() + str.slice(index + 2));

      } else {
        return       str.charAt(0).toUpperCase() + str.slice(1);
      }

      }).join(' ');

  }

  changeRoles(idRole: number): any {
    const role = this.roles?.filter(r => r.idRole === +idRole)[0];
    this.role.nom = role ? role.nom : '';
  }

  addRole(): any {
    if (this.familleRole?.idGroupeRole) {
      this.acteur.familleRoles.push(this.familleRole);
      this.familleRole = new GroupeRole();
      this.acteur.roles.push(this.role);
      this.role = new Role();
    }
  }

  deleteRole(index: number): any {
    this.acteur.roles.splice(index, 1);
    this.acteur.familleRoles.splice(index, 1);
  }
}
