import { ConstantService, AuthenticationService } from '@tec/condor/services';
import { environment } from 'src/environments/environment';


export function initApplication(cs: ConstantService, authService: AuthenticationService): () => Promise<void> {
    let url = environment.connectionUrl;
    const commande = 'constants';
    url = url + commande;
    const t1 = performance.now();
    return () => Promise.all(
        [
            // Initialisation du  niveau de trace
            // Promesses chaînées
            Promise.resolve(
                // Chargement des constantes en premier !
                cs.load(url)
            ).then(() => {
                // Récupération de l'utilisateur connecté
                Promise.resolve(authService.loadCurrentUser());
            })
        ]).then(value => {
            const t2 = performance.now();
            const took = ((t2 - t1) / 1000).toFixed(3);
            console.log('Initialisation terminée avec succès (temps d\'exécution:', took, ')');
        }).
        catch(ex => console.error('Erreur lors de l\'initialisation de l\'application:', ex));

}
