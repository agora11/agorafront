import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ObjetComponent } from './objet/objet.component';
import { TermeComponent } from './terme/terme.component';
import { ListObjetComponent } from './list-objet/list-objet.component';
import { ListTermeComponent } from './list-terme/list-terme.component';
import { ObjetListResolver } from './resolvers/objet-list-resolver';
import { ObjetResolver } from './resolvers/objet-resolver';
import { TermeListResolver } from './resolvers/terme-list-resolver';
import { LoginCaimanComponent } from './login-caiman/login-caiman.component';
import { AuthGuard } from './services/auth.service';
import { CdAutoLoginModule } from './condor/auto-login/auto-login.module';
import { FilieresComponent } from './filieres/filieres.component';
import { FiliereComponent } from './filieres/filiere/filiere.component';
import { DeactivateGuardObjet } from './objet/desactivate-guard-objet';
import { DeactivateGuardTerme } from './terme/desactivate-guard-terme';
import { MainPageComponent } from './main-page/main-page.component';
import { ActeursComponent } from './acteurs/acteurs.component';
import { AffectationComponent } from './profil/affectation/affectation.component';
import { MonProfilComponent } from './profil/mon-profil/mon-profil.component';
import { ValidationComponent } from './profil/validation/validation.component';
import { BrouillonComponent } from './profil/brouillon/brouillon.component';
import { GisementsComponent } from './gisements/gisements.component';
import { DesactivateGuardAffectation } from './profil/affectation/desactivate-guard-affectation';
import { GisementComponent } from './gisements/gisement/gisement.component';
import { DesactivateGuardGisement } from './gisements/gisement/desactivate-guard-gisement';
import { QualitesComponent } from './qualites/qualites.component';
import { QualiteComponent } from './qualites/qualite/qualite.component';
import { ModelisationComponent } from './modelisation/modelisation.component';
import { DiagrammesComponent } from './modelisation/diagrammes/diagrammes.component';
import { QualiteResolver } from './resolvers/qualite-resolver';
import {DesactivateGuardQualite} from './qualites/qualite/desactivate-guard-qualite';
import {TermeResolver} from './resolvers/terme-resolver';
import {ModelisationResolver} from './resolvers/modelisation-resolver';
import {DesactivateGaurdModelisation} from './modelisation/desactivate-gaurd-modelisation';
import { DesactivateGuardProfil } from './profil/mon-profil/desactivate-guard-profil';
import {SearchComponent} from './search/search.component';


const routes: Routes = [
  { path: '', redirectTo: 'main/termes', pathMatch: 'full' },
  { path: 'autologin', loadChildren: () => CdAutoLoginModule },
  {
    path: 'main', component: MainPageComponent, canActivate: [AuthGuard],
    children: [
      {
        path: 'termes', component: ListTermeComponent, resolve: {
          termeList: TermeListResolver
        }, canActivate: [AuthGuard]
      },
      {
        path: 'objets', component: ListObjetComponent, resolve: {
          objetList: ObjetListResolver
        }, canActivate: [AuthGuard]
      },
      {
        path: 'objet', component: ObjetComponent, resolve: {
          objet: ObjetResolver, termesList: TermeListResolver
        }, canActivate: [AuthGuard], canDeactivate: [DeactivateGuardObjet]
      },
      {
        path: 'search', component: SearchComponent, canActivate: [AuthGuard]
      },
      {
        path: 'terme', component: TermeComponent, resolve: {
          objetsList: ObjetListResolver, terme: TermeResolver
        }, canActivate: [AuthGuard], canDeactivate: [DeactivateGuardTerme]
      },
      {
        path: 'filieres', component: FilieresComponent, resolve: {
        }, canActivate: [AuthGuard]
      },
      {
        path: 'filiere', component: FiliereComponent, resolve: {
        }, canActivate: [AuthGuard]
      },
      {
        path: 'acteurs', component: ActeursComponent, resolve: {
        }, canActivate: [AuthGuard]
      }, {
        path: 'affectation', component: AffectationComponent, resolve: {
        }, canActivate: [AuthGuard], canDeactivate: [DesactivateGuardAffectation]
      },
      {
        path: 'mon-profil', component: MonProfilComponent, resolve: {
        }, canActivate: [AuthGuard], canDeactivate: [DesactivateGuardProfil]
      },
      {
        path: 'validation', component: ValidationComponent, resolve: {
        }, canActivate: [AuthGuard]
      },
      {
        path: 'brouillon', component: BrouillonComponent, resolve: {
        }, canActivate: [AuthGuard]
      },
      {
        path: 'gisements', component: GisementsComponent, resolve: {
        }, canActivate: [AuthGuard]
      },
      {
        path: 'gisement', component: GisementComponent, resolve: {
        }, canActivate: [AuthGuard], canDeactivate: [DesactivateGuardGisement]
      },
      {
        path: 'qualites', component: QualitesComponent, resolve: {
        }, canActivate: [AuthGuard]
      },
      {
        path: 'qualite', component: QualiteComponent, resolve: {
          qualite: QualiteResolver
        }, canActivate: [AuthGuard], canDeactivate: [DesactivateGuardQualite]
      },
      {
        path: 'diagrams', component: DiagrammesComponent
      },
      {
        path: 'modelisation', component: ModelisationComponent, resolve: {
           modelisation: ModelisationResolver
        }, canActivate: [AuthGuard], canDeactivate: [DesactivateGaurdModelisation]
      }
    ]
  },
  {
    path: 'login-caiman', component: LoginCaimanComponent
  },
  { path: '**', redirectTo: '/main/termes', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
