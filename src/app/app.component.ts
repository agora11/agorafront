import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {DataService} from './services/data.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private router: Router) {
    router.events.subscribe((val: any) => {
      if (val.urlAfterRedirects !== undefined) {
        DataService.changeMenuItem(val.urlAfterRedirects.substring(val.urlAfterRedirects.indexOf('/', 2) + 1));
      } else  if ( val.url !== undefined && val.url !== 'main') {
        DataService.changeMenuItem(val.url.substring(val.url.indexOf('/', 2) + 1));
      }
    });
  }
  ngOnInit(): void {
  }

}
