import { registerLocaleData } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import localeFr from '@angular/common/locales/fr';
import { APP_INITIALIZER, LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AddTokenOidcInterceptor } from '@tec/condor/interceptors';
import {
  AuthenticationService,
  AuthGuardService,
  ConstantService,
  OidcService
} from '@tec/condor/services';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { ClickOutsideModule } from 'ng-click-outside';
import { NgSelectizeModule } from 'ng-selectize';
import { BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { Overlay, ToastrModule } from 'ngx-toastr';
import { AutoCompleteModule, CalendarModule, CardModule, CheckboxModule, DropdownModule, InputSwitchModule, TabMenuModule } from 'primeng';
import { MultiSelectModule } from 'primeng/multiselect';
import { PanelMenuModule } from 'primeng/panelmenu';
import { TableModule } from 'primeng/table';
import { ActeursComponent } from './acteurs/acteurs.component';
import { initApplication } from './app-init.function';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CdAutoLoginModule } from './condor/auto-login/auto-login.module';
import { CheckboxComponent } from './containers/checkbox/checkbox.component';
import { FilterComponent } from './containers/filter/filter.component';
import { IconButtonComponent } from './containers/icon-button/icon-button.component';
import { WorkflowComponent } from './containers/workflow/workflow.component';
import { FiliereComponent } from './filieres/filiere/filiere.component';
import { FilieresComponent } from './filieres/filieres.component';
import { DesactivateGuardGisement } from './gisements/gisement/desactivate-guard-gisement';
import { GisementComponent } from './gisements/gisement/gisement.component';
import { GisementsComponent } from './gisements/gisements.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HeaderComponent } from './layout/header/header.component';
import { HeaderService } from './layout/header/header.service';
import { ListObjetComponent } from './list-objet/list-objet.component';
import { ListTermeComponent } from './list-terme/list-terme.component';
import { LoginCaimanComponent } from './login-caiman/login-caiman.component';
import { MainPageComponent } from './main-page/main-page.component';
import { MenuComponent } from './menu/menu.component';
import { DiagrammesComponent } from './modelisation/diagrammes/diagrammes.component';
import { ModelisationComponent } from './modelisation/modelisation.component';
import { DeactivateGuardObjet } from './objet/desactivate-guard-objet';
import { ObjetComponent } from './objet/objet.component';
import { AffectationComponent } from './profil/affectation/affectation.component';
import { DesactivateGuardAffectation } from './profil/affectation/desactivate-guard-affectation';
import { BrouillonComponent } from './profil/brouillon/brouillon.component';
import { MonProfilComponent } from './profil/mon-profil/mon-profil.component';
import { ProfilComponent } from './profil/profil.component';
import { ValidationComponent } from './profil/validation/validation.component';
import { QualiteComponent } from './qualites/qualite/qualite.component';
import { QualitesComponent } from './qualites/qualites.component';
import { AuthGuard } from './services/auth.service';
import { DataService } from './services/data.service';
import { ModelisationService } from './services/modelisation.service';
import { SpinnerOverlayService } from './services/spinner-overlay.service';
import { TokenInterceptorService } from './services/token-interceptor-service';
import { SpinnerOverlayComponent } from './spinner-overlay/spinner-overlay.component';
import { DeactivateGuardTerme } from './terme/desactivate-guard-terme';
import { TermeComponent } from './terme/terme.component';
import {DesactivateGuardQualite} from './qualites/qualite/desactivate-guard-qualite';
import {DesactivateGaurdModelisation} from './modelisation/desactivate-gaurd-modelisation';
import { DesactivateGuardProfil } from './profil/mon-profil/desactivate-guard-profil';
import { SearchComponent } from './search/search.component';
import {SearchService} from './services/search.service';

registerLocaleData(localeFr);

@NgModule({
  declarations: [
    AppComponent,
    CheckboxComponent,
    IconButtonComponent,
    ObjetComponent,
    HeaderComponent,
    FooterComponent,
    TermeComponent,
    MenuComponent,
    ListObjetComponent,
    ListTermeComponent,
    SpinnerOverlayComponent,
    LoginCaimanComponent,
    FilterComponent,
    FilieresComponent,
    FiliereComponent,
    MainPageComponent,
    ActeursComponent,
    AffectationComponent,
    BrouillonComponent,
    ValidationComponent,
    MonProfilComponent,
    ProfilComponent,
    GisementsComponent,
    GisementComponent,
    WorkflowComponent,
    QualitesComponent,
    QualiteComponent,
    ModelisationComponent,
    DiagrammesComponent,
    SearchComponent
  ],
  entryComponents: [SpinnerOverlayComponent],
    imports: [
        BrowserModule,
        AppRoutingModule,
        NgbModule,
        BrowserAnimationsModule,
        MatExpansionModule,
        HttpClientModule,
        ClickOutsideModule,
        FormsModule,
        AutocompleteLibModule,
        ModalModule.forRoot(),
        NgSelectizeModule,
        ToastrModule.forRoot({
            timeOut: 20000
        }),
        MatProgressSpinnerModule,
        CdAutoLoginModule,
        MatTooltipModule,
        TableModule,
        MultiSelectModule,
        PanelMenuModule,
        CheckboxModule,
        CalendarModule,
        InputSwitchModule,
        DropdownModule,
        AutoCompleteModule,
        CardModule,
        TabMenuModule
    ],
  providers: [
    HeaderService,
    BsModalService,
    DataService,
    SpinnerOverlayService,
    AuthGuardService,
    AuthGuard,
    Overlay,
    DeactivateGuardObjet,
    DeactivateGuardTerme,
    DesactivateGuardAffectation,
    DesactivateGuardGisement,
    DesactivateGuardQualite,
    ModelisationService,
    SearchService,
    DesactivateGaurdModelisation,
    DesactivateGuardProfil,
    {provide: LOCALE_ID, useValue: 'fr-FR' },
    {
      provide: APP_INITIALIZER,
      useFactory: initApplication,
      deps: [ConstantService, AuthenticationService],
      multi: true
    },
    { provide: AuthenticationService, useClass: OidcService },
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AddTokenOidcInterceptor,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
