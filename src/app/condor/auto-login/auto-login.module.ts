import { NgModule } from '@angular/core';
import { AutoLoginModule } from '@tec/condor/services';

@NgModule({
  declarations: [],
  imports: [
    AutoLoginModule
  ]
})
export class CdAutoLoginModule {

  constructor() {
  }

}
