import { Component, OnInit } from '@angular/core';
import { flattenDiagnosticMessageText } from 'typescript';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css']
})
export class CheckboxComponent implements OnInit {

  private flag: boolean;

  constructor() {
    this.flag = false;
  }

  ngOnInit(): void {
  }

  getImage() {
    return this.flag ? "/agora-admin/assets/img/2020 Iconographie Agora_Check box.png" :
      "/agora-admin/assets/img/2020 Iconographie Agora_Uncheck box.svg"
  }

  switch() {
    this.flag = !this.flag
  }

}
