import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {HeaderService} from "../../layout/header/header.service";
import {DataService} from "../../services/data.service";
import {UtilisateursService} from '../../services/utilisateurs.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit, OnDestroy {

  showFilieres: boolean;
  pickedFiliere: Array<string> = [];
  motif = '';
  filieres = new Array<any>();
  roles = new Array<any>();
  @Input() component: string;
  pickedRoles: any;

  constructor(private headerService: HeaderService, private utilisateurService: UtilisateursService) { }

  ngOnInit(): void {
    this.headerService.getAllFilieres().subscribe(result => {
      result.forEach( f => {
        const filiere = {label: f.nom, value: f.nom};
        this.filieres.push(filiere);
      });
    });
    if (this.component === 'affectation') {
      this.utilisateurService.getV1Roles(undefined, undefined, 'nom').subscribe(result => {
        result.forEach( f => {
          const role = {label: f.nom, value: f.nom};
          this.roles.push(role);
        });
      });
    }
  }

  showPopup(): any {
    this.showFilieres = !this.showFilieres;
  }

  search(): any {
    const searchValues = { filiere: this.pickedFiliere, roles: this.pickedRoles, searchValue: this.motif, component: this.component };
    DataService.pushSearch(searchValues);
  }

  reinit(): any {
    this.motif = '';
    this.pickedFiliere = [];
    this.pickedRoles = [];
    DataService.pushSearch({filiere: [], roles: [], searchValue: '', component: this.component });
  }

  ngOnDestroy(): void {
    this.reinit();
  }

  removeRole(role: any): any {
    this.pickedRoles = this.pickedRoles.filter(r => r !== role);
  }

  removeFiliere(filiere: string): any {
    this.pickedFiliere = this.pickedFiliere.filter(f => f !== filiere);
  }
}
