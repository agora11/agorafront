import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-icon-button',
  templateUrl: './icon-button.component.html',
  styleUrls: ['./icon-button.component.css']
})
export class IconButtonComponent implements OnInit {

  private flag: boolean;
  @Input()
  public image1: string;
  @Input()
  public image2: string;

  constructor() { 
    this.flag = false;
  }

  ngOnInit(): void {
  }

  getImage() {
    return this.flag ? this.image1 : this.image2
  }

  switch() {
    this.flag = !this.flag
  }
}
