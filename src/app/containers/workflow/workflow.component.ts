import { Component, OnInit, Input } from '@angular/core';
import {Statut} from '../../models/statut';
import {Status} from '../../models/status';

@Component({
  selector: 'app-workflow',
  templateUrl: './workflow.component.html',
  styleUrls: ['./workflow.component.css']
})
export class WorkflowComponent implements OnInit {

  @Input()
  status: Array<Statut>;

  @Input()
  status1: Statut;

  statusAccessor = Status;
  constructor() { }

  ngOnInit(): void {
  }

}
