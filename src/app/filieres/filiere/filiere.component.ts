import {Component, OnInit, TemplateRef} from '@angular/core';
import {Filiere} from '../../models/filiere';
import {ActivatedRoute} from '@angular/router';
import {Famille} from '../../models/famille';
import {SousFamille} from '../../models/sousFamille';
import {UtilisateursService} from '../../services/utilisateurs.service';
import {Utilisateur} from '../../models/utilisateur';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {DataService} from '../../services/data.service';
import {FilieresService} from '../../services/filieres.service';
import {ToastrService} from 'ngx-toastr';
import {FamillesService} from '../../services/familles.service';

@Component({
  selector: 'app-filiere',
  templateUrl: './filiere.component.html',
  styleUrls: ['./filiere.component.css', '../filieres.component.css']
})
export class FiliereComponent implements OnInit {
  title = 'Filière';
  filiere: Filiere;
  filiereToEdit: Filiere;
  idDisabled = false;
  famille: Famille;
  sousFamille: Famille;
  users: Utilisateur[];
  keyword = 'nomComplet';
  modalTitle = 'Créer une famille';
  modalRef: BsModalRef;
  totalSousF = 0;
  familleWithRole = 0;
  familleWithRolePercent = 0;
  ngbModalOptions = {
    backdrop: true,
    ignoreBackdropClick: true
  };
  required = false;
  indexF = 0;
  indexSF = 0;
  addBloc = false;
  familleSelectConfig = {
    labelField: 'nom',
    valueField: 'idFamille',
    searchField: ['nom'],
    dropdownDirection: 'down',
    sortField: [{field: 'nom', direction: 'asc'}],
  };

  constructor(private actRoute: ActivatedRoute, private userService: UtilisateursService,
              private modalService: BsModalService, private dataService: DataService,
              private filiereService: FilieresService, private toastr: ToastrService,
              private familleServie: FamillesService) {
  }

  ngOnInit(): void {
    this.userService.getV1Utilisateurs(undefined, undefined, 'prenom').subscribe(result => {
      this.users = result;
    });
    this.actRoute.queryParams.subscribe(
      data => {
        this.idDisabled = data.action !== 'edit';
      });
    DataService.getCurrentFiliere().subscribe(filiere => {
      this.familleWithRole = 0;
      this.totalSousF = 0;
      if (filiere.idFiliere) {
        this.filiere = filiere;
        localStorage.setItem('currentFiliere', JSON.stringify(this.filiere));
      } else {
        this.filiere = JSON.parse(localStorage.getItem('currentFiliere'));
      }

      if (this.filiere.familles && this.filiere.familles.length > 0) {
        this.filiere.familles.forEach(famille => {
          famille.isCollapsed = true;
          if (famille.responsableDeDonnees) {
            this.familleWithRole++;
          }
          if (famille.sousFamilles && famille.sousFamilles.length > 0) {
            this.totalSousF += famille.sousFamilles.length;
          }
        });
        this.familleWithRolePercent = Math.round((this.familleWithRole / this.filiere.familles.length) * 100);
      }
    });
  }

  openModal(template: TemplateRef<any>): any {
    this.filiereToEdit = Object.assign({}, this.filiere);
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, {class: 'modal-dialog-md'}, this.ngbModalOptions)
    );
  }

  openModalFamille(title: string, famille, template: TemplateRef<any>): any {
    this.modalTitle = title;
    this.famille = famille ? Object.assign({}, famille) : new Famille();
    this.famille.filiere = this.filiere;
    this.indexF = famille ? this.filiere.familles.indexOf(famille) : 0;
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, {class: 'modal-dialog-centered'}, this.ngbModalOptions)
    );
  }

  openModalSousFamille(title: string, sousFamille, template: TemplateRef<any>, famille): any {
    this.modalTitle = title;
    this.sousFamille = sousFamille ? Object.assign({}, sousFamille) : new SousFamille();
    if (!this.sousFamille.familleMere) {
      this.sousFamille.familleMere = new Famille();
    }
    this.sousFamille.familleMere.idFamille = famille.idFamille;
    this.sousFamille.familleMere.id = famille.id;
    this.indexF = this.filiere.familles.indexOf(famille);
    this.indexSF = sousFamille ? this.filiere.familles[this.indexF].sousFamilles.indexOf(sousFamille) : 0;
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, {class: 'modal-dialog-centered'}, this.ngbModalOptions)
    );
  }

  editFiliere(): any {
    if (this.checkFields(this.filiereToEdit)) {
      // @ts-ignore
      if (this.filiereToEdit.coordinateurDeFiliere === '') {
        this.filiereToEdit.coordinateurDeFiliere = null;
      }
      // @ts-ignore
      if (this.filiereToEdit.responsableDeFiliere === '') {
        this.filiereToEdit.responsableDeFiliere = null;
      }
      // @ts-ignore
      if (this.filiereToEdit.responsableDeFiliereDelegue === '') {
        this.filiereToEdit.responsableDeFiliereDelegue = null;
      }
      this.filiereService.putV1Filieres(this.filiereToEdit).subscribe(result => {
        this.filiere = result;
        DataService.changeFiliereData(this.filiere);
        this.hideModal();
        this.showSuccess();

      }, () => {
        this.showFailure('L\'opération a échoué. Veuillez réessayer à nouveau.');
      });
    } else {
      this.showError();
    }
  }

  saveFamille(): any {
    if (this.checkFields(this.famille)) {
      if (this.famille.responsableDeDonnees === undefined
        // @ts-ignore
        || this.famille.responsableDeDonnees === null || this.famille.responsableDeDonnees === '') {
        this.addBloc = true;
      } else {
        this.submitFamille();
      }
    } else {
      this.showError();
    }
  }

  saveSousFamille(): any {
    if (this.checkFields(this.sousFamille)) {
      if (this.sousFamille.responsableDeDonnees === undefined
        // @ts-ignore
        || this.sousFamille.responsableDeDonnees === null || this.sousFamille.responsableDeDonnees === '') {
        this.addBloc = true;
      } else {
        this.submitSousFamille();
      }
    } else {
      this.showError();
    }
  }

  showSuccess(): any {
    this.toastr.clear();
    this.toastr.success('Opération effectuée avec succès !');
  }

  showFailure(msg): any {
    this.toastr.clear();
    this.toastr.error(msg);
  }


  showError(): any {
    this.toastr.clear();
    this.toastr.error('Merci de remplir tous les champs obligatoires');
  }

  hideModal(): any {
    this.required = false;
    this.addBloc = false;
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }

  checkFields(object): boolean {
    if (object.nom === undefined || object.nom.toString() === '') {
      this.required = true;
      return false;
    } else {
      this.required = false;
      return true;
    }
  }

  isInvalid(value): boolean {
    return this.required && (value === undefined || value === '');
  }

  delete(famille, sousF): any {
    if (sousF == null) {
      this.deleteBackEnd(famille);
      this.filiere.familles.splice(this.indexF, 1);
      DataService.changeFiliereData(this.filiere);
    } else {
      this.deleteBackEnd(sousF);
      this.filiere.familles[this.indexF].sousFamilles.splice(this.indexSF, 1);
      DataService.changeFiliereData(this.filiere);
    }
    this.hideModal();
  }

  deleteBackEnd(famille: Famille): any {
    this.familleServie.deleteV1Famille(famille).subscribe(() => {
      this.showSuccess();
    }, () => {
      this.showFailure('L\'opération a échoué. Veuillez réessayer à nouveau.');
    });
  }

  submitFamille(): any {
    // @ts-ignore
    this.famille.responsableDeDonnees = this.famille.responsableDeDonnees === '' ? null : this.famille.responsableDeDonnees;
    if (this.famille.idFamille == null) {
      this.familleServie.postV1Famille(this.famille).subscribe(result => {
        result.isCollapsed = this.famille.isCollapsed;
        this.filiere.familles ? this.filiere.familles.push(result) :
          this.filiere.familles = new Array<Famille>(result);
        DataService.changeFiliereData(this.filiere);
        this.hideModal();
        this.showSuccess();
      }, () => {
        this.showFailure('L\'opération a échoué. Veuillez réessayer à nouveau.');
      });
    } else {
      this.familleServie.putV1Famille(this.famille).subscribe(result => {
        result.isCollapsed = this.famille.isCollapsed;
        this.filiere.familles[this.indexF] = result;
        DataService.changeFiliereData(this.filiere);
        this.hideModal();
        this.showSuccess();
      }, () => {
        this.showFailure('L\'opération a échoué. Veuillez réessayer à nouveau.');
      });
    }
  }

  submitSousFamille(): any {
    // @ts-ignore
    this.sousFamille.responsableDeDonnees = this.sousFamille.responsableDeDonnees === '' ? null : this.sousFamille.responsableDeDonnees;
    if (this.sousFamille.idFamille == null) {
      this.familleServie.postV1Famille(this.sousFamille).subscribe(result => {
        const famille = this.filiere.familles.filter(f => f.idFamille === +this.sousFamille.familleMere.idFamille)[0];
        this.filiere.familles[this.filiere.familles.indexOf(famille)].sousFamilles ? this.filiere.familles[this.filiere.familles.indexOf(famille)].sousFamilles.push(result) :
          this.filiere.familles[this.filiere.familles.indexOf(famille)].sousFamilles = new Array<Famille>(result);
        this.totalSousF++;
        DataService.changeFiliereData(this.filiere);
        this.hideModal();
        this.showSuccess();
      }, () => {
        this.showFailure('L\'opération a échoué. Veuillez réessayer à nouveau.');
      });
    } else {
      this.familleServie.putV1Famille(this.sousFamille).subscribe(result => {
        const indexFamille = this.filiere.familles.indexOf(this.filiere.familles.filter(f => f.idFamille === +this.sousFamille.familleMere.idFamille)[0]);
        if (this.indexF === indexFamille) {
          this.filiere.familles[this.indexF].sousFamilles[this.indexSF] = result;
        } else {
          this.filiere.familles[indexFamille].sousFamilles ? this.filiere.familles[indexFamille].sousFamilles.push(result) :
            this.filiere.familles[indexFamille].sousFamilles = new Array<Famille>(result);
        }

        DataService.changeFiliereData(this.filiere);
        this.hideModal();
        this.showSuccess();
      }, () => {
        this.showFailure('L\'opération a échoué. Veuillez réessayer à nouveau.');
      });
    }
  }

  uploadFile(event): any {
    const reader = new FileReader(); // HTML5 FileReader API
    const file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      reader.readAsDataURL(file);

      // When file uploads set it to file formcontrol
      reader.onload = () => {
        this.filiereToEdit.image = reader.result;
      };
    }
  }
}
