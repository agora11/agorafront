import { Component, OnInit, TemplateRef } from '@angular/core';
import { FilieresService } from '../services/filieres.service';
import { Filiere } from '../models/filiere';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UtilisateursService } from '../services/utilisateurs.service';
import { ToastrService } from 'ngx-toastr';
import { DataService } from '../services/data.service';
import {AuthGuard} from '../services/auth.service';
import {Utilisateur} from '../models/utilisateur';

@Component({
  selector: 'app-filieres',
  templateUrl: './filieres.component.html',
  styleUrls: ['./filieres.component.css']
})
export class FilieresComponent implements OnInit {

  filieres = new Array();
  filiere: Filiere;
  users: any[] = new Array();
  responsables: Utilisateur[] = new Array<Utilisateur>();
  keyword = 'nomComplet';
  modalRef: BsModalRef;
  ngbModalOptions = {
    backdrop: true,
    ignoreBackdropClick: true
  };
  isDisabled = false;
  required = false;
  imageUrl: any = '';
  connectedUser: any;
  constructor(private filiereService: FilieresService, private userService: UtilisateursService,
              private modalService: BsModalService, private dataService: DataService,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    AuthGuard.getConnectedUser().subscribe(currentUser => {
      this.connectedUser = currentUser;
    });
    this.userService.getV1Utilisateurs(undefined, undefined, 'prenom').subscribe(result => {
      result.forEach(user => {
        const u = { label: user.nomComplet, value: user.nomComplet };
        this.users.push(u);
      });
      this.responsables = result;
    });
    this.filiereService.getV1Filieres(undefined, undefined, undefined, 'nom').subscribe(result => {
      this.filieres = result;
    });
  }


  openModal(template: TemplateRef<any>, filiere, classModal): any {
    if (filiere) {
      this.filiere = filiere;
    } else {
      this.filiere = new Filiere();
    }
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: classModal }, this.ngbModalOptions)
    );
  }

  submit(): any {
    if (this.checkFields()) {
      // @ts-ignore
      if (this.filiere.coordinateurDeFiliere === '') {
        this.filiere.coordinateurDeFiliere = null;
      }
      // @ts-ignore
      if (this.filiere.responsableDeFiliere === '') {
        this.filiere.responsableDeFiliere = null;
      }
      // @ts-ignore
      if (this.filiere.responsableDeFiliereDelegue === '') {
        this.filiere.responsableDeFiliereDelegue = null;
      }

      this.filiereService.postV1Filieres(this.filiere).subscribe(result => {
        this.hideModal();
        this.showSuccess();
        this.filieres.push(result);
        this.filieres.sort((a, b) => (a.nom.toLowerCase() > b.nom.toLowerCase()) ? 1 : ((b.nom.toLowerCase() > a.nom.toLowerCase()) ? -1 : 0));
        this.filiere = new Filiere();
      }, error => {
        this.showFailure('L\'opération a échoué. Veuillez réessayer à nouveau.');
      });
    } else {
        this.showError();
    }
  }

  cancel(): any {
    this.hideModal();
    this.filiere = new Filiere();
  }

  hideModal(): any {
    this.required = false;
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }

  showSuccess(): any {
    this.toastr.clear();
    this.toastr.success('Opération effectuée avec succès !');
  }

  showFailure(msg): any {
    this.toastr.clear();
    this.toastr.error(msg);
  }

  showError(): any {
    this.toastr.clear();
    this.toastr.error('Merci de remplir tous les champs obligatoires');
  }

  changeDataService(filiere): any {
    DataService.changeFiliereData(filiere);
  }

  checkFields(): boolean {
    if (this.filiere.nom === undefined || this.filiere.nom.toString() === '') {
      this.required = true;
      return false;
    } else {
      this.required = false;
      return true;
    }
  }

  isInvalid(value): boolean {
    return this.required && (value === undefined || value === '');
  }

  uploadFile(event): any {
    const reader = new FileReader(); // HTML5 FileReader API
    const file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      reader.readAsDataURL(file);

      // When file uploads set it to file formcontrol
      reader.onload = () => {
        this.imageUrl = reader.result;
      }
    }
  }

  archiver(): any {
    this.filiereService.deleteV1Filiere(this.filiere.idFiliere).subscribe(() => {
      this.filieres = this.filieres.filter(item => item.idFiliere !== this.filiere.idFiliere);
      this.hideModal();
      this.showSuccess();
    }, () => { this.showFailure('L\'opération a échoué. Veuillez réessayer à nouveau.');
    });
  }

  showButton(action: string, filiere: any): boolean {
     return this.connectedUser.isAdmin || filiere.responsableDeFiliere?.idUtilisateur === this.connectedUser?.idUtilisateur
       || filiere.responsableDeFiliereDelegue?.idUtilisateur === this.connectedUser?.idUtilisateur
          || filiere.coordinateurDeFiliere?.idUtilisateur === this.connectedUser?.idUtilisateur;
  }
}
