import { CanDeactivate } from '@angular/router';
import {GisementComponent} from './gisement.component';
import {DataService} from '../../services/data.service';

export class DesactivateGuardGisement implements CanDeactivate<GisementComponent> {
  isModified = false;
  canDeactivate(component: GisementComponent): any {
    DataService.getIsModified().subscribe( isModified => this.isModified = isModified );

    return this.isModified ? component.canDeactivate() : true;
  }
}
