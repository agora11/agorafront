import {Component, OnInit, TemplateRef} from '@angular/core';
import {Acteur, Gisement, LienUtile} from '../../models/gisement';
import {GisementsService} from '../../services/gisements.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {DataService} from '../../services/data.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {FilieresService} from '../../services/filieres.service';

@Component({
  selector: 'app-gisement',
  templateUrl: './gisement.component.html',
  styleUrls: ['./gisement.component.css']
})
export class GisementComponent implements OnInit {
  titre = 'Création d’un gisement';
  titre2 = '';
  gisement = new Gisement();
  isDisabled = false;
  acteur = new Acteur();
  lienUtile = new LienUtile();
  modalRef: BsModalRef;
  required = false;
  filieres = [];
  ngbModalOptions = {
    backdrop: true,
    ignoreBackdropClick: true
  };
  public isModified: boolean;

  constructor(private gisementService: GisementsService, private toastr: ToastrService,
              private actRoute: ActivatedRoute,  private router: Router, private modalService: BsModalService,
              private filiereService: FilieresService) { }

  ngOnInit(): void {
    this.setIsModified(false);
    DataService.getIsModified().subscribe(result => this.isModified = result);
    this.filiereService.getV1Filieres(undefined, undefined, undefined, 'nom').subscribe(result => {
        this.filieres = result;
    });

    this.actRoute.queryParams.subscribe(params => {
      if (params.action === 'consult') {
        this.isDisabled = true;
        this.getCurrentData();
        this.titre = 'Gisement';
        this.titre2 = ' - ' + this.gisement.libelle;
      } else if (params.action === 'edit') {
        this.isDisabled = false;
        this.getCurrentData();
        this.titre = 'Gisement';
        this.titre2 = ' - ' + this.gisement.libelle;
      } else {
        this.gisement = new Gisement();
        this.isDisabled = false;
        this.titre = 'Création d’un gisement';
        this.titre2 = '';
      }
    });
  }

  save(): any {
    if (this.checkFields()) {
      if (this.gisement.idGisement === undefined) {
        this.gisementService.postV1Gisement(this.gisement).subscribe(result => {
          this.showSuccess();
          this.gisement = result;
          this.refreshPage();

        }, () => {
          this.showFailure('L\'opération a échoué. Veuillez réessayer à nouveau.');
        });
      } else {
        this.gisementService.putV1Gisement(this.gisement).subscribe(result => {
          this.showSuccess();
          this.gisement = result;
          this.refreshPage();
        }, () => {
          this.showFailure('L\'opération a échoué. Veuillez réessayer à nouveau.');
        });
      }
    } else {
      this.showFailure('Merci de remplir tous les champs obligatoires');
    }
  }

  showSuccess(): any {
    this.toastr.clear();
    this.toastr.success('Opération effectuée avec succès !');
  }

  showFailure(msg): any {
    this.toastr.clear();
    this.toastr.error(msg);
  }

  ajouterActeur(): any {
    this.gisement.acteurs = this.gisement.acteurs ? this.gisement.acteurs : new Array<Acteur>();
    this.gisement.acteurs.push(JSON.parse(JSON.stringify(this.acteur)));
    this.acteur = new Acteur();
    this.setIsModified(true);
  }

  ajouterLien(): any {
    this.gisement.liensUtiles =  this.gisement.liensUtiles ?  this.gisement.liensUtiles : new Array<LienUtile>();
    this.gisement.liensUtiles.push(JSON.parse(JSON.stringify(this.lienUtile)));
    this.lienUtile = new LienUtile();
    this.setIsModified(true);
  }

  removeItem(list: Array<any>, index): any {
    list.splice(index, 1);
    this.setIsModified(true);
  }

  private getCurrentData(): any {
    DataService.getCurrentGisement().subscribe(gisement => {
      if (gisement.idGisement) {
        this.gisement = gisement;
        localStorage.setItem('currentData', JSON.stringify(this.gisement));
      } else {
        this.gisement = JSON.parse(localStorage.getItem('currentData'));
      }
    });
  }

  private refreshPage(): any {
    this.setIsModified(false);
    DataService.changeCurrentGisement(this.gisement);
    this.router.navigate(['/main/gisement'], {
      relativeTo: this.actRoute,
      queryParams: {
        action:  'edit'
      }
    });
  }

  redirect(): any {
    this.setIsModified(false);
    this.hideModal();
    this.router.navigateByUrl('/main/gisements');
  }

  hideModal(): any {
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }

  openModal(template: TemplateRef<any>, templateClass): any {
    this.filieres.forEach(f => f.associated = false);
    this.gisement.filieres?.forEach(gfiliere => {
        const filtredTerme = this.filieres.filter(t => t.idFiliere === gfiliere.idFiliere)[0];
        if (filtredTerme) { filtredTerme.associated = true; }
      });

    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: templateClass }, this.ngbModalOptions)
    );
  }

  isInvalid(libelle: string): boolean {
    return this.required && (libelle === undefined || libelle === '');
  }

  checkFields(): boolean {
    if (this.gisement.libelle === undefined || this.gisement.libelle?.toString() === '') {
      this.required = true;
      return false;
    } else {
      this.required = false;
      return true;
    }
  }

  associerFilieres(): any {
    this.gisement.filieres = this.filieres.filter(o => o.associated);
    this.modalRef.hide();
    this.setIsModified(true);
  }

  removeFiliere(i: number): any {
    this.gisement.filieres.splice(i, 1);
    this.setIsModified(true);
  }

  setIsModified(value: boolean): any {
    DataService.setIsModified(value);
  }

  canDeactivate(): any {
    return confirm('Êtes-vous sûr(e) de vouloir quitter la page ? \n' +
      'En cliquant sur le bouton « OK », vous perdrez ' +
      'toutes les informations apportées à cette fiche.');
  }
}
