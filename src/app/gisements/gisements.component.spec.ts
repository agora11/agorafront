import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GisementsComponent } from './gisements.component';

describe('GisementsComponent', () => {
  let component: GisementsComponent;
  let fixture: ComponentFixture<GisementsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GisementsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GisementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
