import {Component, OnInit, TemplateRef} from '@angular/core';
import {Gisement} from '../models/gisement';
import {GisementsService} from '../services/gisements.service';
import {FilieresService} from '../services/filieres.service';
import {DataService} from '../services/data.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {ToastrService} from 'ngx-toastr';
import {AuthGuard} from '../services/auth.service';

@Component({
  selector: 'app-gisements',
  templateUrl: './gisements.component.html',
  styleUrls: ['./gisements.component.css']
})
export class GisementsComponent implements OnInit {
  titre = 'Gisements';
  gisements: Array<Gisement>;
  filieres = new Array<any>();
  statuts =  [{label: 'En projet', value: 'En projet'}, {label: 'En service', value: 'En service'}];
  gisement: any;
  modalRef: BsModalRef;
  public connecterUser: any;
  isResponsable = false;
  ngbModalOptions = {
    backdrop: true,
    ignoreBackdropClick: true
  };
  constructor(private gisementService: GisementsService, private filiereService: FilieresService,
              private modalService: BsModalService, private toastr: ToastrService) { }

  ngOnInit(): void {
    AuthGuard.getConnectedUser().subscribe(result => {
      this.connecterUser = result;
      this.isResponsable =  this.connecterUser?.roles?.filter(role => role.nom ===
      'Responsable de filière' || role.nom === 'Responsable de filière délégué'
        || role?.nom === 'Coordinateur de filière' || role?.nom === 'Chargé d\'affaires Data Office'
        || role?.nom === 'Chargé Qualité Data Office' || role?.nom === 'Responsable pôle affaires'
        || role?.nom === 'Responsable pôle qualité' || role?.nom === 'Système d’information architecte')?.length > 0;

    });
    this.filiereService.getV1Filieres(undefined, undefined, undefined, 'nom').subscribe(result => {
      result.forEach(item => {
        const filiere = { label: item.nom, value: item.nom };
        this.filieres.push(filiere);
      });
    });
    this.gisementService.getV1Gisements(undefined, undefined, 'libelle').subscribe(result => {
      this.gisements = result;
      this.gisements.forEach(gisement => {
        gisement.filieres.sort((a, b) => (a.nom.toLowerCase() > b.nom.toLowerCase()) ? 1 : ((b.nom.toLowerCase() > a.nom.toLowerCase()) ? -1 : 0));
        gisement.filiere = '';
        gisement.filiere += gisement.filieres.map(item => item.nom);
      });
    });
  }

  changeData(gisement: any): any {
    DataService.changeCurrentGisement(gisement);
  }

  archiveGisement(): any {
    this.gisementService.deleteV1GisementsId(this.gisement.idGisement).subscribe(() => {
      this.gisements = this.gisements.filter(gisement => gisement.idGisement !== this.gisement.idGisement);
      this.modalRef?.hide();
      this.showSuccess();
    }, () => {
      this.showFailure();
    });
  }

  openModal(gisement, template: TemplateRef<any>): any {
    this.gisement = gisement;
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'modal-dialog-centered' }, this.ngbModalOptions)
    );
  }
  showSuccess(): any {
    this.toastr.clear();
    this.toastr.success('Opération effectuée avec succès !');
  }

  showFailure(): any {
    this.toastr.clear();
    this.toastr.error( 'L\'opération a échoué. Veuillez réessayer à nouveau.');
  }
}
