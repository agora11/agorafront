import { Component, OnInit, TemplateRef } from '@angular/core';
import {NavigationExtras, Router} from '@angular/router';
import { AuthenticationService } from '@tec/condor/services';
import { BehaviorSubject } from 'rxjs';
import { HeaderService } from './header.service';
import {Utilisateur} from '../../models/utilisateur';
import {FilieresService} from '../../services/filieres.service';
import {Filiere} from '../../models/filiere';
import {AuthGuard} from '../../services/auth.service';
import {DataService} from "../../services/data.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  motif = '';
  userName: BehaviorSubject<string>;
  authorized = false;
  acteur: Utilisateur;
  motifStorage = '';
  constructor(private headerService: HeaderService,
              private router: Router,
              private authenticationService: AuthenticationService) {
    this.userName = this.headerService.getFullName();
  }

  ngOnInit(): void {
    this.motifStorage = localStorage.getItem('item');
    AuthGuard.getConnectedUser().subscribe(acteur => {
      if (acteur?.idUtilisateur) {
        this.acteur = acteur;
        this.acteur.filiereAssoc = new Array<Filiere>();
        if ( this.acteur.filieresResponsable?.length > 0) {
          this.acteur.filiereAssoc =  this.acteur.filieresResponsable;
          this.authorized = true;
        }
        if ( this.acteur.filieresResponsableDelegue?.length > 0) {
          this.acteur.filieresResponsableDelegue.forEach(filiere => {
            if ( this.acteur.filiereAssoc?.filter(f => f.idFiliere === filiere.idFiliere)?.length === 0) {
              this.acteur.filiereAssoc.push(filiere);
            }
          });
          this.authorized = true;
        }
        if ( this.acteur.filieresCoordinateur?.length > 0) {
          this.acteur.filieresCoordinateur.forEach(filiere => {
            if ( this.acteur.filiereAssoc?.filter(f => f.idFiliere === filiere.idFiliere)?.length === 0) {
              this.acteur.filiereAssoc.push(filiere);
            }
          });
          this.authorized = true;
        }
        localStorage.setItem('utilisateur', JSON.stringify(this.acteur));
      }
    }, () => {},  () => {
      AuthGuard.setConnectedUser(this.acteur);
    });
  }

  search(): void {
    if (this.motif !== ''){
      localStorage.setItem('item', this.motif);
      const searchValues = { filiere: [], roles: [], searchValue: this.motif, component: '' };
      DataService.pushSearch(searchValues);
      // this.router.navigate(['main/search/' + item]);
      this.router.navigate(['main/search']);
    }
  }

  logOut() {
    localStorage.clear();
    this.authenticationService.logout();
    this.router.navigateByUrl('/login-caiman');
  }
}
