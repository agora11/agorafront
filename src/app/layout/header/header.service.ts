
import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { FilieresService } from 'src/app/services/api';
import { Filiere } from 'src/app/models/filiere';


@Injectable()
export class HeaderService {

  private fullName = new BehaviorSubject("");

  constructor(private filieresService: FilieresService) { }


  getAllFilieres(): Observable<Filiere[]> {
    return this.filieresService.getV1Filieres();
  }

  public getFullName(): BehaviorSubject<string> {
    return this.fullName;
  }

  public addFullName(value: string) {
    this.fullName.next(value);
  }
}
