import { Component, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { Objet } from '../models/objet';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DataService } from '../services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import {FilieresService, ObjetsService} from '../services/api';
import { ToastrService } from 'ngx-toastr';
import {  map } from 'rxjs/operators';
import {Status} from '../models/status';
import {AuthGuard} from '../services/auth.service';

@Component({
  selector: 'app-list-objet',
  templateUrl: './list-objet.component.html',
  styleUrls: ['./list-objet.component.css']
})
export class ListObjetComponent implements OnInit, OnDestroy {
  modalRef: BsModalRef;
  objets: Objet[];
  objet: Objet;
  totalItems = 0;
  filteredObjets: Objet[];
  objetSubscription: any;
  filieres = new Array<any>();
  statuts = Status.list;

  constructor(private actRoute: ActivatedRoute,
              private modalService: BsModalService,
              private dataService: DataService,
              private router: Router,
              private objetService: ObjetsService,
              private toastr: ToastrService, private filiereService: FilieresService) { }

  ngOnDestroy(): void {
    this.objetSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.filiereService.getV1Filieres(undefined, undefined, undefined, 'nom').subscribe(result => {
      result.forEach( f => {
        const filiere = {label: f.nom, value: f.nom};
        this.filieres.push(filiere);
      });
    });
    this.objetSubscription = this.actRoute.data.pipe(map(item => item.objetList)).subscribe(data => {
        if (data) {
          this.objets = data;
          this.objets.forEach(objet => {
            objet.definition = objet.definitions[0]?.definition;
            objet.filiere = objet.filieres[0]?.nom;
          });
          this.totalItems = this.objets.length;
        } else {
          this.objets = [];
          this.totalItems = 0;
        }
      });
  }

  openModal(objet, template: TemplateRef<any>): any {
    this.objet = objet;
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'modal-dialog-centered' })
    );
  }

  showSuccess(): any {
    this.toastr.success('Opération effectuée avec succès !');
  }

  showFailure(): any {
    this.toastr.error('L\'opération a échoué. Veuillez réessayer à nouveau.');
  }

  archiveObjet(objet: Objet): any {
    this.objetService.deleteV1ObjetsId(objet.idClasseDobjet).subscribe(() => {
        this.objets = this.objets.filter(item => item.id !== objet.id);
        this.totalItems = this.objets.length;
        this.showSuccess();
        this.modalRef.hide();
      }, () => {
        this.showFailure();
        this.modalRef.hide();
      });
  }

  editObjet(objet: Objet): any {
    this.objetService.getV1ObjetsId(objet.idClasseDobjet).subscribe(result =>
     DataService.changeObjetData(result)
    );
  }

  showButton(objet, action: string): boolean {
    if (action === 'edit') {
      return  objet.statut?.libelle === Status.BROUILLON
                 && (AuthGuard.isAdmin() || AuthGuard.isValideur(objet.filieres[0]) || AuthGuard.isRedacteur(objet.filieres[0]));
    } else if (action === 'archiver') {
      return AuthGuard.isAdmin() || AuthGuard.isValideur(objet.filieres[0]);
    }
  }
}
