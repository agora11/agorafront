import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { Terme } from '../models/terme';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../services/data.service';
import { TermesService } from '../services/termes.service';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';
import { FilieresService } from '../services/filieres.service';
import {Status} from '../models/status';
import {AuthGuard} from '../services/auth.service';
import {Table} from "primeng";

@Component({
  selector: 'app-terme-list',
  templateUrl: './list-terme.component.html',
  styleUrls: ['./list-terme.component.css']
})
export class ListTermeComponent implements OnInit, OnDestroy {
  modalRef: BsModalRef;
  termes: Terme[];
  terme: Terme;
  @ViewChild('myTable') myTable: Table;
  termeSubscription: any;
  filieres = new Array<any>();
  statuts = Status.list;
  constructor(private modalService: BsModalService, private router: Router, private dataService: DataService,
              private termeService: TermesService, private toastr: ToastrService, private filiereService: FilieresService,
              private actRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.filiereService.getV1Filieres(undefined, undefined, undefined, 'nom').subscribe(result => {
      result.forEach(f => {
        const filiere = { label: f.nom, value: f.nom };
        this.filieres.push(filiere);
      });
    });
    DataService.getCurrentTerme().subscribe(response => {
      if (response.libelle){
        console.log('table ', this.myTable);
      }
    });
    this.termeSubscription = this.actRoute.data.pipe(map(item => item.termeList)).subscribe(data => {
        if (data) {
          this.termes = data;
          this.termes.forEach(terme => {
            terme.definition = terme.definitions[0]?.definition;
            terme.filiere = terme.filieres[0]?.nom;
          });
        } else {
          this.termes = [];
        }
      });
  }

  openModal(terme, template: TemplateRef<any>): any {
    this.terme = terme;
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'modal-dialog-centered' })
    );
  }

  editTerme(terme: Terme): any {
    DataService.changeTermeData(terme);
  }

  archiveTerme(terme: Terme): any {
    this.termeService.deleteV1TermesId(terme.idTerme).toPromise().then(() => {
      this.termes = this.termes.filter(item => item.id !== terme.id);
      this.showSuccess();
      this.modalRef.hide();
    }, () => {
      this.modalRef.hide();
      this.showFailure();
    });
  }

  showSuccess(): any {
    this.toastr.clear();
    this.toastr.success('Opération effectuée avec succès !');
  }

  showFailure(): any {
    this.toastr.clear();
    this.toastr.error('L\'opération a échoué. Veuillez réessayer à nouveau.');
  }

  ngOnDestroy(): any {
    this.termeSubscription.unsubscribe();
  }

  showButton(terme, action: string): boolean {
    if (action === 'edit') {
      return  terme.statut?.libelle === Status.BROUILLON
        && (AuthGuard.isAdmin() || AuthGuard.isValideur(terme.filieres[0]) || AuthGuard.isRedacteur(terme.filieres[0]));
    } else if (action === 'archiver') {
      return AuthGuard.isAdmin() || AuthGuard.isValideur(terme.filieres[0]);
    }
  }
}
