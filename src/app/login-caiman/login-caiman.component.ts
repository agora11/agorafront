import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '@tec/condor/services';
import { AppComponent } from '../app.component';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-login-caiman',
  templateUrl: './login-caiman.component.html',
  styleUrls: ['./login-caiman.component.css']
})
export class LoginCaimanComponent implements OnInit {
  cp: any;

  constructor(private route: ActivatedRoute,
    private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
        this.cp = params.cp;
      }
    );
  }

  login(){
    this.authenticationService.login("/main/termes");
  }

}
