import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AuthenticationService } from '@tec/condor/services';
import { HeaderService } from '../layout/header/header.service';
import { UtilisateursService } from '../services/utilisateurs.service';
import {AuthGuard} from '../services/auth.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  title = 'agora';

  constructor(private router: Router,
              private utilisateurService: UtilisateursService,
              private authenticationService: AuthenticationService,
              private headerService: HeaderService) {
  }
  ngOnInit(): void {
    this.authenticationService.authenticatedSubject.subscribe(() => {
      if (this.authenticationService.isAuthenticated() || this.isAuthenticated()) {
        const helper = new JwtHelperService();
        const decodedToken = helper.decodeToken(localStorage.getItem('myapp.id.token'));
        this.utilisateurService.getV1Utilisateurs(undefined, undefined, undefined, undefined, undefined, decodedToken.uid_unifie)
          .subscribe(data => {
            if (data == null || data.length === 0) {
              localStorage.clear();
              this.router.navigate(['/login-caiman'], { queryParams: { cp: decodedToken.uid_unifie } });
            } else {
              this.headerService.addFullName(`${data[0].prenom} ${data[0].nom}`);
              AuthGuard.setConnectedUser(data[0]);
            }
          }, () => {
            localStorage.clear();
            this.router.navigate(['/login-caiman'], { queryParams: { cp: decodedToken.uid_unifie } });
          });
      }
    });
  }

  isAuthenticated(): boolean {
    return localStorage.getItem('utilisateur') != null;
  }




}
