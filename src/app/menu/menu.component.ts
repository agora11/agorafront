import { Component, OnInit } from '@angular/core';
import {DataService} from '../services/data.service';
import {Menu, MenuList} from '../models/menu';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  dropdown = false;
  menuItem = 'Glossaire';
  menuList = MenuList.menuList;
  constructor() {}

  ngOnInit(): void {
    DataService.getMenuItem().subscribe(data => this.menuItem = data);

  }
}
