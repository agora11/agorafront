import { CanDeactivate } from '@angular/router';
import {DataService} from '../services/data.service';
import {ModelisationComponent} from './modelisation.component';

export class DesactivateGaurdModelisation implements CanDeactivate<ModelisationComponent> {
  isModified = false;
  canDeactivate(component: ModelisationComponent): any {
    DataService.getIsModified().subscribe( isModified => this.isModified = isModified );

    return this.isModified ? component.canDeactivate() : true;
  }
}
