import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';

import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Status } from '../../models/status';
import { FilieresService } from '../../services/filieres.service';
import { ModelisationService } from '../../services/modelisation.service';
import { Diagramme } from '../../models/diagramme';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-modelisation',
  templateUrl: './diagrammes.component.html',
  styleUrls: ['./diagrammes.component.css']
})
export class DiagrammesComponent implements OnInit {
  public diagrammes = [];
  public filieres = [];
  public statuts = Status.list;
  public diagramme = new Diagramme();
  public modalRef: BsModalRef;
  @ViewChild('Diagramme') diagramTemplate: TemplateRef<any>;
  filiereSelectConfig = {
    labelField: 'nom',
    valueField: 'nom',
    searchField: ['nom'],
    dropdownDirection: 'down',
    sortField: [{ field: 'nom', direction: 'asc' }],
  };
  ngbModalOptions = {
    backdrop: true,
    ignoreBackdropClick: true
  };
  public required: boolean;
  constructor(private filiereService: FilieresService, private toastr: ToastrService,
              private modelisationService: ModelisationService, private router: Router, private modalService: BsModalService/*, private createDiagramFormBuilder: FormBuilder*/) {
  }

  ngOnInit(): void {
    this.filiereService.getV1Filieres(undefined, undefined, undefined, 'nom').subscribe(result => {
        this.filieres = result;
        this.filieres.forEach(filiere => {
        filiere.label = filiere.nom, filiere.value = filiere.nom;
      });
    });
    this.modelisationService.getAllDiagrams()
      .subscribe(result => {this.diagrammes = result;
        this.diagrammes.sort((a, b) => (a.libelle.toLowerCase() > b.libelle.toLowerCase()) ? 1 : ((b.libelle.toLowerCase() > a.libelle.toLowerCase()) ? -1 : 0));
      });
  }
  openDiagramToEdit(diagram: any, action): void {
    this.router.navigate(['main/modelisation'], {queryParams: {action}});
    localStorage.setItem('currentDiagram', JSON.stringify(diagram));
  }

  openModal(diagramme, template: TemplateRef<any>): any {
    if (diagramme) {
      this.diagramme = diagramme;
    } else {
      this.diagramme = new Diagramme();
    }
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'modal-dialog-centered' }, this.ngbModalOptions)
    );
  }

  saveDiagramme(): void {
    if (this.checkFields()) {
      this.modelisationService.saveDiagram(this.diagramme).subscribe(result => {
        this.modalRef.hide();
        this.showSuccess();
        localStorage.setItem('currentDiagram', JSON.stringify(result));
        this.router.navigate(['main/modelisation'], /*{state: {diagramDetails: result}}*/);

      }, () => {
        this.showFailure('L\'opération a échoué. Veuillez réessayer à nouveau.\'');
      });
    } else {
      this.showFailure('Merci de remplir tous les champs obligatoires');
    }

  }

  showSuccess(): any {
    this.toastr.clear();
    this.toastr.success('Opération effectuée avec succès !');
  }

  showFailure(msg): any {
    this.toastr.clear();
    this.toastr.error(msg);
  }

  archiveDiagramme(diagram: any): any {
    this.modelisationService.archiveDiagram(diagram).subscribe(
      result => {
        this.diagrammes = this.diagrammes.filter(item => item.diagramId !== this.diagramme.diagramId);
        this.showSuccess();
        this.modalRef.hide();
      },
      () => {
        this.showFailure('L\'opération a échoué. Veuillez réessayer à nouveau.\'');
      }
    );
  }

  isInvalid(value): boolean {
    return this.required && (value === undefined || value === '');
  }

  checkFields(): boolean {
    if (this.diagramme.libelle?.toString() === '' || this.diagramme.libelle === undefined ||
      this.diagramme.filiere === undefined || this.diagramme.filiere?.toString() === '') {
      this.required = true;
      return false;
    } else {
      this.required = false;
      return true;
    }
  }
}
