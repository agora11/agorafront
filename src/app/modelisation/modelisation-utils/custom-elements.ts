import * as joint from 'jointjs';
import {
  CONTAINER_HEADER_HEIGHT,
  CONTAINER_HEIGHT,
  CONTAINER_WIDTH, EXPANDED_OBJECT_HEIGHT,
  HEADER_BUTTON_SIZE, OBJECT_HEADER_HEIGHT,
  OBJECT_HEIGHT, OBJECT_WIDTH_DIAGRAM,
  OBJECT_WIDTH_PALETTE
} from './Constants';
import {DataService} from '../../services/data.service';


export var Container = joint.dia.Element.define('container.Parent', {
  collapsed: false,
  z: 1,
  size: {width: CONTAINER_WIDTH, height: CONTAINER_HEIGHT},
  //
  attrs: {
    root: {
      magnet: false
    },
    shadow: {
      refWidth: '100%',
      refHeight: '100%',
      x: 3,
      y: 3,
      fill: '#000000',
      opacity: 0.05
    },
    body: {
      refWidth: '100%',
      refHeight: '100%',
      strokeWidth: 1,
      stroke: '#DDDDDD',
      fill: '#FCFCFC',
    },
    header: {
      refWidth: '100%',
      height: CONTAINER_HEADER_HEIGHT,
      strokeWidth: 0.5,
      stroke: '#A1006B',
      fill: '#A1006B'
    },
    headerText: {
      textVerticalAnchor: 'middle',
      textAnchor: 'middle',
      refX: '50%',
      refY: CONTAINER_HEADER_HEIGHT / 2,
      fontSize: 16,
      fontFamily: 'sans-serif',
      letterSpacing: 1,
      fill: '#FFFFFF',
      textWrap: {
        width: -40,
        maxLineCount: 1,
        ellipsis: '*'
      },
      style: {
        textShadow: '1px 1px #222222',
      }
    },
    button: {
      refDx: -HEADER_BUTTON_SIZE - (CONTAINER_HEADER_HEIGHT - HEADER_BUTTON_SIZE) / 2,
      refY: (CONTAINER_HEADER_HEIGHT - HEADER_BUTTON_SIZE) / 2,
      cursor: 'pointer',
      event: 'element:button:pointerdown',

    },
    buttonBorder: {
      width: HEADER_BUTTON_SIZE,
      height: HEADER_BUTTON_SIZE,
      fill: '#000000',
      fillOpacity: 0.2,
      stroke: '#FFFFFF',
      strokeWidth: 0.5,
    },
    buttonIcon: {
      fill: 'red',
      stroke: '#FFFFFF',
      strokeWidth: 1,
      d: 'M 2 2 L 12 12 M 2 12 L 12 2'
    }
  }
}, {

  markup: [{
    tagName: 'rect',
    selector: 'shadow'
  }, {
    tagName: 'rect',
    selector: 'body'
  }, {
    tagName: 'rect',
    selector: 'header'
  }, {
    tagName: 'text',
    selector: 'headerText'
  }, {
    tagName: 'g',
    selector: 'button',
    children: [{
      tagName: 'rect',
      selector: 'buttonBorder'
    }, {
      tagName: 'path',
      selector: 'buttonIcon'
    }]
  }],
});


export var Shape = joint.dia.Element.define('example.Shape', {
  id: '',
  z: 2,
  associationType: true,
  size: {
    width: OBJECT_WIDTH_PALETTE,
    height: OBJECT_HEIGHT
  },
  collapsed: true,
  attrs: {
    root: {
      magnet: true,
      highlighterSelector: 'root'
    },
    header: {
      refWidth: '100%',
      refHeight: '100%',
      fill: '#7777771A',
      stroke: 'none',
      /*strokeWidth: 2*/
      rx: 4,
      ry: 4,

    },
    body: {
      refWidth: '100%',
      refHeight: '0%',
      fill: '#7777771A',
      stroke: 'none',
      /*strokeWidth: 2*/
      rx: 4,
      ry: 4,
    }, label: {
      textVerticalAnchor: 'middle',
      textAnchor: 'middle',
      refX: '50%',
      refY: '50%',
      /*refX: '45%',
      refY: '20%',*/
      fontSize: 13,
      fill: '#AFA59B',
    }, bodyText: {
      textVerticalAnchor: 'top',
      textAnchor: 'left',
      refX: '5%',
      refY: '30%',
      fontSize: 12,
      fill: 'transparent',
    }, topTool: {
      width: OBJECT_WIDTH_PALETTE,
      height: 1,
      cx: 0,
      cy: 0,
      fill: 'transparent',
      stroke: 'transparent',
      strokeWidth: 2,
      magnet: true,
      highlighterSelector: 'root',
    },
    bottomTool: {
      width: OBJECT_WIDTH_PALETTE,
      height: 1,
      refY: OBJECT_HEADER_HEIGHT,
      fill: 'transparent',
      stroke: 'transparent',
      strokeWidth: 2,
      magnet: true,
      highlighterSelector: 'root',
    }, leftTool: {
      width: 1,
      height: OBJECT_HEADER_HEIGHT,
      refX: 0,
      refY: 0,
      fill: 'transparent',
      stroke: 'transparent',
      strokeWidth: 2,
      magnet: true,
      highlighterSelector: 'root',
    }, rightTool: {
      width: 1,
      height: OBJECT_HEADER_HEIGHT,
      refX: OBJECT_WIDTH_PALETTE,
      refY: 0,
      fill: 'transparent',
      stroke: 'transparent',
      strokeWidth: 2,
      magnet: true,
      highlighterSelector: 'root',
    },
    button: {
      refDx: -HEADER_BUTTON_SIZE - (CONTAINER_HEADER_HEIGHT - HEADER_BUTTON_SIZE) / 2 + 5,
      refY: (CONTAINER_HEADER_HEIGHT - HEADER_BUTTON_SIZE) / 2 + 10,
      cursor: 'pointer',
      event: 'element:objbutton:pointerdown',

    },
    buttonBorder: {
      width: HEADER_BUTTON_SIZE,
      height: HEADER_BUTTON_SIZE,
      fill: '#000000',
      fillOpacity: 0.2,
      stroke: '#FFFFFF',
      strokeWidth: 0.5,
    },
    buttonIcon: {
      fill: 'red',
      stroke: '#FFFFFF',
      strokeWidth: 1,
      d: 'M 2 7 12 7 M 7 2 7 12'
    }

  }
}, {
  markup: [
    {
      tagName: 'rect',
      selector: 'header'
    },
    {
      tagName: 'rect',
      selector: 'body',
    },
    {
      tagName: 'text',
      selector: 'bodyText'
    },
    {
      tagName: 'rect',
      selector: 'topTool'
    },
    {
      tagName: 'rect',
      selector: 'bottomTool'
    },
    {
      tagName: 'rect',
      selector: 'leftTool'
    },
    {
      tagName: 'rect',
      selector: 'rightTool'
    },
   {
      tagName: 'text',
      selector: 'label'
    }, {
      tagName: 'g',
      selector: 'button',
      children: [{
        tagName: 'rect',
        selector: 'buttonBorder'
      }, {
        tagName: 'path',
        selector: 'buttonIcon'
      }]
    }
  ], toggle: function(shouldCollapse) {
    var buttonD;
    var collapsed = (shouldCollapse === undefined) ? !this.get('collapsed') : shouldCollapse;
    if (collapsed) {
      buttonD = 'M 2 7 12 7 M 7 2 7 12';
      this.resize(OBJECT_WIDTH_DIAGRAM, OBJECT_HEADER_HEIGHT);
      this.attr({
        body: {refHeight: '0%'},
        /*        label: {fill: '#AFA59B'},*/
        header: {
          refHeight: '100%',
        },
        label: {
          refX: '50%',
          refY: '50%',
        }, bodyText: {
          refX: '5%',
          refY: '30%',
          fill: 'transparent',
        },
        bottomTool: {
          refY: OBJECT_HEADER_HEIGHT
        }, leftTool: {
          height: OBJECT_HEADER_HEIGHT
        }, rightTool: {
          height: OBJECT_HEADER_HEIGHT
        }
      });
      var padding = 10;
    } else {
      buttonD = 'M 2 7 12 7';
      const attributesNumber = this.attributes.attrs.bodyText.textWrap.text.split('\n').length - 1;
      const objectHeight = OBJECT_HEADER_HEIGHT + 15 * attributesNumber + 35 ;
      const headerRefHeightPercentage = 100 / (objectHeight / OBJECT_HEADER_HEIGHT);
      const bodyRefHeightPercentage = 100 - headerRefHeightPercentage ;

      this.resize(OBJECT_WIDTH_DIAGRAM, objectHeight);
      this.attr({
        body: {
          refY: /*'25%'*/ headerRefHeightPercentage + '%',
          refHeight: /*'75%'*/ bodyRefHeightPercentage + '%',
          fill: '#EFE0EE'
        },
        /*        label: {fill: '#AFA59B'},*/
        header: {
          refHeight: /*'25%'*/ headerRefHeightPercentage + '%', fill: '#A1006B'
        },
        label: {
          refY: /*'12.5%'*/ (headerRefHeightPercentage / 2) + '%' ,
        },
        bodyText: {
          refX: '5%',
          refY: /*'37%'*/ attributesNumber < 5 ? (headerRefHeightPercentage + 9) + '%'
                         : attributesNumber <= 8 ? (headerRefHeightPercentage + 6) + '%'
                          : attributesNumber < 12 ? (headerRefHeightPercentage + 4) + '%'
                          : (headerRefHeightPercentage + 3) + '%' ,
          fill: '#A1006B',
        },
        bottomTool: {
          refY: objectHeight
        }, leftTool: {
          height: objectHeight
        }, rightTool: {
          height: objectHeight
        }
      });
    }
    this.attr(['buttonIcon', 'd'], buttonD);
    this.set('collapsed', collapsed);
  },

});

export var ReverseDirectionButton = joint.linkTools.Button.extend({

    name: 'reverse-button',

    options: {
      focusOpacity: 0.5,
      distance: 80,
      action: (evt, linkView, toolView) => {

        DataService.setIsModified(true);
        if (linkView.model.get('source') && linkView.model.get('target')) {
          const tempSource = linkView.model.get('source').id;
          const tempTarget = linkView.model.get('target').id;
          const tempSourceView = linkView.sourceView;
          const tempTargetView = linkView.targetView;
          linkView.sourceView = tempTargetView;
          linkView.targetView = tempSourceView;
          const sourcePoint = linkView.sourcePoint;
          const targetPoint = linkView.targetPoint;
          linkView.sourcePoint = targetPoint;
          linkView.targetPoint = sourcePoint;
          const sourceAnchor = linkView.sourceAnchor;
          const targetAnchor = linkView.targetAnchor;
          linkView.sourceAnchor = targetAnchor;
          linkView.targetAnchor = sourceAnchor;
          linkView.model.attributes.source.id = tempTarget;
          linkView.model.attributes.target.id = tempSource;
          linkView.update(linkView.model, {source: tempTarget, target: tempSource});
        }
      },
      markup: [{
        tagName: 'circle',
        selector: 'button',
        attributes: {
          'r': 12,
          'fill': '#1ABC9C',
          'cursor': 'pointer',
          'title': 'reverse'
        }
      }, {
        tagName: 'path',
        selector: 'icon',
        attributes: {
          'd': 'M 7 2 L -7 2 L 1 6 M -7 -2 L 7 -2 -1 -6',
          'fill': 'none',
          'stroke': '#FFFFFF',
          'stroke-width': 2.5,
          'cursor': 'pointer',
          'title': 'reverse'
          /*'pointer-events': 'none' */
        }
      }]
    }
  })
;


export var ChangeAssociationTypeButton = joint.linkTools.Button.extend({
    name: 'change-association-type-button',
    root: {
      title: 'fromm bodyyy'
    },
    options: {
      focusOpacity: 0.5,
      distance: 105,
      markup: [{
        tagName: 'circle',
        selector: 'button',
        attributes: {
          'r': 12,
          'fill': '#0088CE',
          'cursor': 'pointer',
          title: {text: 'Changer le type de relation'},

        }
      }, {
        tagName: 'path',
        selector: 'icon',
        attributes: {
          // 'd': 'M -2 4 2 4 M 0 3 0 0 M -2 -1 1 -1 ',
          'fill': 'none',
          'stroke': '#FFFFFF',
          'stroke-width': 2.5,
          'cursor': 'pointer',
          title: 'Changer le type de relation',
          event: 'icon:mouseenter'
        }
      }]
    }
  });

export var AssociationTypeButton = joint.elementTools.Button.extend({
  name: 'association-type-button',
  options: {
    focusOpacity: 0.5,
    useModelGeometry: true,
    y: '50%',
    x: '0%',
    distance: 60,
    markup: [{
      tagName: 'circle',
      selector: 'button',
      attributes: {
        'r': 10,
        'fill': '#0088CE',
        'cursor': 'pointer',
        // event: 'element:association:pointerdown',
      }
    }, {
      tagName: 'path',
      selector: 'icon',
      attributes: {
        /*d: (evt, cellView, toolButton, ckoi) => {
          console.log(cellView);
          if( 1 === 1) return 'M 2 7 L -6 0 L 2 -7 '
          else 'M 2 7 L -6 0 L 2 -7 Z'
        },*/
        d: 'M 2 7 L -6 0 L 2 -7 ',
        'fill': 'none',
        'stroke': '#FFFFFF',
        'stroke-width': 2,
        'cursor': 'pointer'
        //'pointer-events': 'none'
      }
    }],

  }
});


