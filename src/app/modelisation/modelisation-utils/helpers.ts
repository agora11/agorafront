import * as joint from 'jointjs';
import {Shape} from './custom-elements';
import {
  INITIAL_X_COORDINATE_XL_SCREEN,
  INITIAL_X_COORDINATE_MEDIUM_SCREEN,
  INITIAL_X_COORDINATE_SMALL_SCREEN,
  OBJECT_HEIGHT,
  SPACE_BETWEEN_OBJECTS, INITIAL_X_COORDINATE_XXL_SCREEN, OBJECT_WIDTH_DIAGRAM
} from './Constants';
import {DataService} from '../../services/data.service';

export function dropOnTarget(x: number, y: number, containerBBox, flyShape,
                             graph: joint.dia.Graph, paper: joint.dia.Paper, familyContainer, targetPaperOffset, objectOffset,
                             cellView, objectsMovedToDiagram: any[], diagramTranslatedBy): boolean {
  DataService.setIsModified(true);
  let clonedShape = flyShape.clone().attr('id', cellView.model.attributes.data.id);

  var pagePoint = new joint.g.Point(x, y);
  const localPoint = paper.pageToLocalPoint(pagePoint);
  clonedShape.position(localPoint.x, localPoint.y);
  if (familyContainer && containerBBox && x > containerBBox.x + targetPaperOffset.left
    && x < containerBBox.x + containerBBox.width + targetPaperOffset.left
    && y > containerBBox.y + targetPaperOffset.top
    && y < containerBBox.y + containerBBox.height + targetPaperOffset.top) {

    clonedShape.attributes.z = familyContainer.attributes.z + 1;
    clonedShape.attributes.size.width = OBJECT_WIDTH_DIAGRAM;
    graph.addCell(clonedShape);
    familyContainer.embed(clonedShape);
    objectsMovedToDiagram.push(cellView.model.attributes.data.id);
    clonedShape.attr({topTool: {width: OBJECT_WIDTH_DIAGRAM},  bottomTool: {width: OBJECT_WIDTH_DIAGRAM}, rightTool: {refX: OBJECT_WIDTH_DIAGRAM}, header: {fill: '#A1006B'}, label: {fill: '#FFFFFF'}});
    cellView.model.attr({header: {fill: '#A1006B'}, label: {fill: '#FFFFFF'}});
    return true;

  } else /*if (familyContainer === null || !objectsMovedToDiagram.includes(cellView.model.attributes.data.id)) */{
    clonedShape.attributes.size.width = OBJECT_WIDTH_DIAGRAM;
    graph.addCell(clonedShape);
    objectsMovedToDiagram.push(cellView.model.attributes.data.id);
    /*clonedShape.setAttribute('id', cellView.model.attributes.data.id);*/
    clonedShape.attr({topTool: {width: OBJECT_WIDTH_DIAGRAM}, bottomTool: {width: OBJECT_WIDTH_DIAGRAM}, rightTool: {refX: OBJECT_WIDTH_DIAGRAM}, header: {fill: '#A1006B'}, label: {fill: '#FFFFFF'}});    cellView.model.attr({header: {fill: '#A1006B'}, label: {fill: '#FFFFFF'}});
    cellView.model.attr({header: {fill: '#A1006B'}, label: {fill: '#FFFFFF'}});
    return true;
  }
}

export function listObjects(objects: any[], objectsMovedToDiagram: any[], graph: joint.dia.Graph, paper: joint.dia.Paper, xCoordinate: number, yCoordinate: number): void {

  // objects = [...objects, ...objects, ...objects, ...objects, ...objects, ...objects, ...objects, ...objects, ...objects, ...objects, ...objects, ...objects, ...objects, ...objects];
  var rowNumber = 1;
  const initialXCoordinate = window.screen.width > 1600 ? INITIAL_X_COORDINATE_XXL_SCREEN
    : window.screen.width > 1450 ? INITIAL_X_COORDINATE_XL_SCREEN
    : window.screen.width > 1390 ? INITIAL_X_COORDINATE_MEDIUM_SCREEN
        : INITIAL_X_COORDINATE_SMALL_SCREEN;
  objects.map((object, index) => {
    if ((index + 1) % 2 === 0) {
      const newObject = createObject(object, xCoordinate, yCoordinate, objectsMovedToDiagram);
      graph.addCell(newObject);
      rowNumber += 1;
      yCoordinate = yCoordinate + newObject.attributes.size.height + SPACE_BETWEEN_OBJECTS;
      xCoordinate = initialXCoordinate;
    } else {
      const newObject = createObject(object, xCoordinate, yCoordinate, objectsMovedToDiagram);
      graph.addCell(newObject);
      xCoordinate += newObject.attributes.size.width + SPACE_BETWEEN_OBJECTS;
    }
  });
  let paperHeight = (rowNumber + 1) * (OBJECT_HEIGHT + SPACE_BETWEEN_OBJECTS);
  paper.setDimensions('100%', paperHeight);
}

function createObject(object, xCoordinate: number, yCoordinate: number, objectsMovedToDiagram: any[]): any {

  return objectsMovedToDiagram.includes(object.id) ?
    new Shape({
      data: {isAssociationType: true, ...object},
      attrs: {
        label: {
          textWrap: {
            text: object.libelle,
            width: '60%',
            height: '95%',
            ellipsis: true,
            textOverflow: 'hidden',
          },
          fill: '#FFFFFF',
          fontStyle: object.statut && object.statut.libelle === 'Brouillon' ? 'italic' : 'normal',
          // fontWeight : object.statut && object.statut.libelle === 'Brouillon' ? 'bold' : 'normal'
        }, bodyText: {
          textWrap: {
            /*text: joint.util.breakText(object.libelle + "\n" + object.libelle + "\n" + object.libelle, {width: 112}),*/
            text: formatObjectDetails(object),
            width: '95%',
            height: '90%',
            ellipsis: true,
            textOverflow: 'hidden'
          },
          fill: 'transparent'
        },
        header: {
          fill: '#A1006B'
        }
      },
      position: {x: xCoordinate, y: yCoordinate}
    })
    : new Shape({
      data: {...object},
      attrs: {
        label: {
          textWrap: {
            text: object.libelle,
            width: '60%',
            height: '95%',
            ellipsis: true,
            textOverflow: 'hidden'
          },
          fontStyle: object.statut && object.statut.libelle === 'Brouillon' ? 'italic' : 'normal',
        },
        bodyText: {
          textWrap: {
            /*text: joint.util.breakText(object.libelle + "\n" + object.libelle + "\n" + object.libelle, {width: 112}),*/
            text: formatObjectDetails(object),
            width: '95%',
            height: '90%',
            ellipsis: true,
            textOverflow: 'hidden',
            lineHeight: 6
          },
          fill: 'transparent'
        }
      },
      position: {x: xCoordinate, y: yCoordinate}
    });
}

function formatObjectDetails(object: any): string {
  return object.attributs.map((attribut, index) => joint.util.breakText((index + 1) + ' - ' + attribut.libelle, {width: 159, height: 35}, {'class': 'v-line-custom'}, {
    ellipsis: true,
    maxLineCount: 2
  })).join('\n');
  /*joint.util.breakText('¤ Id: ', {width: 140}, { fontSize: 60 }, {ellipsis: true, maxLineCount: 1}) + '\n'
    + joint.util.breakText(' ' + object.id, {width: 140}, null, {ellipsis: true, maxLineCount: 1}) + '\n'
    + joint.util.breakText('--- Mis à jour le: ', {width: 140}, null, {ellipsis: true, maxLineCount: 1}) + '\n'
    + joint.util.breakText( ' ' + object.dateDebutActivite, {width: 140}, null, {ellipsis: true, maxLineCount: 2}) + '\n'
    + joint.util.breakText('--- Filières: ', {width: 140}, null, {ellipsis: true, maxLineCount: 1}) + '\n'
    + joint.util.breakText(' ' + object.filieres.map(filiere => filiere.nom).join('/'), {width: 140}, null, {
      ellipsis: true,
      maxLineCount: 2
    }) + '\n';*/
  /*
            + joint.util.breakText('Familles: ' + object.famille.map(filiere => filiere.nom).join('/'), {width: 140}, null, {ellipsis: true, maxLineCount: 1 }) + '\n';
  */
};

export const handleRemoveObject =
  (mainGraph: joint.dia.Graph, objectsListGraph: joint.dia.Graph, cellView, objectsMovedToDiagram: any[]) => {
    DataService.setIsModified(true);

    cellView.model.remove();
    const index = objectsMovedToDiagram.indexOf(cellView.model.attributes.data.id, 0);
    if (index > -1) {
      objectsMovedToDiagram.splice(index, 1);

      if (objectsListGraph.getCells()) {
        const cellOnObjectsListPaper = objectsListGraph.getCells().find(
          cell => cell.attributes.data.id === cellView.model.attributes.data.id
        );

        if (cellOnObjectsListPaper && objectsMovedToDiagram.indexOf(cellView.model.attributes.data.id, 0) === -1) {
          cellOnObjectsListPaper.attr({
            header: {fill: '#7777771A'},
            label: {fill: '#AFA59B'}
          });
        }

        if (mainGraph.getLinks()) {
          const linksToRemove = mainGraph.getLinks().filter(
            link => link.attributes.source.id === cellView.model.attributes.id
              || link.attributes.target.id === cellView.model.attributes.id
          );
          if (linksToRemove) {
            mainGraph.removeCells(linksToRemove);
          }
        }
      }
    }
  };

export const handleRemoveFamilyContainer =
  (mainGraph: joint.dia.Graph, mainPaper: joint.dia.Paper, objectsListGraph: joint.dia.Graph, cellView, createdContainersList: any[], objectsMovedToDiagram: any[]) => {
    DataService.setIsModified(true);

    if (cellView.model.attributes.type === 'container.Parent') {
      mainGraph.removeCells([cellView]);
      const index = createdContainersList.findIndex(container => container.attributes.data.id === cellView.model.attributes.data.id);
      createdContainersList.splice(index, 1);

      const embeddedObjects = cellView.model.getEmbeddedCells();
      embeddedObjects.forEach(
        embeddedObj => handleRemoveObject(mainGraph, objectsListGraph, mainPaper.findViewByModel(embeddedObj), objectsMovedToDiagram)
      );
    }
  };


export const handleRestrictTranslateInMainPaper =
  (mainPaper: joint.dia.Paper, elementView: joint.dia.ElementView, containerHeaderHeight) => {
    DataService.setIsModified(true);
    const parent = elementView.model.getParentCell();
    if (parent) {
      let bodyBBox = parent.getBBox();
      bodyBBox.y = bodyBBox.y + containerHeaderHeight;
      bodyBBox.height = bodyBBox.height - containerHeaderHeight;
      return bodyBBox;
    } else {
      return null;
    }
  };


export const handleValidateConnectionInMainPaper =
  (mainGraph: joint.dia.Graph, mainPaper: joint.dia.Paper, linkView, cellViewT, cellViewS) => {
    DataService.setIsModified(true);
    const links = mainGraph.getLinks();
    for (let i = 0; i < links.length; i++) {  // Skip the wire the user is drawing
      if (linkView === links[i].findView(mainPaper)) {
        continue;
      }
      if (((cellViewT.model.id === links[i].get('target').id) && (cellViewS.model.id === links[i].get('source').id))
        || ((cellViewS.model.id === links[i].get('target').id) && (cellViewT.model.id === links[i].get('source').id))
        || (cellViewS.model.id === cellViewT.model.id)) {
        return false;
      }
    }
    return true;
  };
