import {Component, OnDestroy, OnInit, TemplateRef, ViewChild,} from '@angular/core';
import * as $ from 'jquery';
import * as joint from 'jointjs';
import {shapes} from 'jointjs';
import {v4 as uuidv4} from 'uuid';
import {
  AssociationTypeButton,
  ChangeAssociationTypeButton,
  Container,
  ReverseDirectionButton,
  Shape,
} from './modelisation-utils/custom-elements';
import {
  dropOnTarget,
  handleRemoveFamilyContainer,
  handleRemoveObject,
  handleValidateConnectionInMainPaper,
  listObjects
} from './modelisation-utils/helpers';
import {MenuItem} from 'primeng';
import {ActivatedRoute} from '@angular/router';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {ToastrService} from 'ngx-toastr';
import {
  INITIAL_X_COORDINATE_MEDIUM_SCREEN,
  INITIAL_X_COORDINATE_SMALL_SCREEN,
  INITIAL_X_COORDINATE_XL_SCREEN,
  INITIAL_X_COORDINATE_XXL_SCREEN,
  INITIAL_Y_COORDINATE
} from './modelisation-utils/Constants';
import {ModelisationService} from '../services/modelisation.service';
import {Diagramme} from '../models/diagramme';
import {DataService} from '../services/data.service';

const {standard, devs} = shapes;

@Component({
  selector: 'app-modelisation',
  templateUrl: './modelisation.component.html',
  styleUrls: ['./modelisation.component.css'],
})
export class ModelisationComponent implements OnInit, OnDestroy {

  public mainGraph: joint.dia.Graph;
  public mainPaper: joint.dia.Paper;
  public objectsListPaper: joint.dia.Paper;
  public objectsListGraph: joint.dia.Graph;
  public currentDiagram: any;
  public items: MenuItem[];
  public activeItem: MenuItem;
  public isAssociationType = true;
  public objects: any[];
  public objectsMovedToDiagram: any[] = [];
  public objectListXCoordinate = window.screen.width > 1600 ? INITIAL_X_COORDINATE_XXL_SCREEN
    : window.screen.width > 1450 ? INITIAL_X_COORDINATE_XL_SCREEN
      : window.screen.width > 1390 ? INITIAL_X_COORDINATE_MEDIUM_SCREEN
        : INITIAL_X_COORDINATE_SMALL_SCREEN;
  public objectListYCoordinate = INITIAL_Y_COORDINATE;
  public modalRef: BsModalRef;
  @ViewChild('CreateFamilyContainer') createFamilyContainerTemplate: TemplateRef<any>;
  @ViewChild('ConfirmDeletingFamilyContainer') confirmDeletingFamilyContainerTemplate: TemplateRef<any>;
  @ViewChild('AssociationRole') associationRoleTemplate: TemplateRef<any>;
  @ViewChild('SaveDiagramBeforeExit') saveDiagramBeforeExitTemplate: TemplateRef<any>;
  @ViewChild('DuplicateDiagramme') duplicateDiagrammeTemplate: TemplateRef<any>;
  public families: any[] = [{id: '1', name: 'Ouvrages d\'art'}, {id: '2', name: 'Autres'}];
  public selectedFamily = null;
  public familyContainerToDelete = null;
  public createdContainersList: any[] = [];
  public zoomLevel = 1;
  public mousePositionOnPointerDownEvent = {x: 0, y: 0};
  public diagramTranslatedBy = {x: 0, y: 0};
  public currentDiagramOrigin = {x: 0, y: 0};
  public associationToAddARole = null;
  public typedRole = '';
  public rolesTypedBefore: any[] = [];
  public rolesTypedBeforeFiltred: any[] = [];
  public modifyAssociationRoleModal = false;
  public duplicatedDiagram = new Diagramme();
  diagrammeToEdit: any;
  public filieres: any;
  public isDisabled: boolean;
  selectConfig = {
    labelField: 'nom',
    valueField: 'nom',
    searchField: ['nom'],
    dropdownDirection: 'down',
    sortField: [{field: 'nom', direction: 'asc'}],
  };
  ngbModalOptions = {
    backdrop: true,
    ignoreBackdropClick: true
  };
  public isModified: boolean;
  public required: boolean;
  filieresFilter: any;
  familles = new Array();
  sousFamilles = new Array();
  selectedFiliere: any;
  public filtredObjects: any;
  selectedFamille: any;
  selectedSFamille: any;

  constructor(private activatedRoute: ActivatedRoute,
              private modalService: BsModalService,
              private modelizationService: ModelisationService,
              private toastr: ToastrService) {
  }


  ngDoCheck(): void {

    const elements = document.querySelectorAll('text > tspan.v-line');
    const regex = /^\d+\s-\s.*$/;
    for (let i = 0; i < elements.length; i++) {
      if (elements[i].textContent.match(regex)) {
        elements[i].setAttribute('dy', '1.3em');
      }
    }
  }

  ngOnInit(): void {
    this.setIsModified(false);
    DataService.getIsModified().subscribe(result => this.isModified = result);
    // initialize tab menu items
    // this.items = [
    //   /* {
    //      label: 'Famille', command: (event) => {
    //        this.onChangeTab(event.item);
    //      }
    //    },*/
    //   {
    //     label: 'Objet', command: (event) => {
    //       this.onChangeTab(event.item);
    //     }
    //   },
    //   {
    //     label: 'Usage', command: (event) => {
    //       this.onChangeTab(event.item);
    //     }
    //   },
    // ];
    //this.activeItem = this.items[0];

    // Get objects list
    this.activatedRoute.data.subscribe((data) => {
      this.objects = data.modelisation.objetList;
      this.filtredObjects = JSON.parse(JSON.stringify(this.objects));
      this.filieres = data.modelisation.filieres;
      // this.filieres.forEach(filiere => {
      //   filiere.label = filiere.nom;
      //   filiere.value =  filiere.idFiliere;
      //   filiere.familles?.forEach(famille => {
      //     this.familles.push({label: famille.nom, value: famille.idFamille});
      //     famille.sousFamilles.forEach(sf => {
      //       this.sousFamilles.push({label: sf.nom, value: sf.idFamille});
      //     });
      //   });
      // });
    });
    this.activatedRoute.queryParams.subscribe(params => {
      this.isDisabled = params.action === 'consult';
    });

    // Canvas which contains diagram
    this.mainGraph = new joint.dia.Graph({}, {
      cellNamespace: {
        // Optionally, cherry-pick namespaces/shapes you will use in your application
        devs,
        standard,
        example: {Shape},
        container: {Parent: Container}
      }
    });
    this.mainPaper = new joint.dia.Paper({
      el: $('#mainPaper'),
      model: this.mainGraph,
      width: '100%',
      height: '100%',

      defaultLink: (cellView, magnet) => {
        if (cellView.model.attributes.associationType) {
          return new joint.shapes.devs.Link({
            isAssociationType: true,
            attrs: {
              '.connection': {
                strokeWidth: 1.5,
                stroke: '#0088CE'
              },
              '.marker-target': {d: 'M 2 -7 L -10 0 L 2 7 L -10 0 L 2 -7 ', stroke: '#0088CE'},
            }
          });
        } else {
          return new joint.shapes.devs.Link({
            isAssociationType: false,
            attrs: {
              '.connection': {
                strokeWidth: 1.5,
                stroke: '#0088CE'
              },
              '.marker-target': {d: 'M 2 -7 L -10 0 L 2 7 z ', stroke: '#0088CE', strokeWidth: 1.5, fill: 'white'},
            }
          });
        }
      },
      connectionStrategy: (end) => {
        // removing `magnet: 'tool'` from the end object
        return {id: end.id};
      },
      validateConnection: (cellViewS, magnetS, cellViewT, magnetT, end, linkView) =>
        handleValidateConnectionInMainPaper(this.mainGraph, this.mainPaper, linkView, cellViewT, cellViewS),
      linkPinning: false,
      preventContextMenu: true,
      embeddingMode: true,
      interactive: !this.isDisabled,
      validateEmbedding: (childView, parentView) => {
        if (parentView.model.attributes.type === 'container.Parent' && childView.model.attributes.type === 'example.Shape') {
          return true;
        }
        return false;
      },
      highlighting: {
        connecting: {
          name: 'stroke',
          options: {
            className: 'highlight-connecting',
            padding: 4,
            rx: 5,
            ry: 5,
            attrs: {
              stroke: '#1ABC9C',
              'stroke-width': 3,
              opacity: 0.55,
            }
          }
        }
      }

    });
    // redefining link's remove tool and vertex
    joint.dia.Link.prototype.toolMarkup = this.isDisabled ?
      ['<g class="link-tool" >',
        '</g>'
      ].join('')
      : ['<g class="link-tool">',
        '<g class="tool-remove" event="remove">',
        '<circle r="11" />',
        '<path transform="scale(.8) translate(-16, -16)" d="M24.778,21.419 19.276,15.917 24.777,10.415 21.949,7.585 16.447,13.087 10.945,7.585 8.117,10.415 13.618,15.917 8.116,21.419 10.946,24.248 16.447,18.746 21.948,24.248z" />',
        '</g>',
        '<g class="tool-options" event="link:options">',
        '<circle r="11" transform="translate(25)"/>',
        '<path fill="white" transform="scale(.55) translate(29, -16)" d="M31.229,17.736c0.064-0.571,0.104-1.148,0.104-1.736s-0.04-1.166-0.104-1.737l-4.377-1.557c-0.218-0.716-0.504-1.401-0.851-2.05l1.993-4.192c-0.725-0.91-1.549-1.734-2.458-2.459l-4.193,1.994c-0.647-0.347-1.334-0.632-2.049-0.849l-1.558-4.378C17.165,0.708,16.588,0.667,16,0.667s-1.166,0.041-1.737,0.105L12.707,5.15c-0.716,0.217-1.401,0.502-2.05,0.849L6.464,4.005C5.554,4.73,4.73,5.554,4.005,6.464l1.994,4.192c-0.347,0.648-0.632,1.334-0.849,2.05l-4.378,1.557C0.708,14.834,0.667,15.412,0.667,16s0.041,1.165,0.105,1.736l4.378,1.558c0.217,0.715,0.502,1.401,0.849,2.049l-1.994,4.193c0.725,0.909,1.549,1.733,2.459,2.458l4.192-1.993c0.648,0.347,1.334,0.633,2.05,0.851l1.557,4.377c0.571,0.064,1.148,0.104,1.737,0.104c0.588,0,1.165-0.04,1.736-0.104l1.558-4.377c0.715-0.218,1.399-0.504,2.049-0.851l4.193,1.993c0.909-0.725,1.733-1.549,2.458-2.458l-1.993-4.193c0.347-0.647,0.633-1.334,0.851-2.049L31.229,17.736zM16,20.871c-2.69,0-4.872-2.182-4.872-4.871c0-2.69,2.182-4.872,4.872-4.872c2.689,0,4.871,2.182,4.871,4.872C20.871,18.689,18.689,20.871,16,20.871z"/>',
        '</g>',
        '</g>'
      ].join('');
    joint.dia.Link.prototype.vertexMarkup = [
      '<g class="marker-vertex-group" transform="translate(<%= x %>, <%= y %>)">',
      '<circle class="marker-vertex" idx="<%= idx %>" r="10" />',
      '<path class="marker-vertex-remove-area" idx="<%= idx %>" d="M16,5.333c-7.732,0-14,4.701-14,10.5c0,1.982,0.741,3.833,2.016,5.414L2,25.667l5.613-1.441c2.339,1.317,5.237,2.107,8.387,2.107c7.732,0,14-4.701,14-10.5C30,10.034,23.732,5.333,16,5.333z" transform="translate(5, -33)"/>',
      '<path class="marker-vertex-remove" idx="<%= idx %>" transform="scale(.8) translate(9.5, -37)" d="M24.778,21.419 19.276,15.917 24.777,10.415 21.949,7.585 16.447,13.087 10.945,7.585 8.117,10.415 13.618,15.917 8.116,21.419 10.946,24.248 16.447,18.746 21.948,24.248z">',
      '</path>',
      '</g>'
    ].join('');

    // Open diagram in editing mode
    if (localStorage.getItem('currentDiagram') !== null) {
      this.currentDiagram = JSON.parse(localStorage.getItem('currentDiagram'));
      // localStorage.setItem('currentDiagram', JSON.stringify(history.state.diagramDetails));
      const graphJson = this.currentDiagram.cells.map(cell => JSON.parse(cell));
      const graphJsonObject = {cells: JSON.parse(JSON.stringify(graphJson))};
      this.mainGraph.fromJSON(graphJsonObject);
      this.objectsMovedToDiagram = graphJsonObject.cells.map(cell => {
        if (cell.type === 'example.Shape') {
          return cell.data.id;
        }
      });
    }
    // Main Paper listeners
    this.mainPaper.on({
      'element:pointerclick': (cellView, evt) => {

        if (cellView.model.attributes.type !== 'container.Parent') {
          const model = cellView.model;
          const bbox = model.getBBox();
          const ellipseRadius = (1 - Math.cos(joint.g.toRad(45)));
          const offset = model.attr(['pointers', 'pointerShape']) === 'ellipse'
            ? {x: -ellipseRadius * bbox.width / 2, y: ellipseRadius * bbox.height / 2}
            : {x: -3, y: 3};
          if (!this.isDisabled) {
            const associationButton = new AssociationTypeButton({
              icon: {d: cellView.model.attributes.associationType ? 'M 2 7 L -6 0 L 2 -7' : 'M 2 7 L -6 0 L 2 -7 Z'},
              action: (evt, cellView, toolButton) => {
                this.setIsModified(true);
                cellView.model.attributes.associationType = !cellView.model.attributes.associationType;
                if (cellView.model.attributes.associationType) {
                  toolButton.options.markup.filter(markup => markup.selector === 'icon')[0].attributes.d = 'M 2 7 L -6 0 L 2 -7';
                } else {
                  toolButton.options.markup.filter(markup => markup.selector === 'icon')[0].attributes.d = 'M 2 7 L -6 0 L 2 -7 Z';
                }
                let tools = cellView._toolsView.tools.filter(tool => tool.options.y !== '50%');
                tools = [...tools, toolButton];
                cellView.removeTools();
                cellView.addTools(new joint.dia.ToolsView({tools}));
              }
            });
            cellView.model.attributes.associationType ? associationButton.options.markup.filter(markup => markup.selector === 'icon')[0].attributes.d = 'M 2 7 L -6 0 L 2 -7 '
              : associationButton.options.markup.filter(markup => markup.selector === 'icon')[0].attributes.d = 'M 2 7 L -6 0 L 2 -7 Z';
            cellView.addTools(new joint.dia.ToolsView({
              tools: [
                new joint.elementTools.Remove({
                  useModelGeometry: true,
                  y: '0%',
                  x: '100%',
                  offset,
                  action: (evt) =>
                    handleRemoveObject(this.mainGraph, this.objectsListGraph, cellView, this.objectsMovedToDiagram)
                }), associationButton
              ]
            }));
          }
        }
      },
      'element:mouseleave': (cellView, evt) => {
        cellView.hideTools();
      }, 'element:button:pointerdown': (cellView, evt) =>
        this.genericOpenModal(this.confirmDeletingFamilyContainerTemplate, cellView)
      , 'blank:pointerdown': (evt, x, y) => {
        this.mousePositionOnPointerDownEvent = {
          x,
          y
        };
      }, 'blank:pointermove': (evt, x, y) => {
        this.mainPaper.translate(this.mainPaper.options.origin.x + x - this.mousePositionOnPointerDownEvent.x,
          this.mainPaper.options.origin.y + y - this.mousePositionOnPointerDownEvent.y);
      }, translate: (x, y) => {
        this.diagramTranslatedBy.x += this.mainPaper.options.origin.x - this.currentDiagramOrigin.x;
        this.diagramTranslatedBy.y += this.mainPaper.options.origin.y - this.currentDiagramOrigin.y;
        this.currentDiagramOrigin = this.mainPaper.options.origin;
      }, scale: (sx, sy) => {
      }, 'blank:mousewheel': (evt, x, y, delta) => {
        if (delta > 0) {
          this.zoomIn(evt);
        } else {
          this.zoomOut(evt);
        }
      }
      , 'element:mousewheel': (cellView, evt, x, y, delta) => {
        if (delta > 0) {
          this.zoomIn(evt);
        } else {
          this.zoomOut(evt);
        }
      }
      , 'link:mousewheel': (cellView, evt, x, y, delta) => {
        if (delta > 0) {
          this.zoomIn(evt);
        } else {
          this.zoomOut(evt);
        }
      }, 'cell:highlight': (cellView, node, options) => {
        switch (options.type) {
          case joint.dia.CellView.Highlighting.CONNECTING: {
            joint.highlighters.mask.remove(node);
            break;
          }
          case joint.dia.CellView.Highlighting.EMBEDDING: {
            joint.highlighters.stroke.remove(cellView);
            // return false;
            break;
          }
          case joint.dia.CellView.Highlighting.MAGNET_AVAILABILITY: {
            joint.highlighters.mask.remove(node);
            // return false;
            break;
          }
          default: {
            break;
          }
        }
      }, 'link:mouseenter': (cellView, evt, x, y) => {
        if (!this.isDisabled) {
          const reverseDirectionButton = new ReverseDirectionButton();
          const changeAssociationTypeButton = new ChangeAssociationTypeButton({
            action: (evt, linkView, toolView) => {
              this.setIsModified(true);
              linkView.model.attributes.isAssociationType = !linkView.model.attributes.isAssociationType;
              if (linkView.model.attributes.isAssociationType) {
                toolView.options.markup.filter(markup => markup.selector === 'icon')[0].attributes.d = 'M 2 7 L -6 0 L 2 -7';
                linkView.model.attributes.attrs = {
                  '.connection': {
                    strokeWidth: 1.5,
                    stroke: '#0088CE'
                  },
                  '.marker-target': {d: 'M 2 -7 L -10 0 L 2 7 L -10 0 L 2 -7 ', stroke: '#0088CE'},
                };

              } else {
                toolView.options.markup.filter(markup => markup.selector === 'icon')[0].attributes.d = 'M 2 7 L -6 0 L 2 -7 Z';
                linkView.model.attributes.attrs = {
                  '.connection': {
                    strokeWidth: 1.5,
                    stroke: '#0088CE'
                  },
                  '.marker-target': {d: 'M 2 -7 L -10 0 L 2 7 z ', stroke: '#0088CE', strokeWidth: 1.5, fill: 'white'},
                };

                if (linkView.model.labels().length > 0) {
                  linkView.model.removeLabel(0);
                }
              }
              linkView.update(linkView.model, {});
              let tools = linkView._toolsView.tools.filter(tool => tool.options.distance !== '100');
              tools = [...tools, toolView];
              linkView.removeTools();
              linkView.addTools(new joint.dia.ToolsView({tools}));
            }
          });
          cellView.model.attributes.isAssociationType ? changeAssociationTypeButton.options.markup.filter(markup => markup.selector === 'icon')[0].attributes.d = 'M 2 7 L -6 0 L 2 -7 '
            : changeAssociationTypeButton.options.markup.filter(markup => markup.selector === 'icon')[0].attributes.d = 'M 2 7 L -6 0 L 2 -7 Z';
          cellView.addTools(new joint.dia.ToolsView({
            tools: [
              reverseDirectionButton, changeAssociationTypeButton
            ]
          }));
        }
      }, 'link:mouseleave': (cellView, evt, x, y) => {
        cellView.hideTools();
      }, 'link:contextmenu': (linkView, evt, x, y) => {
        if (!this.isDisabled) {
          if (linkView.model.get('isAssociationType')) {
            if (linkView.model.labels() && linkView.model.labels().length > 0) {
              this.typedRole = linkView.model.labels()[0].attrs.label.text;
              this.modifyAssociationRoleModal = true;
            }
            this.genericOpenModal(this.associationRoleTemplate, null, linkView);
          }
        }
      }, 'element:objbutton:pointerdown': (elementView) => {
        const element = elementView.model;
        element.toggle();
        // fitAncestors(element);
      }
    });
    this.initObjects();
    // add a listener to detect that #objectsList is mounted in the DOM to be able to create objectsListPaper
    const targetNode = document.getElementById('tabsContent');
    // Options for the observer (which mutations to observe)
    const config = {characterData: true, childList: true, subtree: true};

    // Callback function to execute when mutations are observed
    const callback = (function (mutationsList, observer): void {
      // Use traditional 'for loops' for IE 11
      for (const mutation of mutationsList) {
        if (mutation.type === 'childList'
          && mutation.addedNodes && mutation.addedNodes[0] === document.getElementById('objectListPaperTab')) {
          this.initObjects();
        }
      }
    }).bind(this);

    // Create an observer instance linked to the callback function
    const observer = new MutationObserver(callback);

    // Start observing the target node for configured mutations
    observer.observe(targetNode, config);
  }

  initObjects() {
    this.objectsListGraph = new joint.dia.Graph();
    this.objectsListPaper = new joint.dia.Paper({
      el: $('#objectsListPaper'),
      model: this.objectsListGraph,
      interactive: false,
      width: '100%',

    });
    listObjects(this.filtredObjects, this.objectsMovedToDiagram, this.objectsListGraph, this.objectsListPaper,
      this.objectListXCoordinate, this.objectListYCoordinate);
    this.objectsListPaper.on({
      'cell:pointerdown': (cellView, e, x, y) => {
        $('body').append('<div id="flyPaper" style="position:fixed;z-index:100;opacity:.7;background: transparent;pointer-event:none;"></div>');
        const flyGraph = new joint.dia.Graph();
        const flyPaper = new joint.dia.Paper({
          el: $('#flyPaper'),
          model: flyGraph,
          interactive: false
        });
        const flyShape = cellView.model.clone();
        const pos = cellView.model.position();
        const offset = {
          x: x - pos.x,
          y: y - pos.y
        };

        flyShape.position(0, 0);
        flyGraph.addCell(flyShape);
        $('#flyPaper').offset({
          left: e.pageX - offset.x,
          top: e.pageY - offset.y
        });
        $('body').on('mousemove.fly', (event) => {
          $('#flyPaper').offset({
            left: event.pageX - offset.x,
            top: event.pageY - offset.y
          });
        });
        $('body').on('mouseup.fly', (event) => {
          const x = event.pageX;
          const y = event.pageY;
          const target = this.mainPaper.$el.offset();
          const size = this.mainPaper.getComputedSize();
          // containers empty so no call for dropOntarget
          if (x > target.left && x < target.left + size.width
            && y > target.top && y < target.top + size.height) {
            if (this.createdContainersList.length > 0 && !this.isDisabled) {
              let alreadyDropedObject = false;
              const sortedCreatedContainersListByZIndex = this.createdContainersList.sort((containerA, containerB) => {
                if (containerA.attributes.z > containerB.attributes.z) {
                  return -1;
                } else {
                  return 1;
                }
              });
              for (const container of sortedCreatedContainersListByZIndex) {
                if (alreadyDropedObject) {
                  break;
                } else {
                  const containerBBox = this.mainPaper.findViewByModel(container).getBBox();
                  alreadyDropedObject = dropOnTarget(x, y, containerBBox, flyShape, this.mainGraph, this.mainPaper,
                    container, target, offset, cellView, this.objectsMovedToDiagram, this.diagramTranslatedBy);
                }
              }

            } else if (!this.isDisabled) {
              dropOnTarget(x, y, null, flyShape,
                this.mainGraph, this.mainPaper, null, target, offset, cellView, this.objectsMovedToDiagram, this.diagramTranslatedBy);
            } else if (!this.objectsMovedToDiagram.includes(cellView.model.attributes.data.id)) {
              cellView.model.attr({header: {fill: '#7777771A'}, label: {fill: '#AFA59B'}});
            }
          } else {
            cellView.model.attr({header: {fill: '#7777771A'}, label: {fill: '#AFA59B'}});
          }
          $('body').off('mousemove.fly').off('mouseup.fly');
          flyShape.remove();
          $('#flyPaper').remove();
        });
      }, 'cell:mouseover':
        (cellView, e, x, y) => {
          cellView.model.attr({header: {fill: '#A1006B'}, label: {fill: '#FFFFFF'}});

        }, 'cell:mouseout':
        (cellView, e, x, y) => {
          if (!this.objectsMovedToDiagram.includes(cellView.model.attributes.data.id)) {
            cellView.model.attr({header: {fill: '#7777771A'}, label: {fill: '#AFA59B'}});
          }
        }
    });
  }

  zoomOut(mouseWheelEvent: any): void {
    this.zoomLevel = Math.max(0.5, this.zoomLevel - 0.075);

    if (mouseWheelEvent) {
      mouseWheelEvent.preventDefault();
      const pagePoint = new joint.g.Point(mouseWheelEvent.pageX, mouseWheelEvent.pageY);
      const localPoint = this.mainPaper.pageToLocalPoint(pagePoint);
      this.mainPaper.scale(this.zoomLevel, this.zoomLevel,
        localPoint.x * 0.075/* + this.mainPaper.options.origin.x*/, localPoint.y * 0.075 /*+ this.mainPaper.options.origin.y*/);
    } else {
      const size = this.mainPaper.getComputedSize();
      this.mainPaper.translate(0, 0);
      this.mainPaper.scale(this.zoomLevel, this.zoomLevel, size.width / 2, size.height / 2);
    }

  }

  zoomReset(): void {
    this.zoomLevel = 1;
    const size = this.mainPaper.getComputedSize();
    this.mainPaper.translate(0, 0);
    this.mainPaper.scale(this.zoomLevel, this.zoomLevel, size.width / 2, size.height / 2);
  }

  zoomIn(mouseWheelEvent: any): void {
    this.zoomLevel = Math.min(3, this.zoomLevel + 0.075);
    if (mouseWheelEvent) {
      mouseWheelEvent.preventDefault();
      const pagePoint = new joint.g.Point(mouseWheelEvent.pageX, mouseWheelEvent.pageY);
      const localPoint = this.mainPaper.pageToLocalPoint(pagePoint);
      this.mainPaper.scale(this.zoomLevel, this.zoomLevel,
        localPoint.x * 0.075, localPoint.y * 0.075);
    } else {
      const size = this.mainPaper.getComputedSize();
      this.mainPaper.translate(0, 0);
      this.mainPaper.scale(this.zoomLevel, this.zoomLevel, size.width / 2, size.height / 2);
    }
  }

  centerDiagram(): void {
    this.mainPaper.translate(0, 0);
  }

  // onChangeTab(currentTab): void {
  //   this.activeItem = currentTab;
  // }

  genericOpenModal(template: TemplateRef<any>, familyContainerToDelete?: any, linkToAddOrEditLabel?: any, diagramToEdit?: Diagramme): any {
    this.required = false;
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, {class: 'modal-dialog-centered'}, this.ngbModalOptions)
    );
    if (familyContainerToDelete) {
      this.familyContainerToDelete = familyContainerToDelete;
    }
    if (linkToAddOrEditLabel) {
      this.associationToAddARole = linkToAddOrEditLabel;
    }
    if (diagramToEdit) {
      this.diagrammeToEdit = JSON.parse(JSON.stringify(diagramToEdit));
    }
  }

  genericHideModal(): any {
    if (this.modalRef) {
      this.modalRef.hide();
    }
    this.selectedFamily = null;
    this.familyContainerToDelete = null;
    this.typedRole = '';
    this.modifyAssociationRoleModal = false;
    this.duplicatedDiagram.libelle = '';
    this.duplicatedDiagram.description = '';
  }


  createFamilyContainer(): void {
    if (this.selectedFamily) {

      const container = new Container({
        position: {x: this.mainPaper.options.origin.x, y: this.mainPaper.options.origin.y},
        data: {...this.selectedFamily},
        attrs: {headerText: {text: this.selectedFamily.name}}
      });

      this.mainGraph.addCell(container);
      this.createdContainersList.push(container);
    }
    this.genericHideModal();

  }

  deleteFamilyContainer(): void {
    if (this.familyContainerToDelete) {
      handleRemoveFamilyContainer(this.mainGraph, this.mainPaper, this.objectsListGraph,
        this.familyContainerToDelete, this.createdContainersList, this.objectsMovedToDiagram);
    }
    this.genericHideModal();

  }

  confirmAssociationRole(): void {
    this.setIsModified(true);
    if (this.associationToAddARole) {
      this.associationToAddARole.model.label(0, {
        attrs: {
          label: {
            text: this.typedRole,
            fill: '#0088CE',
            fontSize: 14,
            textAnchor: 'middle',
            textVerticalAnchor: 'middle'
          },
          body: {
            ref: 'label',
            fill: '#F8F9FE',
            refX: -5,
            refY: -5,
            refWidth: '100%',
            refHeight: '100%',
            refWidth2: 10,
            refHeight2: 10,
            rx: 5,
            ry: 5
          }
        },
        position: {
          distance: 0.5
        }, markup: [
          {
            tagName: 'rect',
            selector: 'body'
          }, {
            tagName: 'text',
            selector: 'label'
          }
        ],
      });

      if (!this.rolesTypedBefore.includes(this.typedRole.trim())) {
        this.rolesTypedBefore.push(this.typedRole.trim());
      }
      this.genericHideModal();
      this.typedRole = '';

    }
  }

  searchInRolesTypedBefore(event): void {
    const query = event.query;
    this.rolesTypedBeforeFiltred = this.rolesTypedBefore.filter(role => role.toLowerCase().includes(query.toLowerCase()));
  }

  deleteAssociationRole(): void {
    this.setIsModified(true);
    if (this.associationToAddARole) {
      this.associationToAddARole.model.removeLabel(0);
      this.genericHideModal();
      this.typedRole = '';
    }
  }

  updateDiagram(): void {
    const cells = this.mainGraph.toJSON().cells.map(
      cell => JSON.stringify(cell));
    const diagram = {
      ...this.currentDiagram,
      cells
    };
    this.modelizationService.updateDiagram(diagram).subscribe(result => {
        this.setIsModified(false);
        this.currentDiagram = result;
        localStorage.setItem('currentDiagram', JSON.stringify(this.currentDiagram));
        this.showSuccess();
      },

      () => {
        this.showFailure('L\'opération a échoué. Veuillez réessayer à nouveau.\'');
      });
  }


  showSuccess(): any {
    this.toastr.clear();
    this.toastr.success('Opération effectuée avec succès !');
  }

  showFailure(msg): any {
    this.toastr.clear();
    this.toastr.error(msg);
  }

  saveDiagramBeforeDuplication(): void {
    this.updateDiagram();
    this.genericHideModal();
    this.genericOpenModal(this.duplicateDiagrammeTemplate, null, null);

  }

  duplicateDiagram(): void {
    const cellsBeforeUpdatingLinkUUID = this.mainGraph.toJSON().cells;
    const duplicatedDiagramAssociations = [];
    const duplicatedDiagramInheritances = [];
    const cellsAfterUpdatingLinkUUID = cellsBeforeUpdatingLinkUUID.map(
      cell => {
        if (cell.type === 'devs.Link') {
          return {
            ...cell,
            id: uuidv4()
          };
        } else {
          return cell;
        }
      }
    );
    cellsAfterUpdatingLinkUUID.forEach(
      cell => {
        if (cell.isAssociationType) {
          duplicatedDiagramAssociations.push(cell.id);
        } else {
          duplicatedDiagramInheritances.push((cell.id));
        }
      }
    );
    const cells = cellsAfterUpdatingLinkUUID.map(
      cell => JSON.stringify(cell));
    const diagram = {
      ...this.currentDiagram,
      libelle: this.duplicatedDiagram.libelle,
      description: this.duplicatedDiagram.description,
      cells,
      associations: duplicatedDiagramAssociations,
      inheritances: duplicatedDiagramInheritances
    };
    if (this.checkFields(diagram)) {
      this.modelizationService.saveDiagram(diagram).subscribe(
        result => {
          this.setIsModified(false);
          this.currentDiagram = {
            ...result, cells: diagram.cells, associations: diagram.associations,
            inheritances: diagram.inheritances
          };
          localStorage.setItem('currentDiagram', JSON.stringify(this.currentDiagram));
          const graphJson = this.currentDiagram.cells.map(cell => JSON.parse(cell));
          const graphJsonObject = {cells: JSON.parse(JSON.stringify(graphJson))};
          this.mainGraph.fromJSON(graphJsonObject);
          this.genericHideModal();
          this.showSuccess();
        }, () => {
          this.showFailure('L\'opération a échoué. Veuillez réessayer à nouveau.\'');
        });
    } else {
      this.showFailure('Merci de remplir tous les champs obligatoires');
    }
  }


  canDeactivate(): any {
    return confirm('Êtes-vous sûr(e) de vouloir quitter la page ? \n' +
      'En cliquant sur le bouton « OK », vous perdrez ' +
      'toutes les informations apportées à cette fiche.');
  }

  setIsModified(value): any {
    DataService.setIsModified(value);
  }

  ngOnDestroy(): void {
    DataService.setIsModified(false);
  }

  updateDiagramMetaData(): any {
    if (this.checkFields(this.diagrammeToEdit)) {
      this.modelizationService.updateDiagramMetaData(this.diagrammeToEdit).subscribe(
        result => {
          this.currentDiagram = this.diagrammeToEdit;
          const diagramBeforeupdate = JSON.parse(localStorage.getItem('currentDiagram'));
          diagramBeforeupdate.libelle = result.libelle;
          diagramBeforeupdate.description = result.description;
          diagramBeforeupdate.filiere = result.filiere;
          localStorage.setItem('currentDiagram', JSON.stringify(diagramBeforeupdate));
          this.showSuccess();
          this.genericHideModal();
        },
        () => {
          this.showFailure('L\'opération a échoué. Veuillez réessayer à nouveau.\'');
        });
    } else {
      this.showFailure('Merci de remplir tous les champs obligatoires');
    }
  }

  isInvalid(value): boolean {
    return this.required && (value === undefined || value === '');
  }

  checkFields(diagram): boolean {
    if (diagram.libelle?.toString() === '' || diagram.libelle === undefined ||
      diagram.filiere === undefined || diagram.filiere?.toString() === '') {
      this.required = true;
      return false;
    } else {
      this.required = false;
      return true;
    }
  }

  changeFiliere(): any {
    this.selectedFamille = '';
    if (this.selectedFiliere !== '') {
      const filiere = this.filieres.filter(f => f.nom === this.selectedFiliere)[0];
      this.familles = filiere.familles;
    }
  }



  changeFamille(): any {
    if (this.selectedFamille !== '')  {
      const famille = this.familles.filter(famille => famille.nom === this.selectedFamille)[0];
      this.sousFamilles = this.selectedFamille.sousFamilles;
    }
    this.filtreObjets();
  }


  changeSFamille(): any {
    this.filtreObjets();
  }

  filtreObjets() {
    this.filtredObjects = JSON.parse(JSON.stringify(this.objects));
    if (this.selectedFiliere !== '') {
      this.filtredObjects = this.filtredObjects.filter(objet => this.selectedFiliere === objet.filieres[0]?.nom);
    }
    if (this.selectedFamille !== '') {
      this.filtredObjects = this.filtredObjects.filter(objet => this.selectedFamille === objet.familles[0]?.nom);
    }
    if (this.selectedSFamille !== '') {
      this.filtredObjects = this.filtredObjects.filter(objet => this.selectedSFamille === objet.sousFamilles[0]?.nom);
    }
    this.initObjects();
  }
}
