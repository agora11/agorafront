export class UsageMetier {
  constructor(usage, id) {
    this.libelle = usage;
    this.idUsageMetier = id;
  }

  idUsageMetier: number;
  libelle: string;
}

export class PolitiqueQualiteParUsages {
  idPolitiqueQualiteParUsage: number;
  tauxSouhaite: number;
  usageMetier: UsageMetier;
}

export class TauxQualiteMesures {
   idTauxQualiteMesure: number;
   dateDeMesure: Date;
   tauxMesure: number;

}

export class PolitiqueQualite {
  idPolitiqueQualite: number;
  indicateurDeQualite: string;
  description: string;
  url: string;
  tauxPromis: number;
  politiqueQualiteParUsages: Array<PolitiqueQualiteParUsages>;
  tauxQualiteMesures: Array<TauxQualiteMesures>;
  usagesObjet: Array<number>;
}

export class Critere {
  constructor(critere) {
    this.libelle = critere;
  }
    idCritereQualite: number;
    libelle: string;
    politiqueQualites: Array<PolitiqueQualite>;

}
