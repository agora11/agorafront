export class Diagramme {
  diagramId: number;
  id: string;
  libelle: string;
  description: string;
  dateCreation: any;
  revision: number;
  idExterne: any;
  dateDebutActivite: any;
  dateFinActivite: any;
  filiere: string;
  cells = [];
  associations: Array<any>;
  inheritances: Array<any>;
}
