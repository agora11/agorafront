
import { Utilisateur } from './utilisateur';
import { SousFamille } from './sousFamille';
import {Filiere} from './filiere';


export class Famille {
    idFamille?: number;
    id?: number;
    responsableDeDonnees?: Utilisateur;
    filiere?: Filiere;
    sousFamilles?: Array<Famille>;
    nom?: string;
    familleMere?: Famille;
    isCollapsed = true;
}

