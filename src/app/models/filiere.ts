import { Utilisateur } from './utilisateur';
import { Famille } from './famille';
import {SuperRessource} from './superRessource';


export class Filiere {
  idFiliere?: number;
  description?: string;
  responsableDeFiliere?: Utilisateur;
  responsableDeFiliereDelegue?: Utilisateur;
  coordinateurDeFiliere?: Utilisateur;
  image?: string | ArrayBuffer;
  slug?: string;
  id?: string;
  familles?: Array<Famille>;
  nom?: string;
  selected?: boolean;
  superRessource: Array<SuperRessource>;
  valideurs?: Array<SuperRessource>;
  redacteurs?: Array<SuperRessource>;
  label: string;
  value: string;
}
