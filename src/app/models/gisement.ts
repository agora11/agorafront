import {Filiere} from './filiere';

export class Acteur {
  idActeur: number;
  contact: string;
  nom: string;
  role: string;
}

export class LienUtile {
  idLienUtile: number;
  libelle: string;
  url: string;
}

export class Gisement {
  idGisement: number;
  libelle: string;
  niveauDeCompletude: number;
  nombreDeFournisseurs: number;
  nombreDeServices: number;
  nombreObjetsReferences: number;
  raisonEtre: string;
  raisonEtreCourte: string;
  servicesDocumentes: number;
  statut: string;
  filieres: Array<Filiere>;
  filiere: string;
  acteurs: Array<Acteur>;
  liensUtiles: Array<LienUtile>;
  description: any;
  date: Date;
  dateMiseEnService: string;
  nombreClientsInternes: number;
  nombreDeServicesExpose: number;
  nombreClientsExternes: number;
  detailsClientsExternes: string;
  nombreServicesWebExposesDataLab: number;
  nombreObjetsReferencesEnProjet: number;
  nombreServicesEnProjet: number;
  nombreServicesWebExposesDataLabEnProjet: number;
}
