import {Role} from './role';

export class GroupeRole {

  idGroupeRole?: number;
  description?: string;
  nom?: string;
  roles?: any;
}
