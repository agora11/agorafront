export class Menu {
  libelle?: string;
  url?: string;
  img?: string;
  sousMenu?: Array<Menu>;
}

export class MenuList {
  public static menuList = new Array<any>(
    {libelle: 'Glossaire', url: '/main/termes', img: 'ic-glossaire'},
    {libelle: 'Objets métier', url: '', img: 'ic-obj-metier', sousMenu: new Array(
        {libelle: 'Liste des objets', url: '/main/objets', img: 'ic-obj-list'},
        {libelle: 'Relation des objets', url: '/main/modelisation', img: 'ic-relation'}
      )},
    {libelle: 'Filières', url: '/main/filieres', img: 'ic-filiere'},
    {libelle: 'Acteurs de la gouvernance', url: '/main/acteurs', img: 'ic-acteur'},
    {libelle: 'Qualité de la donnée', url: '/main/qualites', img: 'ic-qualite'},
    {libelle: 'Gisements', url: '/main/gisements', img: 'ic-gisement'});
}
