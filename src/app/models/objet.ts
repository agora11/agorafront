
import { Filiere } from './filiere';
import { Attribut } from './attribut';
import { Etiquette } from './tag';
import { Famille } from './famille';
import { RessourceGouvernanceDonneeAEtat } from './ressourceGouvernanceDonneeAEtat';
import { Utilisateur } from './utilisateur';
import { Definition } from './definition';
import {Terme} from './terme';
import {Domaine} from './domaine';
import { Statut } from './statut';
import {Critere} from './critere';


export class Objet  extends RessourceGouvernanceDonneeAEtat {
    idClasseDobjet?: number;
    commentaire?: string;
    abreviation?: string;
    libelle?: string;
    filieres?: Array<Filiere>;
    filiere?: string;
    etiquettes?: Array<Etiquette>;
    familles?: Array<Famille>;
    sousFamilles?: Array<Famille>;
    attributs?: Array<Attribut>;
    domaines?: Array<Domaine>;
    responsables?: Array<Utilisateur>;
    definitions?: Array<Definition>;
    definition?: string;
    termes?: Array<Terme>;
    associated?: boolean;
    statut?: Statut;
    proprietaire?: Utilisateur;
    type?: string;
    critereQualites?: Array<Critere>;
    criteresToDelete?: Array<Critere>;
}

