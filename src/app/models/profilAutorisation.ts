import {Utilisateur} from './utilisateur';
import {Role} from './role';

export class IdentiteNumerique {
  constructor(id: number) {
    this.idIdentiteNumerique = id;
  }

  idIdentiteNumerique: number;
  utilisateur: Utilisateur;
}

export class Autorisation {
  idAutorisation: number;
  identiteNumerique: IdentiteNumerique;
}

export class AssociationProfilAutorisationAbstraitRole {
  constructor(role: Role) {
    this.role = role;
  }

  idAssociationProfilAutorisationAbstraitRole: number;
  role: Role;
}

export class ProfilAutorisationAbstrait {
  idProfilAutorisationAbstrait: number;
  autorisations: Array<Autorisation>;
  associationProfilAutorisationAbstraitRoles: Array<AssociationProfilAutorisationAbstraitRole>;
}

export class TypeAutorisation {
  constructor(idTypeAutorisation: number, libelle: string) {
    this.idTypeAutorisation = idTypeAutorisation;
    this.libelle = libelle;
  }

  idTypeAutorisation: number;
  libelle: string;
}

export class AssociationProfilAutorisationTypeAutorisation {
  idAssociationProfilAutorisationTypeAutorisation: number;
  typeAutorisation: TypeAutorisation;
}

export class ProfilAutorisation {
  idProfilAutorisation: number;
  libelle: string;
  profilAutorisationAbstraits: Array<ProfilAutorisationAbstrait>;
  associationProfilAutorisationTypeAutorisations: Array<AssociationProfilAutorisationTypeAutorisation>;
}
