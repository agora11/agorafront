import {ObjetPersistant} from './objetPersistant';

export class RessourceAbstraite extends ObjetPersistant {
    idExterne?: number;
    dateDebutActivite?: string;
    dateFinActivite?: string;
}

