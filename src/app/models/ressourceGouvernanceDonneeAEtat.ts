import {RessourceAbstraiteNommee} from './ressourceAbstraiteNommee';

export class RessourceGouvernanceDonneeAEtat extends RessourceAbstraiteNommee{
    etat?: EtatEnum;
    description?: string;
    codeRoleFonctionnel?: string;


}

export  enum EtatEnum { Actif= 'ACTIF', Archive = 'ARCHIVE', Supprime = 'SUPPRIME'}



