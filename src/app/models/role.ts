

export class Role {

  idRole?: number;

  codeRoleFonctionnel?: string;

  description?: string;

  nom?: string;

  public static REDACTEUR = 'redacteur';
  public static VALIDEUR = 'Valideur';
  public static SUPPLEANT = 'suppleant';
  public static ADMIN = 'admin';
  public static VALIDEURS = new Array({ nom: 'Valideur 1', value: 'Valideur 1' }, { nom: 'Valideur 2', value: 'Valideur 2' }, {
    nom: 'Valideur 3',
    value: 'Valideur 3'
  });
}

