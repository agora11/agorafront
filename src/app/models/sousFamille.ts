
import {Utilisateur} from './utilisateur';


export class SousFamille {
    idSousFamille?: number;
    idFamille?: number;
    referentMetier?: Utilisateur;
    nom?: string;
}

