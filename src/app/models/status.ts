export class Status {
  static BROUILLON = 'Brouillon';
  static PUBLIE = 'Publié';
  static AValider = 'À valider';
  static REFUSER = 'Refusé';
  static Archiver = 'Archivé';
  static SOUMIS = 'Soumis';
  static VALIDE = 'Validé';
  static list = new Array<any>(
    {label: 'À valider', value: 'À valider'}, {label: 'Brouillon', value: 'Brouillon'},
     {label: 'Publié', value: 'Publié'});
}
