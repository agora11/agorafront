import { Utilisateur } from './utilisateur';

export class Statut {

    idStatutTerme?: number;
    valideur?: Utilisateur;
    suppleant?: Utilisateur;
    libelle?: string;
    commentaire?: string;
    dateDebutActivite?: Date;
    nom?: string;
    prenom?: string;
    id?: string;
    nomComplet: any;
}
