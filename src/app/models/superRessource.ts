import {Filiere} from './filiere';
import {ProfilAutorisation} from './profilAutorisation';
import {Utilisateur} from './utilisateur';

export class  SuperRessource {

   idSuperRessource: number;
   profilAutorisations: Array<ProfilAutorisation>;
   nomComplet: string;
   ressource: Utilisateur;
   suppleant: SuperRessource;
   filiere: Filiere;

}
