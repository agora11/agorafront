import { Utilisateur } from './utilisateur';
import { Filiere } from './filiere';
import { Document } from './document';
import { RessourceGouvernanceDonneeAEtat } from './ressourceGouvernanceDonneeAEtat';
import { Definition } from './definition';
import { Objet } from "./objet";
import { Statut } from './statut';


export class Terme extends RessourceGouvernanceDonneeAEtat {

    idTerme?: number;
    abreviation?: string;
    filieres?: Array<Filiere>;
    filiere?: string;
    documents?: Array<Document>;
    definitions?: Array<Definition>;
    definition?: string;
    commentaire?: string;
    libelle?: string;
    objets?: Array<Objet>;
    associated?: boolean;
    statut?: Statut;
    proprietaire?: Utilisateur;
    type?: string;
}

