import { Filiere } from './filiere';
import { Role } from './role';
import {GroupeRole} from './goupeRole';

export class Utilisateur {
    idUtilisateur?: number;
    idIdentiteNumerique?: number;
    prenom?: string;
    nom?: string;
    nomComplet?: string;
    id?: string;
    adresseMail?: string;
    cp?: string;
    poste?: string;
    organisation?: string;
    roles?: Array<Role>;
    familleRoles?: Array<GroupeRole>;
    role: string;
    familleRole: string;
    filieresResponsable?: Array<Filiere>;
    filieresResponsableDelegue?: Array<Filiere>;
    filieresCoordinateur?: Array<Filiere>;
    filiereAssoc: Array<Filiere>;
    ordre: number;
    autorisations: any;
    isAdmin: boolean;
    isValideur: boolean;
    isRedacteur: boolean;
    isResponsable: boolean;
}

