import { CanDeactivate } from '@angular/router';
import {ObjetComponent} from './objet.component';
import {DataService} from '../services/data.service';

export class DeactivateGuardObjet implements CanDeactivate<ObjetComponent> {
  isModified = false;
  canDeactivate(component: ObjetComponent): any {
    DataService.getIsModified().subscribe( isModified => this.isModified = isModified );

    return this.isModified ? component.canDeactivate() : true;
  }
}
