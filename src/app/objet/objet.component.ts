import { AfterViewInit, Component, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { Attribut } from '../models/attribut';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Objet } from '../models/objet';
import { Famille } from '../models/famille';
import { Utilisateur } from '../models/utilisateur';
import { Terme } from '../models/terme';
import { DataService } from '../services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ObjetsService, TermesService } from '../services/api';
import { Filiere } from '../models/filiere';
import { ToastrService } from 'ngx-toastr';
import { Definition } from '../models/definition';
import { Status } from '../models/status';
import { Statut } from '../models/statut';
import { concat, Observable, of, throwError } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { catchError, switchMap } from 'rxjs/operators';
import { AuthGuard } from '../services/auth.service';

@Component({
  selector: 'app-objet',
  templateUrl: './objet.component.html',
  styleUrls: ['./objet.component.css']
})
export class ObjetComponent implements OnInit, AfterViewInit, OnDestroy {

  attr = new Attribut();
  selectedAttr = new Terme();
  modalRef: BsModalRef;
  objet: Objet = new Objet();
  termes: Terme[];
  familles = new Array();
  filieres = [];
  utilisateurs = [];
  filtredTermeAssoc = [];
  filtredTermes = [];
  keyword = 'nomComplet';
  titre = 'Création d’un objet métier';
  titre2 = '';
  motif = '';
  required = false;
  component = 'objet';
  libelleAttrError = false;
  isDisabled: boolean;
  filiereSelectConfig = {
    labelField: 'nom',
    valueField: 'idFiliere',
    searchField: ['nom'],
    dropdownDirection: 'down',
    sortField: [{ field: 'nom', direction: 'asc' }],
  };

  familleSelectConfig = {
    labelField: 'nom',
    valueField: 'idFamille',
    searchField: ['nom'],
    dropdownDirection: 'down',
    sortField: [{ field: 'nom', direction: 'asc' }],
  };
  isModified = false;
  ngbModalOptions = {
    backdrop: true,
    ignoreBackdropClick: true
  };
  termeSubscription: any;
  statusAccessor = Status;
  status = new Array<Statut>();
  allStatus = new Array<Statut>();
  status1: any;
  commentaire = '';
  sFamilles = new Array();
  constructor(private modalService: BsModalService,
    private actRoute: ActivatedRoute,
    private objetsService: ObjetsService, private termeService: TermesService,
    private toastr: ToastrService,
    private router: Router) {
  }

  ngAfterViewInit(): void {
  }

  ngOnInit(): void {
    this.setIsModified(false);
    DataService.getIsModified().subscribe(result => this.isModified = result);

    this.actRoute.data.subscribe((data: any) => {
      this.utilisateurs = data.objet.usersList;
      this.termes = data.termesList.filter(terme => terme.statut?.libelle === this.statusAccessor.PUBLIE);
      this.filtredTermes = this.termes;
      data?.objet?.filieres?.forEach(filiere => {
        if ((AuthGuard.isAdmin() || (this.getCurrentUser().autorisations?.length > 0
          && this.getCurrentUser().autorisations.filter(aut => filiere?.idFiliere === aut.filiere?.idFiliere).length > 0))
          && filiere.superRessource.filter(superRess => superRess.profilAutorisations[0]?.libelle.includes('Valideur'))?.length > 0) {
          this.filieres.push(filiere);
        }
      });
    });

      this.actRoute.queryParams.subscribe(params => {
        if (params.action === 'consult') {
          this.isDisabled = true;
          this.getCurrentData();
          this.initArrays();
          this.titre = 'Objet métier';
          this.titre2 = ' - ' + this.objet.libelle;
        } else if (params.action === 'edit') {
          this.isDisabled = false;
          this.getCurrentData();
          this.initArrays();
          this.titre = 'Objet métier';
          this.titre2 = ' - ' + this.objet.libelle;
        } else {
          this.isDisabled = false;
          this.titre = 'Création d’un objet métier';
          this.titre2 = '';
          this.initObjet();
        }
    });


    this.termeSubscription = DataService.getSearch().subscribe(data => {
      this.component = data.component;
      let result = this.component === 'objet' ? this.objet.termes : this.termes;
      if (data.filiere.length > 0) {
        result = this.termes.filter(terme => data.filiere.includes(terme.filieres[0]?.nom));
      }
      if (data.searchValue !== '') {
        result = result.filter(terme => terme.libelle.toLowerCase().indexOf(data.searchValue.toLowerCase()) > -1
          || terme.definitions[0]?.definition?.toLowerCase().indexOf(data.searchValue.toLowerCase()) > -1);
      }
      if (this.component === 'objet') {
        this.filtredTermeAssoc = result;
      } else {
        this.filtredTermes = result;
      }
    });
  }

  getNextValideur(objet, valideur, typeValideur): Utilisateur {
    let next: Utilisateur;
    const selectedFiliere: Filiere = this.filieres.find((element: Filiere) => `${element.idFiliere}` === `${objet.filieres[0].idFiliere}`);
    const superRessources = selectedFiliere.superRessource.filter(element =>
      element.profilAutorisations != null && element.profilAutorisations[0] != null && element.profilAutorisations[0].libelle.toLocaleLowerCase().indexOf(typeValideur) === 0 &&
      element.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique != null
    ).sort((a, b) => (a.profilAutorisations[0].libelle.toLowerCase() > b.profilAutorisations[0].libelle.toLowerCase()) ? 1 :
      ((b.profilAutorisations[0].libelle.toLowerCase() > a.profilAutorisations[0].libelle.toLowerCase()) ? -1 : 0));

    switch (objet.statut.libelle) {
      case this.statusAccessor.BROUILLON:
        next = objet.proprietaire;
        break;
      case this.statusAccessor.VALIDE:
        next = superRessources[0].profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique.utilisateur;
        break;
      case this.statusAccessor.SOUMIS:
        next = superRessources[0].profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique.utilisateur;
        break;
      case this.statusAccessor.AValider:
        let pos = 0;
        superRessources.forEach((element, index) => {
          if (element.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique.utilisateur.idUtilisateur
            === valideur.idUtilisateur) {
            pos = index + 1;
          }
        });
        if (pos >= superRessources.length) {
          pos = pos - 1;
        }
        next = superRessources[pos].profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique.utilisateur;
        break;
      case this.statusAccessor.REFUSER:
        next = objet.proprietaire;
        break;
      case this.statusAccessor.PUBLIE:
        next = objet.proprietaire;
        break;
      default:
        break;
    }
    return next;
  }

  getNextStatus(object: Objet, operation: string, last: boolean): string {
    let status;

    if ((object.statut == null || object.statut.libelle == null) || object.statut?.libelle === this.statusAccessor.BROUILLON) {
      return operation === 'save' ? this.statusAccessor.BROUILLON : this.statusAccessor.SOUMIS;
    }
    switch (object.statut.libelle) {
      case this.statusAccessor.SOUMIS:
        status = this.statusAccessor.AValider;
        break;
      case this.statusAccessor.AValider:
        status = operation === 'submit' ? this.statusAccessor.VALIDE : this.statusAccessor.REFUSER;
        break;
      case this.statusAccessor.VALIDE:
        status = last ? this.statusAccessor.PUBLIE : this.statusAccessor.AValider;
        break;
      case this.statusAccessor.PUBLIE:
        status = this.statusAccessor.BROUILLON;
        break;
      case this.statusAccessor.REFUSER:
        status = this.statusAccessor.BROUILLON;
        break;
      case this.statusAccessor.BROUILLON:
        status = this.statusAccessor.BROUILLON;
        break;
      default:
        break;
    }

    return status;
  }

  getCurrentData(): any {
    DataService.getCurrentObjet().subscribe(currentData => {
      if (currentData.idClasseDobjet) {
        this.objet = currentData;
        this.initArrays();
        localStorage.setItem('currentData', JSON.stringify(this.objet));
      } else {
        this.objet = JSON.parse(localStorage.getItem('currentData'));
      }
      this.getStatusHistory();
      this.filtredTermeAssoc = this.objet.termes;
      if (this.objet.filieres[0]?.idFiliere) {
        this.changeFiliere(false);
      }
      if (this.objet.familles[0]?.idFamille) {
        this.changeFamille(false);
      }
    });
  }

  initObjet(): any {
    this.objet = {
      commentaire: '',
      abreviation: '',
      libelle: '',
      familles: new Array<Famille>(new Famille()),
      sousFamilles: new Array<Famille>(new Famille()),
      filieres: new Array<Filiere>(new Filiere()),
      responsables: new Array<Utilisateur>(new Utilisateur()),
      attributs: new Array<Attribut>(),
      definitions: new Array<Definition>(new Definition()),
      termes: new Array<Terme>(),
      statut: new Statut()
    };
    this.filtredTermeAssoc = new Array<Terme>();
    this.attr = new Attribut();
    this.status1 = new Statut();
    this.status = new Array();
    this.allStatus = new Array();
    this.setIsModified(false);
  }

  initArrays(): any {
    if (!this.objet.filieres || this.objet.filieres.length === 0) {
      this.objet.filieres = new Array<Filiere>(new Filiere());
    }
    if (!this.objet.familles || this.objet.familles.length === 0) {
      this.objet.familles = new Array<Famille>(new Famille());
    }
    if (!this.objet.sousFamilles || this.objet.sousFamilles.length === 0) {
      this.objet.sousFamilles = new Array<Famille>(new Famille());
    }
    if (!this.objet.responsables || this.objet.responsables.length === 0) {
      this.objet.responsables = new Array<Utilisateur>(new Utilisateur());
    }
    if (!this.objet.definitions || this.objet.definitions.length === 0) {
      this.objet.definitions = new Array<Definition>(new Definition());
    }
    if (!this.objet.attributs) {
      this.objet.attributs = new Array<Attribut>();
    }
  }

  addAttribute(): any {
    if (this.attr.libelle !== undefined && this.attr.libelle !== '') {
      if (this.attr.valeur === undefined) {
        this.attr.valeur = '';
      }
      this.objet.attributs.push(this.attr);
      this.attr = new Attribut();
      this.libelleAttrError = false;
    } else {
      this.libelleAttrError = true;
      this.showFailure('Merci de remplir tous les champs obligatoires');
    }
  }

  openModal(template: TemplateRef<any>): any {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'modal-dialog-centered' }, this.ngbModalOptions)
    );
  }

  openModalTerme(template: TemplateRef<any>): any {
    this.termes.forEach(terme => terme.associated = false);
    if (this.objet.termes) {
      this.objet.termes.forEach(terme => {
        const filtredTerme = this.termes.filter(t => t.idTerme === terme.idTerme)[0];
        if (filtredTerme) { filtredTerme.associated = true; }
      });
    }
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'modal-lg' }, this.ngbModalOptions)
    );
  }

  showSuccess(): any {
    this.toastr.clear();
    this.toastr.success('Opération effectuée avec succès !');
  }

  showFailure(msg): any {
    this.toastr.clear();
    this.toastr.error(msg);
  }

  showWarning(msg): any {
    this.toastr.warning(msg);
  }

  save(statut: string, submit: string): any {
    if (this.checkFields()) {
      if (this.attr.libelle !== undefined && this.attr.libelle !== '') {
        if (this.attr.valeur === undefined) {
          this.attr.valeur = '';
        }
        this.objet.attributs.push(this.attr);
      }
      const savedObject: Objet = JSON.parse(JSON.stringify(this.objet));
      this.attr = new Attribut();
      this.objet.statut = new Statut();
      this.objet.statut.commentaire = this.commentaire;
      this.objet.proprietaire = this.objet.proprietaire != null ? this.objet.proprietaire : this.getCurrentUser();

      if ((submit === 'submit' || submit === 'deny') && savedObject.statut.libelle != null) {
        this.objet.statut.valideur = this.getNextValideur(savedObject, savedObject.statut.valideur, 'valideur');
        this.objet.statut.suppleant = this.getNextValideur(savedObject, savedObject.statut.suppleant, 'suppleant/valideur');
      } else {
        this.objet.statut.valideur = this.getCurrentUser();
        this.objet.statut.suppleant = this.getCurrentUser();
      }

      this.objet.statut.libelle = this.getNextStatus(savedObject, submit, false);

      if (this.objet.idClasseDobjet == null) {
        this.objetsService.postV1Objets(this.objet, 'response').pipe(switchMap((result: HttpResponse<Objet>) => {
          if (this.objet.statut.libelle === this.statusAccessor.BROUILLON) {
            return of(result);
          }
          if (result.headers.get('warning')) {
            this.showWarning(result.headers.get('warning'));
          }
          this.objet.idClasseDobjet = result.body.idClasseDobjet;
          const tempObjet2: Objet = JSON.parse(JSON.stringify(this.objet));
          tempObjet2.statut.libelle = this.getNextStatus(this.objet, 'submit', false);
          tempObjet2.revision = result.body.revision;
          tempObjet2.id = result.body.id;
          tempObjet2.dateCreation = result.body.dateCreation;
          tempObjet2.dateDebutActivite = result.body.dateDebutActivite;
          tempObjet2.dateFinActivite = result.body.dateFinActivite;
          tempObjet2.statut.valideur = this.getNextValideur(this.objet, this.objet.statut.valideur, 'valideur');
          tempObjet2.statut.suppleant = this.getNextValideur(this.objet, this.objet.statut.suppleant, 'suppleant/valideur');
          return this.objetsService.putV1Objet(tempObjet2, 'response');
        }),
          catchError((err: HttpResponse<any>) => throwError(err))
        ).subscribe(result => {
          this.initObjetAfterSubmit(result.body);
          if (result.headers.get('warning')) {
            this.showWarning(result.headers.get('warning'));
          }
          this.refreshPage(result.body);
        }, err => {
          this.objet = savedObject;
          if (err.status === 409) {
            this.hideModal();
            this.showFailure('Le libellé est déjà utilisé sur la même filière');
          } else {
            this.hideModal();
            this.showFailure('L\'opération a échoué. Veuillez réessayer à nouveau.');
          }
        });
      } else {
        const validations: Array<Observable<HttpResponse<Objet>>> = [];
        if (savedObject.statut == null || savedObject.statut.libelle == null) {
          validations.push(this.objetsService.putV1Objet(this.objet, 'response'));
        } else if (savedObject.statut.libelle === this.statusAccessor.BROUILLON && submit === 'save') {
          validations.push(this.objetsService.putV1Objet(this.objet, 'response'));
        } else if (savedObject.statut.libelle === this.statusAccessor.BROUILLON && submit === 'submit') {
          validations.push(this.objetsService.putV1Objet(this.objet, 'response').pipe(switchMap(result => {
            this.objet.idClasseDobjet = result.body.idClasseDobjet;
            const tempObjet2: Objet = JSON.parse(JSON.stringify(this.objet));
            tempObjet2.statut.libelle = this.getNextStatus(this.objet, 'submit', false);
            tempObjet2.revision = result.body.revision;
            tempObjet2.id = result.body.id;
            tempObjet2.dateCreation = result.body.dateCreation;
            tempObjet2.dateDebutActivite = result.body.dateDebutActivite;
            tempObjet2.dateFinActivite = result.body.dateFinActivite;
            tempObjet2.statut.valideur = this.getNextValideur(this.objet, this.objet.statut.valideur, 'valideur');
            tempObjet2.statut.suppleant = this.getNextValideur(this.objet, this.objet.statut.suppleant, 'suppleant/valideur');
            return this.objetsService.putV1Objet(tempObjet2, 'response');
          }
          ))
          );
        } else if (savedObject.statut.libelle === this.statusAccessor.AValider) {
          const tempObjet1: Objet = JSON.parse(JSON.stringify(this.objet));
          const last = this.lastValideur(savedObject);
          const op = this.objet.statut.libelle == this.statusAccessor.REFUSER;
          tempObjet1.statut.valideur = last && !op ? this.objet.statut.valideur : savedObject.statut.valideur;
          tempObjet1.statut.suppleant = last && !op ? this.objet.statut.suppleant : savedObject.statut.suppleant;
          const connectedUser: Utilisateur = JSON.parse(localStorage.getItem('utilisateur'));
          if (connectedUser.nom === tempObjet1.statut.valideur.nom && connectedUser.prenom === tempObjet1.statut.valideur.prenom) {
            tempObjet1.statut.nom = tempObjet1.statut.valideur.nom;
            tempObjet1.statut.prenom = tempObjet1.statut.valideur.prenom;
          } else if (connectedUser.nom === tempObjet1.statut.suppleant.nom && connectedUser.prenom === tempObjet1.statut.suppleant.prenom) {
            tempObjet1.statut.nom = tempObjet1.statut.suppleant.nom;
            tempObjet1.statut.prenom = tempObjet1.statut.suppleant.prenom;
          } else if (AuthGuard.isAdmin()) {
            tempObjet1.statut.nom = connectedUser.nom;
            tempObjet1.statut.prenom = connectedUser.prenom;
          }

          validations.push(this.objetsService.putV1Objet(tempObjet1, 'response').pipe(switchMap((result: HttpResponse<Objet>) => {
            this.objet.idClasseDobjet = result.body.idClasseDobjet;
            const tempObjet2: Objet = JSON.parse(JSON.stringify(this.objet));
            tempObjet2.statut.libelle = this.getNextStatus(this.objet, 'submit', last);
            tempObjet2.statut.commentaire = '';
            this.commentaire = '';
            if (tempObjet2.statut.libelle === this.statusAccessor.BROUILLON) {
              tempObjet2.statut.valideur = savedObject.proprietaire;
              tempObjet2.statut.suppleant = savedObject.proprietaire;
            } else {
              const testObjet = this.objet.statut.libelle === this.statusAccessor.REFUSER ? this.objet : savedObject;
              tempObjet2.statut.valideur = this.getNextValideur(testObjet, testObjet.statut.valideur, 'valideur');
              tempObjet2.statut.suppleant = this.getNextValideur(testObjet, testObjet.statut.suppleant, 'suppleant/valideur');
            }
            return this.objetsService.putV1Objet(tempObjet2, 'response');
          }))
          );

        } else if (savedObject.statut.libelle === this.statusAccessor.PUBLIE) {
          validations.push(this.objetsService.putV1Objet(this.objet, 'response'));
        } else if (savedObject.statut.libelle === this.statusAccessor.VALIDE) {
          validations.push(this.objetsService.putV1Objet(this.objet, 'response'));
        }

        concat(...validations).subscribe(result => {
          this.initObjetAfterSubmit(result.body);
          if (result.headers.get('warning')) {
            this.showWarning(result.headers.get('warning'));
          }
          this.refreshPage(result.body);
        }, err => {
          this.objet = savedObject;
          if (err.status === 409) {
            this.hideModal();
            this.showFailure('Erreur, libellé déjà utilisé sur la même filière');
          } else {
            this.hideModal();
            this.showFailure('L\'opération a échoué. Veuillez réessayer à nouveau.');
          }
        });
      }
    } else {
      this.showFailure('Merci de remplir tous les champs obligatoires');
    }
  }

  submit(): any {
    this.save(this.statusAccessor.AValider, 'submit');
  }

  valider(): any {
    this.save(this.statusAccessor.AValider, 'submit');
  }

  refuser(): any {
    this.save(this.statusAccessor.AValider, 'deny');
  }

  initObjetAfterSubmit(result: Objet): any {
    this.objet.id = result.id;
    this.objet.idClasseDobjet = result.idClasseDobjet;
    this.objet.revision = result.revision;
    this.objet.dateCreation = result.dateCreation;
    if (result.statut != null) {
      this.objet.statut = result.statut;
    }

    DataService.changeObjetData(this.objet);
    this.showSuccess();
    this.titre = 'Objet métier';
    this.titre2 = ' - ' + this.objet.libelle;
    this.setIsModified(false);
  }

  checkFields(): boolean {
    this.libelleAttrError = false;
    let attrs = new Array<Attribut>();
    if (this.objet.attributs) {
      attrs = this.objet.attributs.filter(attr => attr.libelle === undefined || attr.libelle === '');
    }
    if (this.objet.libelle.toString() === '' || this.objet.filieres[0] === undefined ||
           this.objet.filieres[0]?.idFiliere === undefined || this.objet.filieres[0]?.idFiliere?.toString() === '' || attrs.length > 0) {
      this.required = true;
      return false;
    } else {
      this.required = false;
      return true;
    }
  }

  redirect(): any {
    this.hideModal();
    this.setIsModified(false);
    this.router.navigateByUrl('/main/objets');
  }

  setIsModified(value): any {
    DataService.setIsModified(value);
  }

  ngOnDestroy(): any {
    this.hideModal();
    this.toastr.clear();
    this.termeSubscription.unsubscribe();
    DataService.changeObjetData(new Objet());
    DataService.pushSearch({ filiere: [], roles: [], searchValue: '', component: 'objet' });
  }

  associerTermes(): any {
    this.objet.termes = this.termes.filter(o => o.associated);
    this.filtredTermeAssoc = this.objet.termes;
    DataService.pushSearch({ filiere: [], roles: [], searchValue: '', component: 'objet' });
    this.setIsModified(true);
    this.modalRef.hide();
  }

  removeTerme(index): any {
    this.setIsModified(true);
    this.objet.termes.splice(index, 1);
  }

  createAttr(): any {
    this.attr.libelle = this.selectedAttr.libelle;
    this.attr.valeur = this.selectedAttr.definitions[0]?.definition;
    this.selectedAttr = new Terme();
    this.setIsModified(true);
    this.hideModal();
  }

  hideModal(): any {
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }

  isInvalid(value): boolean {
    return this.required && (value === undefined || value === '');
  }

  canDeactivate(): any {
    return confirm('Êtes-vous sûr(e) de vouloir quitter la page ? \n' +
      'En cliquant sur le bouton « OK », vous perdrez ' +
      'toutes les informations apportées à cette fiche.');
  }

  changeFiliere(isModified): any {
    if (this.objet.idClasseDobjet != null
      || (this.objet.idClasseDobjet == null
        && this.objet.filieres[0].idFiliere != null && this.objet.filieres[0].idFiliere.toString() !== '')) {
      this.familles = this.filieres.filter(filiere => filiere.idFiliere === +this.objet.filieres[0].idFiliere)[0]?.familles;
      this.setIsModified(isModified);
    }
  }

  changeFamille(isModified): any {
    if (this.objet.idClasseDobjet != null
      || (this.objet.idClasseDobjet == null
        && this.objet.familles[0].idFamille != null && this.objet.familles[0].idFamille.toString() !== '')) {
      this.sFamilles = this.familles.filter(famille => famille.idFamille === +this.objet.familles[0].idFamille)[0]?.sousFamilles;
      this.setIsModified(isModified);
    }
  }

  lastValideur(objet: Objet): boolean {
    let pos = 0;
    const selectedFiliere: Filiere = this.filieres.find((element: Filiere) => `${element.idFiliere}` === `${objet.filieres[0].idFiliere}`);
    const superRessources = selectedFiliere.superRessource.filter(element =>
      element.profilAutorisations != null && element.profilAutorisations[0] != null && element.profilAutorisations[0].libelle.toLocaleLowerCase().indexOf('valideur') !== -1 &&
      element.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique != null
    ).sort((a, b) => (a.profilAutorisations[0].libelle.toLowerCase() > b.profilAutorisations[0].libelle.toLowerCase()) ? 1 :
      ((b.profilAutorisations[0].libelle.toLowerCase() > a.profilAutorisations[0].libelle.toLowerCase()) ? -1 : 0));
    superRessources.forEach((element, index) => {
      if (element.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique.utilisateur.idUtilisateur
        === objet.statut.valideur.idUtilisateur) {
        pos = index + 1;
      }
    });

    return pos >= superRessources.length ? true : false;

  }

  private getStatusHistory(): any {
    this.allStatus = [];
    this.status1 = new Statut();
    const status = [];
    this.objetsService.getStatusHistory(this.objet.id).subscribe(result => {
      this.allStatus = JSON.parse(JSON.stringify(result));
      result = result.filter(item => item.libelle !== this.statusAccessor.BROUILLON);
      if (result?.length > 0) {
        this.status1 = result[0];
        result.splice(0, 1);
      }
      result.forEach((s, index) => {
        if ((s.libelle === this.statusAccessor.AValider && index === result.length - 1)
          || (s.libelle === this.statusAccessor.REFUSER || s.libelle === this.statusAccessor.VALIDE
            || s.libelle === this.statusAccessor.SOUMIS)) {
          status.push(s);
        }
      });
      this.status = JSON.parse(JSON.stringify(status));
    }, () => { });
  }

  showButton(save: string): boolean {
    switch (save) {
      case 'save':
        return this.isModified;
        break;
      case 'submit':
        return (this.objet.statut?.libelle === undefined && this.isModified)
          || (this.allStatus[this.allStatus?.length - 1]?.libelle === this.statusAccessor.BROUILLON
            && (this.objet.proprietaire.idUtilisateur === this.getCurrentUser().idUtilisateur || AuthGuard.isAdmin()
              || AuthGuard.isValideur(this.objet.filieres[0]) || AuthGuard.isRedacteur(this.objet.filieres[0])));
        break;
      case 'corriger':
        return this.allStatus[this.allStatus?.length - 1]?.libelle === this.statusAccessor.PUBLIE
          && (this.isValidator() || AuthGuard.isAdmin());

      case 'valider':
        return this.allStatus[this.allStatus?.length - 1]?.libelle === this.statusAccessor.AValider
          && (this.allStatus[this.allStatus?.length - 1]?.valideur?.idUtilisateur === this.getCurrentUser().idUtilisateur
            || this.allStatus[this.allStatus?.length - 1]?.suppleant?.idUtilisateur === this.getCurrentUser().idUtilisateur
            || AuthGuard.isAdmin());

      default:
        return false;
    }
  }

  private getCurrentUser(): Utilisateur {
    return (JSON.parse(localStorage.getItem('utilisateur')) as Utilisateur);
  }

  private isValidator(): boolean {
    return this.allStatus.filter(statut => statut.suppleant?.idUtilisateur === this.getCurrentUser().idUtilisateur
      || statut.valideur?.idUtilisateur === this.getCurrentUser().idUtilisateur)?.length > 0;
  }

  private refreshPage(objet): any {
    this.router.navigate(['/main/objet'], {
      relativeTo: this.actRoute,
      queryParams: {
        action: (objet.statut === null || objet.statut?.libelle === this.statusAccessor.BROUILLON) ? 'edit' : 'consult'
      }
    });
  }
}


