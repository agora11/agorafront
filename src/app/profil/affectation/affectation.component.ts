import { AfterViewInit, Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UtilisateursService } from '../../services/utilisateurs.service';
import { Utilisateur } from '../../models/utilisateur';
import { Filiere } from '../../models/filiere';
import {
  AssociationProfilAutorisationAbstraitRole,
  AssociationProfilAutorisationTypeAutorisation,
  Autorisation,
  IdentiteNumerique,
  ProfilAutorisation,
  ProfilAutorisationAbstrait, TypeAutorisation
} from '../../models/profilAutorisation';
import { SuperRessource } from '../../models/superRessource';
import { ToastrService } from 'ngx-toastr';
import { FilieresService } from '../../services/filieres.service';
import { AuthGuard } from '../../services/auth.service';
import { concat, Observable, of } from 'rxjs';
import { DataService } from '../../services/data.service';
import { Role } from '../../models/role';

@Component({
  selector: 'app-affectation',
  templateUrl: './affectation.component.html',
  styleUrls: ['./affectation.component.css', '../profil.component.css']
})
export class AffectationComponent implements OnInit, AfterViewInit {
  filieres = new Array({});
  filiere = new Filiere();
  acteurs = [];
  currentUser: Utilisateur;
  acteur: Utilisateur;
  roles = new Array<any>();
  modalRef: BsModalRef;
  ngbModalOptions = {
    backdrop: true,
    ignoreBackdropClick: true
  };
  selectConfig = {
    labelField: 'nom',
    valueField: 'value',
    searchField: ['nom'],
    dropdownDirection: 'down',
    sortField: [{ field: 'nom', direction: 'asc' }],
  };
  valideurs = Role.VALIDEURS;
  filiereAssoc = new Array();
  intervenant: any;
  selectConfigFiliere = {
    labelField: 'nom',
    valueField: 'idFiliere',
    searchField: ['nom'],
    dropdownDirection: 'down',
    sortField: [{ field: 'nom', direction: 'asc' }],
  };
  isModified = false;

  toEditList = new Array<SuperRessource>();
  toDeleteList = new Array<SuperRessource>();
  index = -1;
  private filtredActeurs: any[];
  private required = false;

  constructor(private modalService: BsModalService, private utilisateurService: UtilisateursService,
              private toastr: ToastrService, private filiereService: FilieresService) {
  }

  ngOnInit(): void {
    this.setIsModified(false);
    DataService.getIsModified().subscribe(result => this.isModified = result);
    this.utilisateurService.getV1Utilisateurs().subscribe(result => {
      this.acteurs = result.sort((a, b) => (a.nomComplet.toLowerCase() > b.nomComplet.toLowerCase()) ? 1 : ((b.nomComplet.toLowerCase() > a.nomComplet.toLowerCase()) ? -1 : 0));
      this.filtredActeurs = this.acteurs;
    });

    DataService.getSearch().subscribe(data => {
      this.filtredActeurs = this.acteurs;
      if (data.roles?.length > 0) {
        this.filtredActeurs = this.acteurs.filter(acteur => data.roles.includes(acteur.roles[0]?.nom));
      }
      if (data.searchValue !== '') {
        this.filtredActeurs = this.filtredActeurs.filter(acteur => acteur.nomComplet.toLowerCase().indexOf(data.searchValue.toLowerCase()) > -1);
      }
    });
    this.getConnectedUser();
  }

  ngAfterViewInit(): void {
  }

  getConnectedUser(): any {
    AuthGuard.getConnectedUser().subscribe(currectUser => {
      if (currectUser) {
        this.currentUser = currectUser;
        if (AuthGuard.isAdmin()) {
           this.filiereService.getV1Filieres(undefined, undefined, undefined, 'nom').subscribe(result => {
             this.currentUser.filiereAssoc = result;
             result.forEach(item => {
               this.filiereAssoc.push({ nom: item.nom, idFiliere: item.idFiliere });
             });
             if (this.filiere?.idFiliere === undefined) {
               this.filiere = JSON.parse(JSON.stringify(this.currentUser.filiereAssoc[0]));
               this.initList();
             } else {
               this.changeFiliere();
             }
           });
        } else if (currectUser?.filiereAssoc) {
          currectUser.filiereAssoc.forEach(item => {
            this.filiereAssoc.push({ nom: item.nom, idFiliere: item.idFiliere });
          });
          this.filiereAssoc.sort((a, b) => (a.nom > b.nom) ? 1 : ((b.nom > a.nom) ? -1 : 0));
          if (this.filiere?.idFiliere === undefined) {
            this.filiere = JSON.parse(JSON.stringify(this.currentUser.filiereAssoc[0]));
            this.initList();
          } else {
            this.changeFiliere();
          }
        }
      }
    });
  }

  /**
   * initialiser liste valideurs / rédacteurs
   * @private
   */
  private initList(): any {
    this.filiere.valideurs = new Array<SuperRessource>();
    this.filiere.redacteurs = new Array<SuperRessource>();
    this.filiere.superRessource.forEach(superRes => {
      if (superRes.profilAutorisations?.length > 0) {
        superRes.ressource = superRes.profilAutorisations[0]?.profilAutorisationAbstraits[0]?.autorisations[0]?.identiteNumerique?.utilisateur;
        if (superRes.ressource) {
          if (!superRes.ressource.roles) {
            superRes.ressource.roles = new Array<Role>();
          }
          superRes.ressource.roles[0] = superRes.profilAutorisations[0]?.profilAutorisationAbstraits[0]?.associationProfilAutorisationAbstraitRoles[0]?.role;
          superRes.ressource.nomComplet =
            superRes.profilAutorisations[0]?.profilAutorisationAbstraits[0]?.autorisations[0]?.identiteNumerique?.utilisateur?.prenom + ' ' +
            superRes.profilAutorisations[0]?.profilAutorisationAbstraits[0]?.autorisations[0]?.identiteNumerique?.utilisateur?.nom;
          if (superRes.profilAutorisations[0]?.libelle.indexOf(Role.VALIDEUR) >= 0
              && superRes.profilAutorisations[0]?.libelle.indexOf(Role.SUPPLEANT) < 0) {
            superRes = this.getSuppleant(superRes);
            this.filiere.valideurs.push(superRes);
          } else if (superRes.profilAutorisations[0]?.libelle.indexOf(Role.REDACTEUR) >= 0) {
            this.filiere.redacteurs.push(superRes);
          }
        }
      }
    });
    this.filiere.valideurs.sort((a, b) => (a.profilAutorisations[0].libelle > b.profilAutorisations[0].libelle) ? 1 : ((b.profilAutorisations[0].libelle > a.profilAutorisations[0].libelle) ? -1 : 0));
    this.filiere.redacteurs.sort((a, b) => (a.ressource.nomComplet > b.ressource.nomComplet) ? 1 : ((b.ressource.nomComplet > a.ressource.nomComplet) ? -1 : 0));
    this.filiere.superRessource = new Array<SuperRessource>();
  }

  openModal(template: TemplateRef<any>, intervenant, modal, index): any {
    this.index = index;
    this.acteurs.forEach(acteur => acteur.associated = false);
    if (this.filiere.redacteurs?.length > 0) {
      this.filiere.redacteurs.forEach(redac => {
        const filtredAct = this.acteurs.filter(t => t.idUtilisateur === redac.ressource.idUtilisateur)[0];
        if (filtredAct) { filtredAct.associated = true; }
      });
    }
    if (intervenant) {
      this.intervenant = JSON.parse(JSON.stringify(intervenant));
    } else {
      this.intervenant = new SuperRessource();
      this.intervenant.profilAutorisations = new Array<ProfilAutorisation>(new ProfilAutorisation());
    }
    this.intervenant.suppleant = this.intervenant.suppleant ? this.intervenant.suppleant : new SuperRessource();
    this.valideurs = Role.VALIDEURS.filter(val => this.filiere.valideurs?.filter(valideur => valideur.profilAutorisations[0].libelle === val.nom)?.length <= 0 || this.intervenant.profilAutorisations[0]?.libelle === val.nom);
    this.required = false;
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: modal }, this.ngbModalOptions)
    );
  }

  affecterActeur(): any {
    const redac = this.acteurs.filter(o => o.associated);
    redac.forEach(r => {
      if (this.filiere.redacteurs.filter(redact => redact.ressource.idUtilisateur === r.idUtilisateur).length === 0) {
        const superRess = new SuperRessource();
        superRess.ressource = r;
        superRess.profilAutorisations = new Array<ProfilAutorisation>(new ProfilAutorisation());
        superRess.profilAutorisations[0].libelle = Role.REDACTEUR;
        superRess.profilAutorisations[0].associationProfilAutorisationTypeAutorisations = new Array<AssociationProfilAutorisationTypeAutorisation>(new AssociationProfilAutorisationTypeAutorisation());
        superRess.profilAutorisations[0].associationProfilAutorisationTypeAutorisations[0].typeAutorisation = new TypeAutorisation(1, 'ecriture');
        superRess.profilAutorisations[0].profilAutorisationAbstraits = new Array<ProfilAutorisationAbstrait>(new ProfilAutorisationAbstrait());
        superRess.profilAutorisations[0].profilAutorisationAbstraits[0].associationProfilAutorisationAbstraitRoles = new Array<AssociationProfilAutorisationAbstraitRole>(new AssociationProfilAutorisationAbstraitRole(r.roles[0]));
        superRess.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations = new Array<Autorisation>(new Autorisation());
        superRess.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique = new IdentiteNumerique(r.idIdentiteNumerique);
        superRess.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique.utilisateur = r;
        this.filiere.redacteurs.push(superRess);
      }
    });
    this.filiere.redacteurs.sort((a, b) => (a.ressource.nomComplet > b.ressource.nomComplet) ? 1 : ((b.ressource.nomComplet > a.ressource.nomComplet) ? -1 : 0));
    this.toDeleteList = [];
    this.filiere.redacteurs.forEach(item => {
      if (redac.filter(ress => ress.idUtilisateur === item.ressource.idUtilisateur).length === 0) {
        if (item.idSuperRessource !== undefined) {
          this.toDeleteList.push(item);
        }
        this.filiere.redacteurs = this.filiere.redacteurs.filter(r => r.ressource.idUtilisateur !== item.ressource.idUtilisateur);
      }
    });

    this.modalRef.hide();
    this.setIsModified(true);
  }

  showActeur(acteur: any, template: TemplateRef<any>): any {
    this.acteur = acteur;
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'modal-lg' })
    );
  }

  affecterValideur(): any {
    if (this.checkFields()) {
    if (!this.filiere.valideurs) {
      this.filiere.valideurs = new Array<SuperRessource>();
    }
    if (this.intervenant.idSuperRessource) {
      this.intervenant.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique.identiteNumerique = this.intervenant.ressource.idIdentiteNumerique;
      this.intervenant.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique.utilisateur = this.intervenant.ressource;
      this.toEditList.push(this.intervenant);
      this.filiere.valideurs[this.index] = this.intervenant;
    } else {
      this.intervenant.profilAutorisations[0].associationProfilAutorisationTypeAutorisations = new Array<AssociationProfilAutorisationTypeAutorisation>(new AssociationProfilAutorisationTypeAutorisation());
      this.intervenant.profilAutorisations[0].associationProfilAutorisationTypeAutorisations[0].typeAutorisation = new TypeAutorisation(1, 'ecriture');
      this.intervenant.profilAutorisations[0].profilAutorisationAbstraits = new Array<ProfilAutorisationAbstrait>(new ProfilAutorisationAbstrait());
      this.intervenant.profilAutorisations[0].profilAutorisationAbstraits[0].associationProfilAutorisationAbstraitRoles = new Array<AssociationProfilAutorisationAbstraitRole>(new AssociationProfilAutorisationAbstraitRole(this.intervenant.ressource.roles[0]));
      this.intervenant.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations = new Array<Autorisation>(new Autorisation());
      this.intervenant.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique = new IdentiteNumerique(this.intervenant.ressource.idIdentiteNumerique);
      this.intervenant.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique.utilisateur = this.intervenant.ressource;
      if (this.index >= 0) {
        this.filiere.valideurs.splice(this.index, 1);
      }
      this.filiere.valideurs.push(this.intervenant);
    }
      this.filiere.valideurs.sort((a, b) => (a.profilAutorisations[0].libelle > b.profilAutorisations[0].libelle) ? 1 : ((b.profilAutorisations[0].libelle > a.profilAutorisations[0].libelle) ? -1 : 0));
      this.modalRef.hide();
    this.setIsModified(true);
    } else {
      this.showFailure('Merci de remplir tous les champs obligatoires');
    }
  }

  annuler(): any {
    this.getConnectedUser();
    this.modalRef.hide();
    this.setIsModified(false);
  }

  save(): any {
    const listToAdd = this.filiere.valideurs.filter(ress => ress.idSuperRessource === undefined).concat(
      this.filiere.redacteurs.filter(ress => ress.idSuperRessource === undefined)
    );
    const observables = new Array<Observable<any>>();
    if (this.toEditList.length > 0) {
      const filiereCloneEdit = JSON.parse(JSON.stringify(this.filiere));
      filiereCloneEdit.superRessource = this.toEditList;
      this.toEditList.forEach(intervenant => {
        const suppleant = JSON.parse(JSON.stringify(intervenant.suppleant));

        if (intervenant.suppleant?.ressource && intervenant.suppleant?.idSuperRessource === undefined) {
          suppleant.profilAutorisations = new Array<ProfilAutorisation>(new ProfilAutorisation());
          suppleant.profilAutorisations[0].associationProfilAutorisationTypeAutorisations = new Array<AssociationProfilAutorisationTypeAutorisation>(new AssociationProfilAutorisationTypeAutorisation());
          suppleant.profilAutorisations[0].associationProfilAutorisationTypeAutorisations[0].typeAutorisation = new TypeAutorisation(1, 'ecriture');
          suppleant.profilAutorisations[0].profilAutorisationAbstraits = new Array<ProfilAutorisationAbstrait>(new ProfilAutorisationAbstrait());
          suppleant.profilAutorisations[0].profilAutorisationAbstraits[0].associationProfilAutorisationAbstraitRoles = new Array<AssociationProfilAutorisationAbstraitRole>(new AssociationProfilAutorisationAbstraitRole(this.intervenant.ressource.roles[0]));
          suppleant.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations = new Array<Autorisation>(new Autorisation());
          suppleant.profilAutorisations[0].libelle = 'suppleant/' + intervenant.profilAutorisations[0].libelle;
          suppleant.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique = new IdentiteNumerique(suppleant.ressource.idIdentiteNumerique);
          suppleant.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique.utilisateur = suppleant.ressource;
          listToAdd.push(suppleant);
        } else if (intervenant.suppleant?.ressource && intervenant.suppleant?.idSuperRessource) {
          suppleant.profilAutorisations[0].libelle = 'suppleant/' + intervenant.profilAutorisations[0].libelle;
          suppleant.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique = new IdentiteNumerique(suppleant.ressource.idIdentiteNumerique);
          suppleant.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique.utilisateur = suppleant.ressource;
          filiereCloneEdit.superRessource.push(suppleant);
        } else {
          this.toDeleteList.push(suppleant);
        }
      });
      observables.push(this.filiereService.putV1Intervenant(filiereCloneEdit));
      this.toEditList = [];
    }
    if (listToAdd.length > 0) {
      const filiereCloneAdd = JSON.parse(JSON.stringify(this.filiere));
      filiereCloneAdd.superRessource = listToAdd;
      listToAdd.forEach(intervenant => {
        if (intervenant.suppleant?.ressource) {
          const suppleantToAdd = JSON.parse(JSON.stringify(intervenant));
          suppleantToAdd.profilAutorisations[0].libelle = 'suppleant/' + intervenant.profilAutorisations[0].libelle;
          suppleantToAdd.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique = new IdentiteNumerique(suppleantToAdd.suppleant.ressource.idIdentiteNumerique);
          suppleantToAdd.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique.utilisateur = suppleantToAdd.suppleant.ressource;
          filiereCloneAdd.superRessource.push(suppleantToAdd);
        }
      });
      observables.push(this.filiereService.postV1Intervenant(filiereCloneAdd));
    }
    if (this.toDeleteList.length > 0) {
      const filiereCloneDelete = JSON.parse(JSON.stringify(this.filiere));
      filiereCloneDelete.superRessource = this.toDeleteList;
      observables.push(this.filiereService.deleteV1Intervenant(filiereCloneDelete));
      this.toDeleteList = [];
    }
    observables.push(of('last'));

    concat(...observables).subscribe(result => {
      if (result === 'last') {
        this.initUserAfterSave();
        this.showSuccess();
        this.setIsModified(false);
      }
    }, () => {
      this.showFailure('L\'opération a échoué. Veuillez réessayer à nouveau.');
    });
  }

  initUserAfterSave(): any {
    this.utilisateurService.getV1Utilisateurs(undefined, undefined, undefined, undefined, undefined, this.currentUser.cp)
      .subscribe(data => {
        AuthGuard.setConnectedUser(data[0]);
      });
  }

  removeIntervenant(intervenant: SuperRessource, list: Array<SuperRessource>): any {
    if (intervenant.idSuperRessource) {
      this.filiere.superRessource = new Array<SuperRessource>(intervenant);
      if (intervenant.suppleant?.idSuperRessource) {
        this.filiere.superRessource.push(intervenant.suppleant);
      }
      this.filiereService.deleteV1Intervenant(this.filiere).subscribe(() => {
        this.initUserAfterSave();
        this.showSuccess();
        list.splice(this.index, 1);
        this.modalRef.hide();
      }, () => {
        this.showFailure('L\'opération a échoué. Veuillez réessayer à nouveau.');
      });
    } else {
      this.showSuccess();
      list.splice(this.index, 1);
      this.modalRef.hide();
    }
  }

  showSuccess(): any {
    this.toastr.clear();
    this.toastr.success('Opération effectuée avec succès !');
  }

  showFailure(msg): any {
    this.toastr.clear();
    this.toastr.error(msg);
  }

  changeFiliere(): any {
    if (this.filiere && this.filiere.idFiliere !== undefined && this.filiere.idFiliere.toString() !== '') {
      this.filiere = JSON.parse(JSON.stringify(this.currentUser.filiereAssoc.filter(item => item.idFiliere === +this.filiere.idFiliere)[0]));
      this.initList();
    }
  }

  private getSuppleant(superRes: SuperRessource): SuperRessource {
    this.filiere.superRessource?.forEach(s => {
      if (s.profilAutorisations[0] != null && s.profilAutorisations[0].libelle.includes(superRes.profilAutorisations[0]?.libelle)
          && s.profilAutorisations[0].libelle.indexOf(Role.SUPPLEANT) >= 0) {
        superRes.suppleant = s;
        superRes.suppleant.ressource = s.profilAutorisations[0]?.profilAutorisationAbstraits[0]?.autorisations[0]?.identiteNumerique?.utilisateur;
      }
    });
    return superRes;
  }

  canDeactivate(): any {
    return confirm('Êtes-vous sûr(e) de vouloir quitter la page ? \n' +
      'En cliquant sur le bouton « OK », vous perdrez ' +
      'toutes les informations apportées à cette fiche.');
  }

  setIsModified(value): any {
    DataService.setIsModified(value);
  }

  isInvalid(value): boolean {
    return this.required && (value === undefined || value === '');
  }

  checkFields(): boolean {
    if (this.intervenant.ressource === undefined || this.intervenant.ressource === ''
        || this.intervenant.profilAutorisations[0] === undefined
         || this.intervenant.profilAutorisations[0]?.libelle === '' ||
      this.intervenant.suppleant.ressource  === undefined || this.intervenant.suppleant.ressource === '') {
      this.required = true;
      return false;
    } else {
      this.required = false;
      return true;
    }
  }
}
