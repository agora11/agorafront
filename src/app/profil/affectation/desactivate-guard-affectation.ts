import {CanDeactivate} from '@angular/router';
import {AffectationComponent} from './affectation.component';
import {DataService} from '../../services/data.service';

export class DesactivateGuardAffectation implements CanDeactivate<AffectationComponent> {
  isModified = false;
  canDeactivate(component: AffectationComponent): any {
    DataService.getIsModified().subscribe( isModified => this.isModified = isModified );

    return this.isModified ? component.canDeactivate() : true;
  }
}
