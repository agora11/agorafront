import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FilieresService } from '../../services/filieres.service';
import { ProfilService } from '../../services/profil.service';
import { ObjetsService } from '../../services/objets.service';
import { TermesService } from '../../services/termes.service';
import { Objet } from '../../models/objet';
import { DataService } from '../../services/data.service';
import { Terme } from '../../models/terme';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import {AuthGuard} from '../../services/auth.service';
import {Status} from '../../models/status';
import {switchMap} from 'rxjs/operators';
import {concat, Observable} from 'rxjs';
import {HttpResponse} from '@angular/common/http';
import {Utilisateur} from '../../models/utilisateur';
import {Filiere} from '../../models/filiere';

@Component({
  selector: 'app-brouillon',
  templateUrl: './brouillon.component.html',
  styleUrls: ['./brouillon.component.css', '../profil.component.css']
})
export class BrouillonComponent implements OnInit {

  brouillons = new Array<any>();
  filieres = new Array<any>();
  types: any;
  modalRef: BsModalRef;
  terme: Terme = new Terme();
  objet: Objet = new Objet();
  constructor(private filiereService: FilieresService, private objetService: ObjetsService,
              private termeService: TermesService, private modalService: BsModalService,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    this.types = ProfilService.getTypes();
    AuthGuard.getConnectedUser().subscribe(acteur => {
      this.filiereService.getV1Filieres(undefined, undefined, undefined, 'nom').subscribe(result => {
          result?.forEach(filiere => {
            if (acteur.autorisations?.length > 0 && acteur.autorisations.filter(aut => aut.filiere && filiere.idFiliere === aut.filiere?.idFiliere).length > 0) {
              filiere.label = filiere.nom;
              filiere.value = filiere.nom;
              this.filieres.push(filiere);
            }
          });
      });
      if (acteur?.idIdentiteNumerique) {
      this.termeService.getV1Termes(undefined, acteur.idIdentiteNumerique, acteur.idIdentiteNumerique, Status.BROUILLON, undefined, undefined, 'libelle')
        .subscribe(termes => {
          this.objetService.getV1Objets(undefined, acteur.idIdentiteNumerique, acteur.idIdentiteNumerique, Status.BROUILLON, undefined, undefined, undefined, 'libelle')
            .subscribe(objets => {
              termes.forEach(terme => {
                terme.type = 'Terme métier';
                terme.filiere = terme.filieres[0]?.nom;
                terme.definition = terme.definitions[0].definition;
                this.brouillons.push(terme);
              });
              objets.forEach(objet => {
                objet.type = 'Objet métier';
                objet.filiere = objet.filieres[0]?.nom;
                objet.definition = objet.definitions[0].definition;
                this.brouillons.push(objet);
              });
              this.brouillons.sort((a, b) => (a.libelle.toLowerCase() > b.libelle.toLowerCase()) ? 1 : ((b.libelle.toLowerCase() > a.libelle.toLowerCase()) ? -1 : 0))
            });
        });
      }
    });
  }

  editObjet(objet: Objet): any {
    DataService.changeObjetData(objet);
  }

  editTerme(terme: Terme): any {
    DataService.changeTermeData(terme);
  }

  archiveObjet(objet: Objet): any {
    this.objetService.deleteV1ObjetsId(objet.idClasseDobjet).subscribe(() => {
      this.brouillons = this.brouillons.filter(item => item.id !== objet.id);
      this.showSuccess();
      this.modalRef.hide();
    }, () => {
      this.showFailure();
      this.modalRef.hide();
    });
  }

  archiveTerme(terme: Terme): any {
    this.termeService.deleteV1TermesId(terme.idTerme).toPromise().then(() => {
      this.brouillons = this.brouillons.filter(item => item.id !== terme.id);
      this.showSuccess();
      this.modalRef.hide();
    }, () => {
      this.showFailure();
      this.modalRef.hide();
    });
  }

  openModal(item, template: TemplateRef<any>): any {
    this.terme = item;
    this.objet = item;
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'modal-dialog-centered' })
    );
  }

  showSuccess(): any {
    this.toastr.success('Opération effectuée avec succès !');
  }

  showFailure(): any {
    this.toastr.error('L\'opération a échoué. Veuillez réessayer à nouveau.');
  }

  submitObjet(objet: any): any {
    objet.statut.valideur = this.getNextValideur(objet, objet.statut.valideur, 'valideur');
    objet.statut.suppleant = this.getNextValideur(objet, objet.statut.suppleant, 'suppleant/valideur');
    objet.statut.libelle = Status.SOUMIS;
    const validations: Array<Observable<HttpResponse<Objet>>> = [];
    validations.push(this.objetService.putV1Objet(objet, 'response').pipe(switchMap(result => {
        objet.idClasseDobjet = result.body.idClasseDobjet;
        const tempObjet2: Objet = JSON.parse(JSON.stringify(objet));
        tempObjet2.statut.libelle = this.getNextStatus(objet, 'submit', false);
        tempObjet2.revision = result.body.revision;
        tempObjet2.id = result.body.id;
        tempObjet2.dateCreation = result.body.dateCreation;
        tempObjet2.dateDebutActivite = result.body.dateDebutActivite;
        tempObjet2.dateFinActivite = result.body.dateFinActivite;
        tempObjet2.statut.valideur = this.getNextValideur(objet, objet.statut.valideur, 'valideur');
        tempObjet2.statut.suppleant = this.getNextValideur(objet, objet.statut.suppleant, 'suppleant/valideur');
        return this.objetService.putV1Objet(tempObjet2, 'response');
      }
      ))
    );

    concat(...validations).subscribe(result => {
      this.brouillons = this.brouillons.filter(b => b.id !== objet.id);
      this.showSuccess();
    }, err => {
        this.modalRef.hide();
        this.showFailure();
    });
  }

  submitTerme(terme: any): any {
    terme.statut.valideur = this.getNextValideur(terme, terme.statut.valideur, 'valideur');
    terme.statut.suppleant = this.getNextValideur(terme, terme.statut.suppleant, 'suppleant/valideur');
    terme.statut.libelle = Status.SOUMIS;
    const validations: Array<Observable<HttpResponse<Objet>>> = [];
    validations.push(this.termeService.putV1Termes(terme, 'response').pipe(switchMap(result => {
        terme.idTerme = result.body.idTerme;
        const tempObjet2: Terme = JSON.parse(JSON.stringify(terme));
        tempObjet2.statut.libelle = this.getNextStatus(terme, 'submit', false);
        tempObjet2.revision = result.body.revision;
        tempObjet2.id = result.body.id;
        tempObjet2.dateCreation = result.body.dateCreation;
        tempObjet2.dateDebutActivite = result.body.dateDebutActivite;
        tempObjet2.dateFinActivite = result.body.dateFinActivite;
        tempObjet2.statut.valideur = this.getNextValideur(terme, terme.statut.valideur, 'valideur');
        tempObjet2.statut.suppleant = this.getNextValideur(terme, terme.statut.suppleant, 'suppleant/valideur');
        return this.termeService.putV1Termes(tempObjet2, 'response');
      }
      ))
    );

    concat(...validations).subscribe(result => {
      this.brouillons = this.brouillons.filter(b => b.id !== terme.id);
      this.showSuccess();
    }, err => {
      this.showFailure();
    });
  }


  getNextValideur(objet, valideur, typeValideur): Utilisateur {
    let next: Utilisateur;
    const selectedFiliere: Filiere = this.filieres.find((element: Filiere) => `${element.idFiliere}` === `${objet.filieres[0].idFiliere}`);
    const superRessources = selectedFiliere.superRessource.filter(element =>
      element.profilAutorisations != null && element.profilAutorisations[0] != null && element.profilAutorisations[0].libelle.toLocaleLowerCase().indexOf(typeValideur) === 0 &&
      element.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique != null
    ).sort((a, b) => (a.profilAutorisations[0].libelle.toLowerCase() > b.profilAutorisations[0].libelle.toLowerCase()) ? 1 :
      ((b.profilAutorisations[0].libelle.toLowerCase() > a.profilAutorisations[0].libelle.toLowerCase()) ? -1 : 0));

    switch (objet.statut.libelle) {
      case Status.BROUILLON:
        next = objet.proprietaire;
        break;
      case Status.SOUMIS:
        next = superRessources[0].profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique.utilisateur;
        break;
      case Status.AValider:
        let pos = 0;
        superRessources.forEach((element, index) => {
          if (element.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique.utilisateur.idUtilisateur
            === valideur.idUtilisateur) {
            pos = index + 1;
          }
        });
        if (pos >= superRessources.length) {
          pos = pos - 1;
        }
        next = superRessources[pos].profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique.utilisateur;
        break;
      default:
        break;
    }
    return next;
  }

  getNextStatus(terme: Terme, operation: string, last: boolean): string {
    let status;

    if (terme.statut == null || terme.statut.libelle == null || terme.statut?.libelle === Status.BROUILLON) {
      return operation === 'save' ? Status.BROUILLON : Status.SOUMIS;
    }

    switch (terme.statut.libelle) {
      case Status.SOUMIS:
        status = Status.AValider;
        break;
      case Status.AValider:
        status = operation === 'submit' ? Status.VALIDE : Status.REFUSER;
        break;
      case Status.VALIDE:
        status = last ? Status.PUBLIE : Status.AValider;
        break;
      case Status.PUBLIE:
        status = Status.BROUILLON;
        break;
      case Status.REFUSER:
        status = Status.BROUILLON;
        break;
      case Status.BROUILLON:
        status = Status.BROUILLON;
        break;
      default:
        break;
    }

    return status;
  }

  showButton(brouillon): boolean {
      return AuthGuard.isAdmin() || AuthGuard.isValideur(brouillon.filieres[0]) || AuthGuard.isRedacteur(brouillon.filieres[0]);
  }
}
