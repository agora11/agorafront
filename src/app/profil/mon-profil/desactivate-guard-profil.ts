import {CanDeactivate} from '@angular/router';
import {DataService} from '../../services/data.service';
import {MonProfilComponent} from './mon-profil.component';

export class DesactivateGuardProfil implements CanDeactivate<MonProfilComponent> {
  isModified = false;
  canDeactivate(component: MonProfilComponent): any {
    DataService.getIsModified().subscribe( isModified => this.isModified = isModified );

    return this.isModified ? component.canDeactivate() : true;
  }
}
