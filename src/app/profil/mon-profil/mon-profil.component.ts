import { Component, OnInit } from '@angular/core';
import {Utilisateur} from '../../models/utilisateur';
import {Filiere} from '../../models/filiere';
import {DataService} from '../../services/data.service';
import {AuthGuard} from '../../services/auth.service';
import {ToastrService} from 'ngx-toastr';
import {UtilisateursService} from '../../services/utilisateurs.service';

@Component({
  selector: 'app-mon-profil',
  templateUrl: './mon-profil.component.html',
  styleUrls: ['./mon-profil.component.css', '../profil.component.css']
})
export class MonProfilComponent implements OnInit {

  acteur: Utilisateur;
  disabled = true;
  isModified = false;
  private required: boolean;
  constructor(private toastr: ToastrService, private utilisateurService: UtilisateursService) { }

  ngOnInit(): void {
    this.setIsModified(false);
    DataService.getIsModified().subscribe(result => this.isModified = result);
    AuthGuard.getConnectedUser().subscribe(acteur => {
      this.acteur = acteur;
    });
  }
  changeDataService(filiere: Filiere): any {
    DataService.changeFiliereData(filiere);
  }

  save() {
          this.utilisateurService.putV1utilisateurs(this.acteur).subscribe(result => {
            this.acteur = result;
            this.showSuccess();
          }, (err) => {
            err.status === 409 ? this.showError('Code cp déjà utilisé') : this.showFailure();
          });
  }

  showSuccess(): any {
    this.toastr.clear();
    this.toastr.success('Opération effectuée avec succès !');
  }

  showFailure(): any {
    this.toastr.clear();
    this.toastr.error('L\'opération a échoué. Veuillez réessayer à nouveau.');
  }

  showError(msg): any {
    this.toastr.clear();
    this.toastr.error(msg);
  }


  setIsModified(value): any {
    DataService.setIsModified(value);
  }

  canDeactivate(): any {
    return confirm('Êtes-vous sûr(e) de vouloir quitter la page ? \n' +
      'En cliquant sur le bouton « OK », vous perdrez ' +
      'toutes les informations apportées à cette fiche.');
  }
}
