import { Component, OnInit } from '@angular/core';
import {FilieresService} from '../services/filieres.service';
import {Utilisateur} from '../models/utilisateur';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  constructor() {

  }

  ngOnInit(): void {}
}
