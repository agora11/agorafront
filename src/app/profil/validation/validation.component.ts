import { Component, OnInit } from '@angular/core';
import {ProfilService} from '../../services/profil.service';
import {UtilisateursService} from '../../services/utilisateurs.service';
import {Status} from '../../models/status';
import {Objet} from '../../models/objet';
import {DataService} from '../../services/data.service';
import {Terme} from '../../models/terme';
import {AuthGuard} from '../../services/auth.service';
import {TermesService} from '../../services/termes.service';
import {ObjetsService} from '../../services/objets.service';

@Component({
  selector: 'app-validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.css', '../profil.component.css']
})
export class ValidationComponent implements OnInit {

  validations = new Array<any>();
  types: any;
  status = Status.list;
  users: any;
  constructor(private  userService: UtilisateursService, private termeService: TermesService, private objetService: ObjetsService) { }

  ngOnInit(): void {
    this.types = ProfilService.getTypes();
    this.userService.getV1Utilisateurs(undefined, undefined, 'prenom').subscribe(result => {
      this.users = result;
      this.users.forEach(user => {user.label = user.nomComplet; user.value = user.nomComplet; });
    });
    AuthGuard.getConnectedUser().subscribe(acteur => {
      if (acteur?.idIdentiteNumerique) {
        this.termeService.getV1Termes(undefined, acteur.idIdentiteNumerique, acteur.idIdentiteNumerique, Status.AValider, undefined, undefined, 'libelle')
          .subscribe(termes => {
            this.objetService.getV1Objets(undefined, acteur.idIdentiteNumerique, acteur.idIdentiteNumerique, Status.AValider, undefined, undefined, undefined, 'libelle')
              .subscribe(objets => {
                termes.forEach(terme => {
                  terme.type = 'Terme métier';
                  terme.filiere = terme.filieres[0]?.nom;
                  terme.definition = terme.definitions[0].definition;
                  this.validations.push(terme);
                });
                objets.forEach(objet => {
                  objet.type = 'Objet métier';
                  objet.filiere = objet.filieres[0]?.nom;
                  objet.definition = objet.definitions[0].definition;
                  this.validations.push(objet);
                });
                this.validations.sort((a, b) => (a.libelle.toLowerCase() > b.libelle.toLowerCase()) ? 1 : ((b.libelle.toLowerCase() > a.libelle.toLowerCase()) ? -1 : 0))
              });
          });
      }
    });

  }

  editObjet(objet: Objet): any {
    DataService.changeObjetData(objet);
  }

  editTerme(terme: Terme): any {
    DataService.changeTermeData(terme);
  }
}
