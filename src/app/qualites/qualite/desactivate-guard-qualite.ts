import {CanDeactivate} from '@angular/router';
import {DataService} from '../../services/data.service';
import {QualiteComponent} from './qualite.component';

export class DesactivateGuardQualite implements CanDeactivate<QualiteComponent> {
  isModified = false;
  canDeactivate(component: QualiteComponent): any {
    DataService.getIsModified().subscribe( isModified => this.isModified = isModified );

    return this.isModified ? component.canDeactivate() : true;
  }
}
