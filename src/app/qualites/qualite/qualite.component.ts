import {Component, OnDestroy, OnInit, TemplateRef} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Objet} from '../../models/objet';
import {Critere, PolitiqueQualite, PolitiqueQualiteParUsages, TauxQualiteMesures, UsageMetier} from '../../models/critere';
import {QualitesService} from '../../services/qualites.service';
import {DataService} from '../../services/data.service';
import {ToastrService} from 'ngx-toastr';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {Attribut} from '../../models/attribut';

@Component({
  selector: 'app-qualite',
  templateUrl: './qualite.component.html',
  styleUrls: ['./qualite.component.css']
})
export class QualiteComponent implements OnInit, OnDestroy {

  objet =  new Objet();
  criteres = [];
  usages = [];
  isDisabled: any;
  isModified = true;
  currentCritereIndex: any;
  modalRef: BsModalRef;
  indicateurs = new Array();
  criteresObjet: any;
  required = false;
  ngbModalOptions = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  constructor(private actRoute: ActivatedRoute, private qualiteService: QualitesService, private toastr: ToastrService,
              private router: Router, private modalService: BsModalService) { }

  ngOnInit(): void {
    this.setIsModified(false);
    DataService.getIsModified().subscribe(result => this.isModified = result);

    this.actRoute.data.subscribe((data: any) => {
      this.criteres = data.qualite.criteresList.map(item => ({ label: item, value: item }));
      this.usages = data.qualite.usagesList.map(item => ({ label: item.libelle, value: item.idUsageMetier }));
    });


    this.actRoute.queryParams.subscribe(params => {
      this.isDisabled = params.action === 'consult' ? true : false;
    });
    this.getCurrentData();
  }

  getCurrentData(): any {
    DataService.getCurrentObjet().subscribe(currentData => {
      if (currentData.idClasseDobjet) {
        this.objet = currentData;

        localStorage.setItem('currentData', JSON.stringify(this.objet));
      } else {
        this.objet = JSON.parse(localStorage.getItem('currentData'));
      }
      if (this.objet?.critereQualites?.length > 0) {
        this.currentCritereIndex = 0;
        this.criteresObjet = this.objet.critereQualites.map(item =>  item.libelle );
        this.objet.criteresToDelete = new Array<Critere>();
      }
      this.objet?.critereQualites?.forEach(critere => {
        critere.politiqueQualites?.forEach(indicateur => {
            indicateur.usagesObjet = new Array<number>();
            indicateur.usagesObjet = indicateur.politiqueQualiteParUsages.map(u =>  u.usageMetier.idUsageMetier);
          }
        );
      });
    });
  }

  save(): any {
    if (this.checkFields()) {
      this.qualiteService.putV1Objet(this.objet).subscribe(result => {
           this.objet = result;
           this.objet.critereQualites.sort((a, b) => (a.libelle.toLowerCase() > b.libelle.toLowerCase()) ? 1 : ((b.libelle.toLowerCase() > a.libelle.toLowerCase()) ? -1 : 0));
           DataService.changeObjetData(this.objet);
           this.setIsModified(false);
           this.showSuccess();

      }, () => {
        this.showFailure('L\'opération a échoué. Veuillez réessayer à nouveau.');
      });
  } else {
  this.showFailure('Merci de remplir tous les champs obligatoires');
}
  }

  removeCritere(index: any): any {
    this.objet.criteresToDelete.push(this.objet.critereQualites[index]);
    this.criteresObjet = this.criteresObjet.filter(critere => critere !== this.objet.critereQualites[index].libelle);
    this.objet.critereQualites.splice(index, 1);
    this.setIsModified(true);
  }

  addIndicateur(): any {
    this.setIsModified(true);
    if (!this.objet.critereQualites[this.currentCritereIndex].politiqueQualites) {
      this.objet.critereQualites[this.currentCritereIndex].politiqueQualites = new Array<PolitiqueQualite>();
    }
    const politique = new PolitiqueQualite();
    politique.idPolitiqueQualite = null;
    politique.tauxQualiteMesures = new Array<TauxQualiteMesures>(new TauxQualiteMesures(), new TauxQualiteMesures(), new TauxQualiteMesures(), new TauxQualiteMesures());
    this.objet.critereQualites[this.currentCritereIndex].politiqueQualites.push(politique);
    this.objet.critereQualites.sort((a, b) => (a.libelle.toLowerCase() > b.libelle.toLowerCase()) ? 1 : ((b.libelle.toLowerCase() > a.libelle.toLowerCase()) ? -1 : 0));

  }

  changeCritere(index: number): any {
    this.currentCritereIndex = index;
  }

  changeCriteresListe(): any {
    if (!this.objet.critereQualites) {
      this.objet.critereQualites = new Array<Critere>();
    }
    this.criteresObjet.forEach(critere => {
     if (!this.objet.critereQualites.find(critereObjet => critereObjet.libelle === critere)) {
       this.objet.critereQualites.push(new Critere(critere));
       if (!this.currentCritereIndex) {
         this.currentCritereIndex = 0;
       }
   }});

    this.objet.critereQualites.forEach(c => {
     if (!this.criteresObjet.find(critereObjet => critereObjet === c.libelle)) {
       this.objet.criteresToDelete.push(this.objet.critereQualites[this.objet.critereQualites.indexOf(c)]);
       this.objet.critereQualites.splice( this.objet.critereQualites.indexOf(c), 1);
     }
   });
    this.objet.critereQualites.sort((a, b) => (a.libelle.toLowerCase() > b.libelle.toLowerCase()) ? 1 : ((b.libelle.toLowerCase() > a.libelle.toLowerCase()) ? -1 : 0));

  }

  changeUsage(indicateur) {
   if (!indicateur.politiqueQualiteParUsages) {
    indicateur.politiqueQualiteParUsages = new Array<PolitiqueQualiteParUsages>();
   }
   indicateur.usagesObjet.forEach(usage => {
  if (!indicateur.politiqueQualiteParUsages.find(usageObjet => usageObjet.usageMetier.idUsageMetier === usage)) {
    const politiqueParMesure = new PolitiqueQualiteParUsages();
    const usageM = this.usages.find(u => u.value === usage);
    politiqueParMesure.usageMetier = new UsageMetier(usageM.label, usageM.value);
    indicateur.politiqueQualiteParUsages.push(politiqueParMesure);
  }});

   indicateur.politiqueQualiteParUsages.forEach(c => {
  if (!indicateur.usagesObjet.find(usage => usage === c.usageMetier.idUsageMetier)) {
    indicateur.politiqueQualiteParUsages.splice( indicateur.politiqueQualiteParUsages.indexOf(c), 1);
  }
});
  }

  removeUsage(indexUsage: number, indicateur: PolitiqueQualite): any {
    indicateur.usagesObjet = indicateur.usagesObjet.filter(usage => usage !== indicateur.politiqueQualiteParUsages[indexUsage].usageMetier.idUsageMetier);
    indicateur.politiqueQualiteParUsages.splice(indexUsage, 1);
    this.setIsModified(true);
  }


  showSuccess(): any {
    this.toastr.clear();
    this.toastr.success('Opération effectuée avec succès !');
  }

  showFailure(msg): any {
    this.toastr.clear();
    this.toastr.error(msg);
  }

  canDeactivate(): any {
    return confirm('Êtes-vous sûr(e) de vouloir quitter la page ? \n' +
      'En cliquant sur le bouton « OK », vous perdrez ' +
      'toutes les informations apportées à cette fiche.');
  }
  setIsModified(value: boolean): any {
    DataService.setIsModified(value);
  }

  redirect(): any {
    this.setIsModified(false);
    this.hideModal();
    this.router.navigateByUrl('/main/qualites');
  }

  hideModal(): any {
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }

  openModal(template: TemplateRef<any>): any {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'modal-dialog-centered' }, this.ngbModalOptions)
    );
  }

  ngOnDestroy(): any {
    this.hideModal();
    this.toastr.clear();
    DataService.changeObjetData(new Objet());
  }


  isInvalid(value): boolean {
    return this.required && (value === undefined || value === '');
  }
  checkFields(): boolean {
    const qualite = new Array();
    this.objet.critereQualites?.forEach(critere => {
        if (critere.politiqueQualites?.length > 0) {
          critere.politiqueQualites.forEach(politique => qualite.push(politique));
        }
      });

    const invalidQualiteLabel = qualite?.filter(
                                 politique => politique.indicateurDeQualite === ''
                                   || politique.indicateurDeQualite === undefined);
    if (invalidQualiteLabel.length > 0) {
      this.required = true;
      return false;
    } else {
      this.required = false;
      return true;
    }
  }
}
