import { Component, OnInit, TemplateRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Objet } from '../models/objet';
import { QualitesService } from '../services/qualites.service';
import {DataService} from '../services/data.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-qualite',
  templateUrl: './qualites.component.html',
  styleUrls: ['./qualites.component.css']
})

export class QualitesComponent implements OnInit {

  objets = new Array<any>(new Objet());
  objet: any;
  modalRef: BsModalRef;

  constructor(private modalService: BsModalService, private qualitesService: QualitesService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.qualitesService.getV1Objets(undefined, undefined, undefined, 'libelle').subscribe(result => {
      this.objets = result;
      this.objets.forEach(objet => {
        objet.critereQualites.sort((a, b) => (a.libelle.toLowerCase() > b.libelle.toLowerCase()) ? 1 : ((b.libelle.toLowerCase() > a.libelle.toLowerCase()) ? -1 : 0));
        objet.critere = '';
        objet.critere += objet.critereQualites.map(item => '  ' + item.libelle );
      });
    });
  }

  editObjet(objet: any): any {
      DataService.changeObjetData(objet);
  }

  openModal(objet, template: TemplateRef<any>): any {
    this.objet = objet;
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'modal-dialog-centered' })
    );
  }

  archiveQualite(objet: any): any {
         this.qualitesService.deleteV1ObjetsId(objet.idClasseDobjet).subscribe(result => {
           objet.critereQualites = new Array();
           this.showSuccess();
           this.modalRef.hide();
         }, () => {
           this.showFailure();
         });
  }

  showSuccess(): any {
    this.toastr.success('Opération effectuée avec succès !');
  }

  showFailure(): any {
    this.toastr.error('L\'opération a échoué. Veuillez réessayer à nouveau.');
  }
}
