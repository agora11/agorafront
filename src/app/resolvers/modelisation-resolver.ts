import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { forkJoin } from 'rxjs';
import {FilieresService, ObjetsService} from '../services/api';

@Injectable({
  providedIn: 'root'
})
export class ModelisationResolver implements Resolve<any> {

  constructor(private filiereService: FilieresService, private objetService: ObjetsService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return forkJoin({
      filieres: this.filiereService.getV1Filieres(undefined, undefined, undefined, 'nom'),
      objetList: this.objetService.getV1Objets(undefined, undefined, undefined, undefined, undefined, undefined, undefined, 'libelle')
    });
  }
}
