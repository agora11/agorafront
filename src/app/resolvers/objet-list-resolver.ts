import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import {ObjetsService} from '../services/objets.service';

@Injectable({
  providedIn: 'root'
})
export class ObjetListResolver implements Resolve<any> {

    constructor(
        private objetService: ObjetsService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.objetService.getV1Objets(undefined, undefined, undefined, undefined, undefined, undefined, undefined, 'libelle');
    }
}
