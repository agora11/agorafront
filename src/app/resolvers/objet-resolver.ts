import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { forkJoin } from 'rxjs';
import { FilieresService, UtilisateursService} from '../services/api';

@Injectable({
    providedIn: 'root'
})
export class ObjetResolver implements Resolve<any> {

    constructor(private utilisateursService: UtilisateursService, private filiereService: FilieresService) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return forkJoin({
            usersList: this.utilisateursService.getV1Utilisateurs(),
            filieres: this.filiereService.getV1Filieres()
        });
    }
}
