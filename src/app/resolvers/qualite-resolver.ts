import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { forkJoin } from "rxjs";
import { QualitesService } from "../services/qualites.service";

@Injectable({
    providedIn: 'root'
})
export class QualiteResolver implements Resolve<any> {

    constructor(private qualiteService: QualitesService) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return forkJoin({
            criteresList: this.qualiteService.getV1Critere(),
            usagesList: this.qualiteService.getV1Usage()
        });
    }
}
