import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { TermesService } from "../services/termes.service";

@Injectable({
    providedIn: 'root'
})
export class TermeListResolver implements Resolve<any> {

    constructor(
        private termeService: TermesService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.termeService.getV1Termes(undefined, undefined, undefined, undefined, undefined, undefined, 'libelle');
    }
}
