import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { forkJoin } from 'rxjs';
import {ObjetsService} from '../services/objets.service';
import {FilieresService} from '../services/filieres.service';

@Injectable({
  providedIn: 'root'
})
export class TermeResolver implements Resolve<any> {

  constructor(private filiereService: FilieresService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    return forkJoin({
      filieresList: this.filiereService.getV1Filieres()
    });
  }
}
