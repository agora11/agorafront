import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DataService} from '../services/data.service';
import {SearchService} from '../services/search.service';
import {AuthGuard} from '../services/auth.service';
import {Objet} from '../models/objet';
import {ObjetsService} from '../services/objets.service';
import {TermesService} from '../services/termes.service';
import {GisementsService} from '../services/gisements.service';
import {QualitesService} from '../services/qualites.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  isAdmin = false;
  item: string ;
  listTerme: any[];
  listObject: any[];
  listDiagramme: any[];
  listFiliere: any[];
  listActeur: any[];
  listQualite: any[];
  listGisement: any[];
  items: number;
  globalSearchList =  new Array();

  // tslint:disable-next-line:max-line-length
  constructor(private route: ActivatedRoute, private searchService: SearchService, private objetService: ObjetsService, private termeService: TermesService, private gisementService: GisementsService, private qualiteService: QualitesService) {
  }

  ngOnInit(): void {
DataService.getSearch().subscribe(result => {
  if (result.searchValue !== '' && result.searchValue !== undefined) {
    this.item = result.searchValue;
  }else {
    this.item =  localStorage.getItem('item');
  }
  this.globalSearchList = [];
  this.searchService.getGlobalSearch(this.item).subscribe(response => {
      this.listTerme = response.glossaire;
      this.listObject = response.objets;
      this.listDiagramme = response.diagrammes;
      this.listFiliere = response.filiere;
      this.listActeur = response.acteurs;
      this.listQualite = response.qualite;
      this.listGisement = response.gisement;
    // tslint:disable-next-line:max-line-length
      this.globalSearchList.push(...this.listTerme, ...this.listObject, ...this.listDiagramme, ...this.listFiliere, ...this.listActeur, ...this.listQualite, ...this.listGisement);
    // tslint:disable-next-line:max-line-length
      this.items = this.listTerme.length + this.listObject.length + this.listDiagramme.length + this.listFiliere.length + this.listActeur.length + this.listQualite.length + this.listGisement.length;
    });
});
AuthGuard.getConnectedUser().subscribe(currentUser => {
      this.isAdmin = currentUser.isAdmin;
    });
  }

  editObjet(id: number): any {
    this.objetService.getV1ObjetsId(id).subscribe(result =>
      DataService.changeObjetData(result)
    );
  }
  editQualite(qualite): any {
    this.qualiteService.getV1Objets().subscribe(result =>
    DataService.changeObjetData(result.filter(q => q.libelle === qualite.libelle)[0])
    );
  }
  editTerme(id: number): any {
    this.termeService.getV1TermesId(id).subscribe(result =>
      DataService.changeTermeData(result)
    );
  }

  editActeur(acteur): any{
    DataService.changeActeurData(acteur);
  }

  changeData(gisement: any): any {
    DataService.changeCurrentGisement(gisement);
  }

  changeDataService(filiere): any {
    DataService.changeFiliereData(filiere);
  }

}
