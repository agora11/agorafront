import { Injectable } from '@angular/core';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { AuthenticationService } from '@tec/condor/services';
import {Utilisateur} from '../models/utilisateur';
import {BehaviorSubject, Observable} from 'rxjs';
import {Role} from '../models/role';
import {Filiere} from '../models/filiere';

@Injectable()
export class AuthGuard implements CanActivate {
  public static connectedUser = new BehaviorSubject(new Utilisateur());
  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): boolean {
    return this.checkLogin();
  }

  checkLogin(): boolean {
    if (this.authenticationService.isAuthenticated() ||
      localStorage.getItem('utilisateur')) { return true; }
    localStorage.clear();
    this.router.navigate(['/login-caiman']);
    return false;
  }

  public static getConnectedUser(): Observable<Utilisateur> {
    return this.connectedUser.asObservable();
  }

  public static setConnectedUser(utilisateur: Utilisateur): any {
    localStorage.setItem('utilisateur', JSON.stringify(utilisateur));
    utilisateur.isAdmin = this.isAdmin();
    utilisateur.isValideur = this.isValideur(null);
    utilisateur.isRedacteur = this.isRedacteur(null);
    utilisateur.isResponsable = this.isResponsable();
    localStorage.setItem('utilisateur', JSON.stringify(utilisateur));
    this.connectedUser.next(utilisateur);
  }

  public static isAdmin(): boolean {
    return (JSON.parse(localStorage.getItem('utilisateur')) as Utilisateur)?.autorisations?.filter(aut => aut.profilAutorisation?.libelle === Role.ADMIN)?.length > 0;
  }

  public static isValideur(filiere: Filiere): boolean {
    return (JSON.parse(localStorage.getItem('utilisateur')) as Utilisateur)?.autorisations?.filter(aut  =>
      (aut.profilAutorisation?.libelle === Role.VALIDEUR || aut.profilAutorisation?.libelle.includes(Role.SUPPLEANT))
          && (filiere === null || aut.filiere?.idFiliere === filiere?.idFiliere))?.length > 0;
  }
  public static isRedacteur(filiere: Filiere): boolean {
    return (JSON.parse(localStorage.getItem('utilisateur')) as Utilisateur)?.autorisations?.filter(aut => aut.profilAutorisation?.libelle === Role.REDACTEUR
      && (filiere === null || aut.filiere?.idFiliere === filiere?.idFiliere))?.length > 0;
  }

  public static isResponsable(): boolean {
    return (JSON.parse(localStorage.getItem('utilisateur')) as Utilisateur)?.filieresResponsable?.length > 0
    || (JSON.parse(localStorage.getItem('utilisateur')) as Utilisateur)?.filieresResponsableDelegue?.length > 0
    || (JSON.parse(localStorage.getItem('utilisateur')) as Utilisateur)?.filieresCoordinateur?.length > 0
  }

}
