import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Terme } from '../models/terme';
import { Objet } from '../models/objet';
import {Filiere} from '../models/filiere';
import {Gisement} from '../models/gisement';
import {Utilisateur} from "../models/utilisateur";

@Injectable()
export class DataService {

  private static dataSearch = new BehaviorSubject({ filiere: [], roles: [], searchValue: '', component: '' });
  private static  menuItem = new BehaviorSubject('termes');
  private  static modified =  new BehaviorSubject(false);
  private static dataSourceTerme = new BehaviorSubject(new Terme());
  private static dataSourceObjet = new BehaviorSubject(new Objet());
  private static dataSourceFiliere = new BehaviorSubject(new Filiere());
  private static dataSourceActeur = new BehaviorSubject(new Utilisateur());
  private static dataSourceGisement = new BehaviorSubject(new Gisement());


  public static getSearch(): Observable<{ filiere: Array<string>, roles: Array<string>, searchValue: string, component: string }> {
    return this.dataSearch.asObservable();
  }

  public static pushSearch(value: { filiere: Array<string>, roles: Array<string>, searchValue: string, component: string }): any {
    this.dataSearch.next(value);
  }

  public static getIsModified(): Observable<boolean> {
    return this.modified.asObservable();
  }

  public static setIsModified(value: boolean): any {
    this.modified.next(value);
  }

  public static getCurrentTerme(): Observable<Terme> {
    return  this.dataSourceTerme.asObservable();
  }

  public static changeTermeData(data: any): any {
    this.dataSourceTerme.next(data);
  }

  public static getCurrentObjet(): Observable<Objet> {
    return this.dataSourceObjet.asObservable();
  }
  public static changeObjetData(data: any): any {
    this.dataSourceObjet.next(data);
  }

  public static getCurrentFiliere(): Observable<Filiere> {
    return this.dataSourceFiliere.asObservable();
  }
  public static changeFiliereData(data: any): any {
    this.dataSourceFiliere.next(data);
  }
  public static changeActeurData(data: any): any {
    this.dataSourceActeur.next(data);
  }
  public static getActeurItem(): Observable<Utilisateur> {
    return this.dataSourceActeur.asObservable();
  }
  public static getMenuItem(): Observable<string> {
    return this.menuItem.asObservable();
  }

  public static changeMenuItem(data: any): any {
    this.menuItem.next(data);
  }

  public static getCurrentGisement(): Observable<Gisement> {
    return this.dataSourceGisement.asObservable();
  }

  public static changeCurrentGisement(data: any): any {
    this.dataSourceGisement.next(data);
  }
}
