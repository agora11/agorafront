import { Inject, Injectable, Optional } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
  HttpResponse, HttpEvent, HttpParameterCodec } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Domaine } from '../models/domaine';
import {Configuration} from '../constants/configuration';
import {BASE_PATH} from '../constants/variables';
import {CustomHttpParameterCodec} from '../constants/encoder';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DomaineService {


  protected basePath = `${environment.basePath}/back-agora`;
  public defaultHeaders = new HttpHeaders();
  public configuration = new Configuration();
  public encoder: HttpParameterCodec;

  constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
    if (configuration) {
      this.configuration = configuration;
    }
    if (typeof this.configuration.basePath !== 'string') {
      if (typeof basePath !== 'string') {
        basePath = this.basePath;
      }
      this.configuration.basePath = basePath;
    }
    this.encoder = this.configuration.encoder || new CustomHttpParameterCodec();
  }

  /**
   * Liste des domaines
   * Liste des domaines
   * @param page numéro de la page courante
   * @param size nombre d’éléments par page
   * @param sort Le tri est défini comme la détermination de l’ordre dans lequel nous souhaitons récupérer les ressources
   * @param filter filtrage avancé
   * @param fields Le consommateur du service spécifie dans un paramètre de requête fields les champs de la ressource qu’il souhaite récupérer exclusivement au sein de la réponse. Ces paramètres seront séparés par une virgule
   * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
   * @param reportProgress flag to report request and response progress.
   */
  public getV1Domaines(page?: number, size?: number, sort?: string, filter?: string, fields?: string, observe?: 'body', reportProgress?: boolean, options?: {httpHeaderAccept?: 'application/json'}): Observable<Array<Domaine>>;
  public getV1Domaines(page?: number, size?: number, sort?: string, filter?: string, fields?: string, observe?: 'response', reportProgress?: boolean, options?: {httpHeaderAccept?: 'application/json'}): Observable<HttpResponse<Array<Domaine>>>;
  public getV1Domaines(page?: number, size?: number, sort?: string, filter?: string, fields?: string, observe?: 'events', reportProgress?: boolean, options?: {httpHeaderAccept?: 'application/json'}): Observable<HttpEvent<Array<Domaine>>>;
  public getV1Domaines(page?: number, size?: number, sort?: string, filter?: string, fields?: string, observe: any = 'body', reportProgress: boolean = false, options?: {httpHeaderAccept?: 'application/json'}): Observable<any> {

    let queryParameters = new HttpParams({encoder: this.encoder});
    if (page !== undefined && page !== null) {
      queryParameters = this.addToHttpParams(queryParameters, <any>page, 'page');
    }
    if (size !== undefined && size !== null) {
      queryParameters = this.addToHttpParams(queryParameters, <any>size, 'size');
    }
    if (sort !== undefined && sort !== null) {
      queryParameters = this.addToHttpParams(queryParameters, <any>sort, 'sort');
    }
    if (filter !== undefined && filter !== null) {
      queryParameters = this.addToHttpParams(queryParameters, <any>filter, 'filter');
    }
    if (fields !== undefined && fields !== null) {
      queryParameters = this.addToHttpParams(queryParameters, <any>fields, 'fields');
    }

    let headers = this.defaultHeaders;

    let httpHeaderAcceptSelected: string | undefined = options && options.httpHeaderAccept;
    if (httpHeaderAcceptSelected === undefined) {
      // to determine the Accept header
      const httpHeaderAccepts: string[] = [
        'application/json'
      ];
      httpHeaderAcceptSelected = this.configuration.selectHeaderAccept(httpHeaderAccepts);
    }
    if (httpHeaderAcceptSelected !== undefined) {
      headers = headers.set('Accept', httpHeaderAcceptSelected);
    }


    let responseType: 'text' | 'json' = 'json';
    if(httpHeaderAcceptSelected && httpHeaderAcceptSelected.startsWith('text')) {
      responseType = 'text';
    }

    return this.httpClient.get<Array<Domaine>>(`${this.configuration.basePath}/v1/domaines`,
      {
        params: queryParameters,
        responseType: <any>responseType,
        withCredentials: this.configuration.withCredentials,
        headers: headers,
        observe: observe,
        reportProgress: reportProgress
      }
    );
  }


  private addToHttpParams(httpParams: HttpParams, value: any, key?: string): HttpParams {
    if (typeof value === "object" && value instanceof Date === false) {
      httpParams = this.addToHttpParamsRecursive(httpParams, value);
    } else {
      httpParams = this.addToHttpParamsRecursive(httpParams, value, key);
    }
    return httpParams;
  }

  private addToHttpParamsRecursive(httpParams: HttpParams, value?: any, key?: string): HttpParams {
    if (value == null) {
      return httpParams;
    }

    if (typeof value === "object") {
      if (Array.isArray(value)) {
        (value as any[]).forEach( elem => httpParams = this.addToHttpParamsRecursive(httpParams, elem, key));
      } else if (value instanceof Date) {
        if (key != null) {
          httpParams = httpParams.append(key,
            (value as Date).toISOString().substr(0, 10));
        } else {
          throw Error("key may not be null if value is Date");
        }
      } else {
        Object.keys(value).forEach( k => httpParams = this.addToHttpParamsRecursive(
          httpParams, value[k], key != null ? `${key}.${k}` : k));
      }
    } else if (key != null) {
      httpParams = httpParams.append(key, value);
    } else {
      throw Error("key may not be null if value is not object or array");
    }
    return httpParams;
  }

}
