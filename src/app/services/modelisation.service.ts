import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Diagramme} from '../models/diagramme';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})


export class ModelisationService {

  private basePath = `${environment.basePath}/back-agora/`;
  public defaultHeaders = new HttpHeaders();

  constructor(private httpClient: HttpClient) {
  }

  public saveDiagram(diagram: any): Observable<any> {
    return this.httpClient.post<Diagramme>(this.basePath + 'save-diagram', diagram, {
      headers : new HttpHeaders({
        'Content-Type' : 'application/json'
      })});
  }

  public updateDiagram(diagram: any): Observable<any> {
    return this.httpClient.put<any>(this.basePath + 'update-diagram', diagram, {
      headers : new HttpHeaders({
        'Content-Type' : 'application/json'
      })});
  }

  public getAllDiagrams(): Observable<any> {
    return this.httpClient.get<any>(this.basePath + 'get-all-diagrams',
      {
        headers : new HttpHeaders({
          'Content-Type' : 'application/json'
        })}
    );
  }

  public getDiagram(id: number): Observable<any> {
    return this.httpClient.get<any>(this.basePath + 'get-diagram/' + id,
      {
        headers : new HttpHeaders({
          'Content-Type' : 'application/json'
        })}
    );
  }

  public updateDiagramMetaData(diagram: any): Observable<any> {
    return this.httpClient.patch<any>(this.basePath + '/update-diagram-meta-data', diagram, {
      headers : new HttpHeaders({
        'Content-Type' : 'application/json'
      })});
  }

  public archiveDiagram(diagram: any): Observable<any> {
    return this.httpClient.patch<any>(this.basePath + 'archive-diagram', diagram,
      {
        headers : new HttpHeaders({
          'Content-Type' : 'application/json'
        })}
    );
  }
}
