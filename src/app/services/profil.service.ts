import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProfilService {

  public static getTypes(): Array<any> {
    return new Array<any>({label: 'Terme métier', value: 'Terme métier'},
      {label: 'Objet métier', value: 'Objet métier'});
  }
}
