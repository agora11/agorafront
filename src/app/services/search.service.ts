import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders, HttpParameterCodec} from '@angular/common/http';
import {Configuration} from '../constants/configuration';
import {Observable} from 'rxjs';

@Injectable({ providedIn: 'root' })
export class SearchService {
  protected basePath = `${environment.basePath}/back-agora`;
  public defaultHeaders = new HttpHeaders();
  public configuration = new Configuration();
  public encoder: HttpParameterCodec;

  constructor(private httpClient: HttpClient) {
  }

  public getGlobalSearch(value: string): Observable<any> {
    return this.httpClient.get<any>(this.basePath + '/v1/search?keyWord=' + value , {
      headers : new HttpHeaders({
        'Content-Type' : 'application/json'
      })});
  }

}
