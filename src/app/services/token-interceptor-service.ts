import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { SpinnerOverlayService } from './spinner-overlay.service';
import { finalize, tap } from 'rxjs/operators';
import { Subscription } from 'rxjs/internal/Subscription';
import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {

  constructor(private readonly spinnerOverlayService: SpinnerOverlayService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const spinnerSubscription: Subscription = this.spinnerOverlayService.spinner$.subscribe();
    return next.handle(req)
      .pipe(
        tap(() => { }, (err: any) => {
          if (err instanceof HttpErrorResponse) {
            if (err.status !== 401) {
              return;
            }
          }
        }),
        finalize(() => spinnerSubscription.unsubscribe())
      );
  }
}
