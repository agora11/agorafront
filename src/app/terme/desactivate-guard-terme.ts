import { CanDeactivate } from '@angular/router';
import {TermeComponent} from './terme.component';
import {DataService} from '../services/data.service';

export class DeactivateGuardTerme implements CanDeactivate<TermeComponent> {
  isModified = false;
  canDeactivate(component: TermeComponent): any {
    DataService.getIsModified().subscribe( isModified => this.isModified = isModified );

    return this.isModified ? component.canDeactivate() : true;
  }
}
