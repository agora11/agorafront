import { Component, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { Terme } from '../models/terme';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Filiere } from '../models/filiere';
import { DataService } from '../services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TermesService } from '../services/termes.service';
import { Document } from '../models/document';
import { ToastrService } from 'ngx-toastr';
import { Definition } from '../models/definition';
import { Objet } from '../models/objet';
import { ObjetsService } from '../services/objets.service';
import { Statut } from '../models/statut';
import { Status } from '../models/status';
import { Utilisateur } from '../models/utilisateur';
import { HttpResponse } from '@angular/common/http';
import { catchError, switchMap } from 'rxjs/operators';
import { concat, Observable, of, throwError } from 'rxjs';
import { AuthGuard } from '../services/auth.service';

@Component({
  selector: 'app-terme',
  templateUrl: './terme.component.html',
  styleUrls: ['./terme.component.css']
})
export class TermeComponent implements OnInit, OnDestroy {
  modalRef: BsModalRef;
  terme: Terme = new Terme();
  isDisabled: boolean;
  filieres = [];
  objets: Objet[];
  required = false;
  libelleDocError = false;
  libelleUrlError = false;
  component = 'terme';
  statusAccessor = Status;
  filiereSelectConfig = {
    labelField: 'nom',
    valueField: 'idFiliere',
    searchField: ['nom'],
    dropdownDirection: 'down',
    sortField: [{ field: 'nom', direction: 'asc' }],
  };

  titre = 'Création d’un terme métier';
  titre2 = '';
  doc = new Document();
  ngbModalOptions = {
    backdrop: true,
    ignoreBackdropClick: true
  };
  objetSubscription: any;
  filtredObjetAssoc: Objet[];
  filtredObjets: Objet[];
  isModified = false;
  status = new Array<Statut>();
  status1: any;
  commentaire = '';
  private allStatus = new Array<Statut>();
  constructor(private modalService: BsModalService, private dataService: DataService,
    private termeService: TermesService,
    private toastr: ToastrService,
    private objetService: ObjetsService,
    private actRoute: ActivatedRoute, private route: Router) {
  }

  ngOnInit(): void {
    this.setIsModified(false);
    DataService.getIsModified().subscribe(result => this.isModified = result);
    this.actRoute.data.subscribe((data: any) => {
      this.objets = data.objetsList.filter(objet => objet.statut.libelle === this.statusAccessor.PUBLIE);
      this.filtredObjets = this.objets;
      data?.terme?.filieresList?.forEach(filiere => {
        if ((AuthGuard.isAdmin() || (this.getCurrentUser().autorisations?.length > 0 && this.getCurrentUser().autorisations.filter(aut => filiere?.idFiliere === aut.filiere?.idFiliere
        ).length > 0)) && filiere.superRessource.filter(superRess => superRess.profilAutorisations[0]?.libelle.includes('Valideur'))?.length > 0) {
          this.filieres.push(filiere);
        }
      });
    });

    this.actRoute.queryParams.subscribe(params => {
      if (params.action === 'consult') {
        this.isDisabled = true;
        this.getCurrentData();
        this.initArrays();
        this.titre = 'Terme métier ';
        this.titre2 = ' - ' + this.terme.libelle;
      } else if (params.action === 'edit') {
        this.isDisabled = false;
        this.getCurrentData();
        this.initArrays();
        this.titre = 'Terme métier ';
        this.titre2 = ' - ' + this.terme.libelle;
      } else {
        this.isDisabled = false;
        this.titre = 'Création d’un terme métier';
        this.titre2 = '';
        this.initTerme();
      }
    });

    this.objetSubscription = DataService.getSearch().subscribe(data => {
      this.component = data.component;
      let result = this.component === 'terme' ? this.terme.objets : this.objets;
      if (data.filiere.length > 0) {
        result = this.objets.filter(objet => data.filiere.includes(objet.filieres[0]?.nom));
      }
      if (data.searchValue !== '') {
        result = result.filter(objet => objet.libelle.toLowerCase().indexOf(data.searchValue.toLowerCase()) > -1
          ||
          objet.definitions[0]?.definition?.toLowerCase().indexOf(data.searchValue.toLowerCase()) > -1);
      }
      if (this.component === 'terme') {
        this.filtredObjetAssoc = result;
      } else {
        this.filtredObjets = result;
      }
    });
  }

  getCurrentData(): any {
    DataService.getCurrentTerme().subscribe(currentData => {
      if (currentData.idTerme) {
        this.terme = currentData;
        this.initArrays();
        localStorage.setItem('currentData', JSON.stringify(this.terme));
      } else {
        this.terme = JSON.parse(localStorage.getItem('currentData'));
      }
      this.getStatusHistory();
      this.filtredObjetAssoc = this.terme.objets;
    });
  }

  initTerme(): any {
    this.terme = {
      libelle: '',
      description: '',
      commentaire: '',
      abreviation: '',
      filieres: new Array<Filiere>(new Filiere()),
      documents: new Array<Document>(),
      definitions: new Array<Definition>(new Definition()),
      objets: new Array<Objet>(),
      statut: new Statut()
    };
    this.filtredObjetAssoc = new Array<Objet>();
    this.doc = new Document();
    this.status1 = new Statut();
    this.status = new Array();
    this.allStatus = new Array();
    this.setIsModified(false);
  }

  initArrays(): any {
    if (!this.terme.definitions || this.terme.definitions.length === 0) {
      this.terme.definitions = new Array<Definition>(new Definition());
    }
    if (!this.terme.filieres || this.terme.filieres.length === 0) {
      this.terme.filieres = new Array<Filiere>(new Filiere());
    }
    if (!this.terme.documents) {
      this.terme.documents = new Array<Document>();
    }
  }

  save(statut: string, submit: string): any {
    if (this.checkFields()) {
      if (this.doc.titre !== undefined && this.doc.titre !== ''
        && this.doc.url !== undefined && this.doc.url !== '') {
        this.terme.documents.push(this.doc);
      }

      const savedTerme: Terme = JSON.parse(JSON.stringify(this.terme));
      this.doc = new Document();
      this.terme.statut = new Statut();
      this.terme.statut.commentaire = this.commentaire;
      this.terme.proprietaire = this.terme.proprietaire != null ? this.terme.proprietaire : this.getCurrentUser();


      if ((submit === 'submit' || submit === 'deny') && savedTerme.statut.libelle != null) {
        this.terme.statut.valideur = this.getNextValideur(savedTerme, savedTerme.statut?.valideur, 'valideur');
        this.terme.statut.suppleant = this.getNextValideur(savedTerme, savedTerme.statut?.suppleant, 'suppleant/valideur');
      } else {
        this.terme.statut.valideur = this.getCurrentUser();
        this.terme.statut.suppleant = this.getCurrentUser();
      }

      this.terme.statut.libelle = this.getNextStatus(savedTerme, submit, false);

      if (this.terme.idTerme === undefined || this.terme.idTerme == null) {
        this.termeService.postV1Termes(this.terme, 'response').pipe(switchMap((result: HttpResponse<Terme>) => {
          if (this.terme.statut.libelle === this.statusAccessor.BROUILLON) {
            return of(result);
          }
          if (result.headers.get('warning')) {
            this.showWarning(result.headers.get('warning'));
          }
          this.terme.idTerme = result.body.idTerme;
          const tempTerme: Terme = JSON.parse(JSON.stringify(this.terme));
          tempTerme.statut.libelle = this.getNextStatus(this.terme, 'submit', false);
          tempTerme.revision = result.body.revision;
          tempTerme.id = result.body.statut.id;
          tempTerme.dateCreation = result.body.dateCreation;
          tempTerme.dateDebutActivite = result.body.dateDebutActivite;
          tempTerme.dateFinActivite = result.body.dateFinActivite;
          tempTerme.statut.valideur = this.getNextValideur(this.terme, this.terme.statut.valideur, 'valideur');
          tempTerme.statut.suppleant = this.getNextValideur(this.terme, this.terme.statut.suppleant, 'suppleant/valideur');
          return this.termeService.putV1Termes(tempTerme, 'response');
        }),
          catchError((err: HttpResponse<any>) => throwError(err)))
          .subscribe(result => {
            this.initTermeAfterSubmit(result.body);
            if (result.headers.get('warning')) {
              this.showWarning(result.headers.get('warning'));
            }
            this.refreshPage(result.body);
          },
            msg => { // Error
              this.terme = savedTerme;
              if (msg.status === 409) {
                this.hideModal();
                this.showFailure('Le libellé est déjà utilisé sur la même filière');
              } else {
                this.hideModal();
                this.showFailure('L\'opération a échoué. Veuillez réessayer à nouveau.');
              }
            });
      } else {
        const validations: Array<Observable<HttpResponse<Terme>>> = [];

        if (savedTerme.statut == null || savedTerme.statut.libelle == null) {
          validations.push(this.termeService.putV1Termes(this.terme, 'response'));
        } else if (savedTerme.statut.libelle === this.statusAccessor.BROUILLON && submit === 'save') {
          validations.push(this.termeService.putV1Termes(this.terme, 'response'));
        } else if (savedTerme.statut.libelle === this.statusAccessor.BROUILLON) {
          validations.push(this.termeService.putV1Termes(this.terme, 'response').pipe(switchMap(result => {
            this.terme.idTerme = result.body.idTerme;
            const tempTerme2: Terme = JSON.parse(JSON.stringify(this.terme));
            tempTerme2.statut.libelle = this.getNextStatus(this.terme, 'submit', false);
            tempTerme2.revision = result.body.revision;
            tempTerme2.id = result.body.id;
            tempTerme2.dateCreation = result.body.dateCreation;
            tempTerme2.dateDebutActivite = result.body.dateDebutActivite;
            tempTerme2.dateFinActivite = result.body.dateFinActivite;
            tempTerme2.statut.valideur = this.getNextValideur(this.terme, this.terme.statut.valideur, 'valideur');
            tempTerme2.statut.suppleant = this.getNextValideur(this.terme, this.terme.statut.suppleant, 'suppleant/valideur');
            return this.termeService.putV1Termes(tempTerme2, 'response');
          }
          ))
          );
        } else if (savedTerme.statut.libelle === this.statusAccessor.AValider) {
          const tempTerme1: Terme = JSON.parse(JSON.stringify(this.terme));
          const last = this.lastValideur(savedTerme);
          let op = this.terme.statut.libelle == this.statusAccessor.REFUSER;
          tempTerme1.statut.valideur = last && !op ? this.terme.statut.valideur : savedTerme.statut.valideur;
          tempTerme1.statut.suppleant = last && !op ? this.terme.statut.suppleant : savedTerme.statut.suppleant;

          let connectedUser: Utilisateur = this.getCurrentUser();
          if (connectedUser.nom === tempTerme1.statut.valideur.nom && connectedUser.prenom === tempTerme1.statut.valideur.prenom) {
            tempTerme1.statut.nom = tempTerme1.statut.valideur.nom;
            tempTerme1.statut.prenom = tempTerme1.statut.valideur.prenom;
          } else if (connectedUser.nom === tempTerme1.statut.suppleant.nom && connectedUser.prenom === tempTerme1.statut.suppleant.prenom) {
            tempTerme1.statut.nom = tempTerme1.statut.suppleant.nom;
            tempTerme1.statut.prenom = tempTerme1.statut.suppleant.prenom;
          } else if (AuthGuard.isAdmin()) {
            tempTerme1.statut.nom = connectedUser.nom;
            tempTerme1.statut.prenom = connectedUser.prenom;
          }

          validations.push(this.termeService.putV1Termes(tempTerme1, 'response').pipe(switchMap((result: HttpResponse<Terme>) => {
            const tempTerme: Terme = JSON.parse(JSON.stringify(savedTerme));
            tempTerme.idTerme = result.body.idTerme;
            tempTerme.statut.libelle = this.getNextStatus(this.terme, 'submit', last);
            tempTerme.statut.commentaire = '';
            this.commentaire = '';
            if (tempTerme.statut.libelle === this.statusAccessor.BROUILLON) {
              tempTerme.statut.valideur = savedTerme.proprietaire;
              tempTerme.statut.suppleant = savedTerme.proprietaire;
            } else {
              let testTerme = this.terme.statut.libelle == this.statusAccessor.REFUSER ? this.terme : savedTerme;
              tempTerme.statut.valideur = this.getNextValideur(testTerme, testTerme.statut.valideur, 'valideur');
              tempTerme.statut.suppleant = this.getNextValideur(testTerme, testTerme.statut.suppleant, 'suppleant/valideur');
            }
            return this.termeService.putV1Termes(tempTerme, 'response');
          }))
          );

        } else if (savedTerme.statut.libelle === this.statusAccessor.PUBLIE) {
          validations.push(this.termeService.putV1Termes(this.terme, 'response'));
        } else if (savedTerme.statut.libelle === this.statusAccessor.VALIDE) {
          validations.push(this.termeService.putV1Termes(this.terme, 'response'));
        }

        concat(...validations).subscribe(result => {
          this.initTermeAfterSubmit(result.body);
          this.refreshPage(result.body);
          if (result.headers.get('warning')) {
            this.showWarning(result.headers.get('warning'));
          }
        },
          msg => { // Error
            this.terme = savedTerme;
            if (msg.status === 409) {
              this.hideModal();
              this.showFailure('Le libellé est déjà utilisé sur la même filière');
            } else {
              this.hideModal();
              this.showFailure('L\'opération a échoué. Veuillez réessayer à nouveau.');
            }
          });
      }
    } else {
      this.showFailure('Merci de remplir tous les champs obligatoires');
    }
  }

  initTermeAfterSubmit(result): any {
    this.terme.id = result.id;
    this.terme.idTerme = result.idTerme;
    this.terme.revision = result.revision;
    this.terme.dateCreation = result.dateCreation;
    if (result.statut) {
      this.terme.statut = result.statut;
    }
    DataService.changeTermeData(this.terme);
    this.showSuccess();
    this.titre = 'Terme métier ';
    this.titre2 = ' - ' + this.terme.libelle;
    this.setIsModified(false);
  }

  checkFields(): boolean {
    this.libelleDocError = false;
    this.libelleUrlError = false;
    let docs = new Array<Document>();
    if (this.terme.documents) {
      docs = this.terme.documents.filter(doc => doc.titre === undefined || doc.titre === ''
        || doc.url === undefined || doc.url === '');
    }
    if (this.terme.libelle.toString() === '' || this.terme.filieres[0] === undefined ||
      this.terme.filieres[0]?.idFiliere === undefined || this.terme.filieres[0]?.idFiliere?.toString() === '' || docs.length > 0) {
      this.required = true;
      return false;
    } else {
      this.required = false;
      return true;
    }
  }

  addDocument(): any {
    this.libelleDocError = false;
    this.libelleUrlError = false;
    if (this.doc.titre !== undefined && this.doc.titre !== ''
      && this.doc.url !== undefined && this.doc.url !== '') {
      this.terme.documents.push(this.doc);
      this.doc = new Document();
    } else {
      this.showFailure('Merci de remplir tous les champs obligatoires');
      if (this.doc.titre === undefined || this.doc.titre === '') {
        this.libelleDocError = true;
      }
      if (this.doc.url === undefined || this.doc.url === '') {
        this.libelleUrlError = true;
      }
    }
  }

  removeDocument(index: number): any {
    this.setIsModified(true);
    this.terme.documents.splice(index, 1);
  }

  openModal(template: TemplateRef<any>): any {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'modal-dialog-centered' }, this.ngbModalOptions)
    );
  }

  openModalObjet(template: TemplateRef<any>): any {
    this.objets.forEach(obj => obj.associated = false);
    if (this.terme.objets) {
      this.terme.objets.forEach(obj => {
        const filtredObjet = this.objets.filter(o => o.idClasseDobjet === obj.idClasseDobjet)[0];
        if (filtredObjet) { filtredObjet.associated = true; }
      });
    }

    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: 'modal-lg' }, this.ngbModalOptions)
    );
  }

  showSuccess(): any {
    this.toastr.clear();
    this.toastr.success('Opération effectuée avec succès !');
  }

  showFailure(msg): any {
    this.toastr.clear();
    this.toastr.error(msg);
  }

  showWarning(msg): any {
    this.toastr.warning(msg);
  }

  ngOnDestroy(): void {
    this.hideModal();
    this.toastr.clear();
    this.objetSubscription.unsubscribe();
    DataService.pushSearch({ filiere: [], roles: [], searchValue: '', component: 'terme' });
  }

  associerObjets(): any {
    this.terme.objets = this.objets.filter(o => o.associated);
    this.filtredObjetAssoc = this.terme.objets;
    DataService.pushSearch({ filiere: [], roles: [], searchValue: '', component: 'terme' });
    this.setIsModified(true);
    this.hideModal();
  }

  removeObjet(index): any {
    this.setIsModified(true);
    this.terme.objets.splice(index, 1);
  }

  consultObjet(objet): any {
    this.objetService.getV1ObjetsId(objet.idClasseDobjet).subscribe(result => {
      DataService.changeObjetData(result);
      if (this.modalRef) {
        this.modalRef.hide();
      }
    });
  }

  hideModal(): any {
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }

  redirect(): any {
    this.hideModal();
    DataService.setIsModified(true);
    this.route.navigateByUrl('/main/termes');
  }

  isInvalid(value): boolean {
    return this.required && (value === undefined || value === '');
  }

  canDeactivate(): any {
    return confirm('Êtes-vous sûr(e) de vouloir quitter la page ? \n' +
      'En cliquant sur le bouton « OK », vous perdrez ' +
      'toutes les informations apportées à cette fiche.');
  }

  setIsModified(value): any {
    DataService.setIsModified(value);
  }

  changeFiliere(): any {
    if (this.terme.idTerme != null
      || (this.terme.idTerme == null
        && this.terme.filieres[0].idFiliere != null && this.terme.filieres[0].idFiliere.toString() !== '')) {
      this.setIsModified(true);
    }
  }

  private getStatusHistory(): any {
    this.allStatus = [];
    this.status1 = new Statut();
    const status = [];
    this.termeService.getStatusHistory(this.terme.statut?.id).subscribe(result => {
      this.allStatus = JSON.parse(JSON.stringify(result));
      result = result.filter(item => item.libelle !== this.statusAccessor.BROUILLON);
      if (result?.length > 0) {
        this.status1 = result[0];
        result.splice(0, 1);
      }
      result.forEach((s, index) => {
        if ((s.libelle === this.statusAccessor.AValider && index === result.length - 1)
          || (s.libelle === this.statusAccessor.REFUSER || s.libelle === this.statusAccessor.VALIDE
            || s.libelle === this.statusAccessor.SOUMIS)) {
          status.push(s);
        }
      });
      this.status = JSON.parse(JSON.stringify(status));
    }, () => { });
  }

  lastValideur(terme: Terme): boolean {
    let pos = 0;
    const selectedFiliere: Filiere = this.filieres.find((element: Filiere) => `${element.idFiliere}` === `${terme.filieres[0].idFiliere}`);
    const superRessources = selectedFiliere.superRessource.filter(element =>
      element.profilAutorisations != null && element.profilAutorisations[0] != null && element.profilAutorisations[0].libelle.toLocaleLowerCase().indexOf('valideur') !== -1 &&
      element.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique != null
    ).sort((a, b) => (a.profilAutorisations[0].libelle.toLowerCase() > b.profilAutorisations[0].libelle.toLowerCase()) ? 1 :
      ((b.profilAutorisations[0].libelle.toLowerCase() > a.profilAutorisations[0].libelle.toLowerCase()) ? -1 : 0));
    superRessources.forEach((element, index) => {
      if (element.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique.utilisateur.idUtilisateur
        === terme.statut.valideur.idUtilisateur) {
        pos = index + 1;
      }
    });

    return pos >= superRessources.length ? true : false;
  }

  submit(): any {
    this.save(this.statusAccessor.AValider, 'submit');
  }

  valider(): any {
    this.save(this.statusAccessor.AValider, 'submit');
  }

  refuser(): any {
    this.save(this.statusAccessor.AValider, 'deny');
  }


  getNextValideur(terme, valideur, typeValideur): Utilisateur {
    let next: Utilisateur;
    const selectedFiliere: Filiere = this.filieres.find((element: Filiere) => `${element.idFiliere}` === `${terme.filieres[0].idFiliere}`);
    const superRessources = selectedFiliere.superRessource.filter(element =>
      element.profilAutorisations != null && element.profilAutorisations[0] != null && element.profilAutorisations[0].libelle.toLocaleLowerCase().indexOf(typeValideur) === 0 &&
      element.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique != null
    ).sort((a, b) => (a.profilAutorisations[0].libelle.toLowerCase() > b.profilAutorisations[0].libelle.toLowerCase()) ? 1 :
      ((b.profilAutorisations[0].libelle.toLowerCase() > a.profilAutorisations[0].libelle.toLowerCase()) ? -1 : 0));

    switch (terme.statut.libelle) {
      case this.statusAccessor.BROUILLON:
        next = terme.proprietaire;
        break;
      case this.statusAccessor.VALIDE:
        next = superRessources[0].profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique.utilisateur;
        break;
      case this.statusAccessor.SOUMIS:
        next = superRessources[0].profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique.utilisateur;
        break;
      case this.statusAccessor.AValider:
        let pos = 0;
        superRessources.forEach((element, index) => {
          if (element.profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique.utilisateur.idUtilisateur
            === valideur.idUtilisateur) {
            pos = index + 1;
          }
        });
        if (pos >= superRessources.length) {
          pos = pos - 1;
        }
        next = superRessources[pos].profilAutorisations[0].profilAutorisationAbstraits[0].autorisations[0].identiteNumerique.utilisateur;
        break;
      case this.statusAccessor.REFUSER:
        next = terme.proprietaire;
        break;
      case this.statusAccessor.PUBLIE:
        next = terme.proprietaire;
        break;
      default:
        break;
    }
    return next;
  }

  getNextStatus(terme: Terme, operation: string, last: boolean): string {
    let status;

    if (terme.statut == null || terme.statut.libelle == null || terme.statut?.libelle === this.statusAccessor.BROUILLON) {
      return operation === 'save' ? this.statusAccessor.BROUILLON : this.statusAccessor.SOUMIS;
    }

    switch (terme.statut.libelle) {
      case this.statusAccessor.SOUMIS:
        status = this.statusAccessor.AValider;
        break;
      case this.statusAccessor.AValider:
        status = operation === 'submit' ? this.statusAccessor.VALIDE : this.statusAccessor.REFUSER;
        break;
      case this.statusAccessor.VALIDE:
        status = last ? this.statusAccessor.PUBLIE : this.statusAccessor.AValider;
        break;
      case this.statusAccessor.PUBLIE:
        status = this.statusAccessor.BROUILLON;
        break;
      case this.statusAccessor.REFUSER:
        status = this.statusAccessor.BROUILLON;
        break;
      case this.statusAccessor.BROUILLON:
        status = this.statusAccessor.BROUILLON;
        break;
      default:
        break;
    }

    return status;
  }

  showButton(save: string): boolean {
    switch (save) {
      case 'save':
        return this.isModified;
        break;
      case 'submit':
        return (this.terme.statut?.libelle === undefined && this.isModified)
          || (this.allStatus[this.allStatus?.length - 1]?.libelle === this.statusAccessor.BROUILLON
            && (this.terme.proprietaire.idUtilisateur === this.getCurrentUser().idUtilisateur || AuthGuard.isAdmin()
              || AuthGuard.isValideur(this.terme.filieres[0]) || AuthGuard.isRedacteur(this.terme.filieres[0])));
        break;
      case 'corriger':
        return this.allStatus[this.allStatus?.length - 1]?.libelle === this.statusAccessor.PUBLIE
          && (this.isValidator() || AuthGuard.isAdmin());

      case 'valider':
        return this.allStatus[this.allStatus?.length - 1]?.libelle === this.statusAccessor.AValider
          && (this.allStatus[this.allStatus?.length - 1]?.valideur?.idUtilisateur === this.getCurrentUser().idUtilisateur
            || this.allStatus[this.allStatus?.length - 1]?.suppleant?.idUtilisateur === this.getCurrentUser().idUtilisateur
            || AuthGuard.isAdmin());

      default:
        return false;
    }
  }

  private getCurrentUser(): Utilisateur {
    return (JSON.parse(localStorage.getItem('utilisateur')) as Utilisateur);
  }

  private isValidator(): boolean {
    return this.allStatus.filter(statut => statut.libelle === this.statusAccessor.AValider &&
      (statut.suppleant?.idIdentiteNumerique === this.getCurrentUser().idIdentiteNumerique
        || statut.valideur?.idIdentiteNumerique === this.getCurrentUser().idIdentiteNumerique))?.length > 0;
  }
  private refreshPage(terme): any {
    this.route.navigate(['/main/terme'], {
      relativeTo: this.actRoute,
      queryParams: {
        action: terme.statut === null || terme.statut?.libelle === this.statusAccessor.BROUILLON ? 'edit' : 'consult'
      }
    });
  }

}


