

export function init(line) {
    $("#draggable").draggable({
        revert: "invalid",
        drag: function () {
            if (line) {
                line.position();
            }
        },
        stop: function () {
            if (line) {
                line.position();
            }
        }
    });
    $("#draggable2").draggable({
        revert: "invalid",
        drag: function () {
            if (line) {
                line.position();
            }
        },
        stop: function () {
            if (line) {
                line.position();
            }
        }
    });

    $("#droppable").droppable({
        classes: {
            "ui-droppable-active": "ui-state-active",
            "ui-droppable-hover": "ui-state-hover"
        },
        drop: function (event, ui) {
            $(this)
                .addClass("ui-state-highlight")
                .find("p")
                .html("Dropped!");
        }
    });
} 